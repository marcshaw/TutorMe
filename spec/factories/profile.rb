FactoryGirl.define do
  factory :profile do
    sequence(:email) { |i| "marc#{i}@hotmail.com" }
    confirmed_at          Time.now
    password "marcmarc"
    password_confirmation 'marcmarc'
    first_name "Marc"

    trait :tutor do
      phone_number "027027027"
      tutoring_subjects {[FactoryGirl.create(:tutoring_subject)]}
      active_tutor true
      description "Hi!"
      university
      tutoring_years {[FactoryGirl.create(:tutoring_year)]}
      mediums {[FactoryGirl.create(:medium)]}
      course_codes {[FactoryGirl.create(:course_code)]}
      rate_cents 4000
      availabilities {[FactoryGirl.create(:availability)]}
    end

    factory :rich_profile, traits: [:tutor] do
      after(:create) do |profile, evaluator|
        profile.txns.create!(amount_cents: 4000, txn_type: Txn.txn_types[:deposit])
      end
    end

    factory :tutor, traits: [:tutor]
  end
end
