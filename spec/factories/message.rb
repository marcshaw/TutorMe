FactoryGirl.define do
  factory :message do

    association :conversation, factory: :conversation
    association :profile, factory: :tutor
    body "Hello there"

  end
end
