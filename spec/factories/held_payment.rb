FactoryGirl.define do
  factory :held_payment do
    tutor_accepted true
    student_accepted true
    association :booking, factory: :completed_booking
  end
end
