FactoryGirl.define do
  factory :txn do
    association :profile, factory: :rich_profile

    trait :deposit do
      amount_cents 2000
      txn_type Txn.txn_types[:deposit]
    end

    trait :withdrawl do
      amount_cents 2000
      txn_type Txn.txn_types[:withdrawl]
    end

    trait :booking do
      amount_cents 1000
      txn_type Txn.txn_types[:booking]
    end

    factory :valid_booking_txn, traits: [:booking]
    factory :valid_deposit, traits: [:deposit]
    factory :valid_withdrawl, traits: [:withdrawl]
  end
end
