FactoryGirl.define do
  factory :conversation do

    association :sender, factory: :tutor
    association :recipient, factory: :tutor
    latest_message Time.now

  end
end
