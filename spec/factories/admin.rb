FactoryGirl.define do
  factory :admin do
    association :profile, factory: :rich_profile
  end
end
