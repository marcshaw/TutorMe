FactoryGirl.define do
  factory :search do

    min_rate_cents 15
    max_rate_cents 30
    university "Victoria University of Wellington"
    tutoring_subject "Mathematics"

  end
end