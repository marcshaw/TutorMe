FactoryGirl.define do
  factory :review do

    body "This is the review"
    rating 4
    token "jimmy"
    association :profile, factory: :tutor
    association :reviewer, factory: :profile
    association :booking, factory: :completed_booking

    trait :reviewed do
      reviewed true
    end

    factory :reviewed, traits: [:reviewed]
  end
end
