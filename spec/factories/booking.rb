FactoryGirl.define do
  factory :booking do
    datetime 3.days.from_now
    time_length 30
    contact_number '0273448487'
    subject 'Mathematics'
    course_code 'Comp301'
    price_cents 2000
    tutor_rate 4000
    medium 'Skype'
    tutoring_year 'First'
    university 'Victoria University of Wellington'
    association :tutor_profile, factory: :rich_profile
    association :student_profile, factory: :rich_profile
    tutor_confirmed false
    student_confirmed false
    cancelled false
    completed false
    paid false
    disputed false

    trait :confirmed do
      tutor_confirmed true
      student_confirmed true
    end

    trait :unconfirmed do
      tutor_confirmed false
      student_confirmed false
    end

    trait :unpaid do
      paid false
    end

    trait :paid do
      paid true
      completed true
    end

    trait :disputed do
      disputed true
    end

    trait :completed do
      completed true
    end

    trait :uncompleted do
      completed false
    end

    trait :cancelled do
      cancelled true
    end

    factory :cancelled_booking, traits: [:cancelled]
    factory :paid_booking, traits: [:paid, :confirmed]
    factory :unpaid_booking, traits: [:unpaid, :confirmed]
    factory :unconfirmed_booking, traits: [:unconfirmed]
    factory :confirmed_booking, traits: [:confirmed]
    factory :completed_booking, traits: [:completed, :confirmed, :unpaid]
    factory :uncompleted_booking, traits: [:uncompleted, :confirmed, :unpaid]
    factory :disputed_booking, traits: [:disputed, :confirmed]
  end
end

