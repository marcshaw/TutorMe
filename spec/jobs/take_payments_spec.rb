require 'rails_helper'

RSpec.describe TakePayments, :type => :job do
  let(:bookings) do
    2.times.map do
      FactoryGirl.build(:confirmed_booking, datetime: Time.now + 2.day).tap { |booking| booking.save(validate: false) }
    end
  end

  subject(:take_payments) do
    TakePayments.new(
      query_method: BookingsToTakePayment.new(bookings: Booking.all)
    )
  end

  context "with invalid input" do
    before { bookings[0].update!(cancelled: true) }

    it "will continue running if one booking fails" do
      expect(bookings[0].held_payment).to be_falsey
      expect(bookings[1].held_payment).to be_falsey
      subject.call
      reload_bookings
      expect(bookings[0].held_payment).to be_falsey
      expect(bookings[1].held_payment).to be_truthy
    end
  end

  context "with valid input" do
    it "will release all the funds of held payments" do
      expect(bookings[0].held_payment).to be_falsey
      expect(bookings[1].held_payment).to be_falsey
      subject.call
      reload_bookings
      expect(bookings[0].held_payment).to be_truthy
      expect(bookings[1].held_payment).to be_truthy
    end
  end

  private

  def reload_bookings
    bookings.each { |booking| booking.reload }
  end
end
