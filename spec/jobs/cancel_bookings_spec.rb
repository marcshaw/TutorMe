require 'rails_helper'

RSpec.describe CancelBookings, :type => :job do
  let(:bookings) do
    2.times.map do
      FactoryGirl.build(:unconfirmed_booking, datetime: Time.now).tap { |x| x.save(validate: false) }
    end
  end

  subject(:cancel_bookings) do
    CancelBookings.new(
      query_method: BookingsToCancel.new(bookings: Booking.all)
    )
  end

  context "with invalid input" do
    before { bookings[0].update_attribute(:completed, true) }

    it "will continue running if one booking fails" do
      expect(bookings[0].cancelled).to be_falsey
      expect(bookings[1].cancelled).to be_falsey
      subject.call
      reload_bookings
      expect(bookings[0].cancelled).to be_falsey
      expect(bookings[1].cancelled).to be_truthy
    end
  end

  context "with valid input" do
    it "will release all the funds of held payments" do
      expect(bookings[0].cancelled).to be_falsey
      expect(bookings[1].cancelled).to be_falsey
      subject.call
      reload_bookings
      expect(bookings[0].cancelled).to be_truthy
      expect(bookings[1].cancelled).to be_truthy
    end
  end

  private

  def reload_bookings
    bookings.each { |booking| booking.reload }
  end
end
