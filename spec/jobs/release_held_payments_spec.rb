require 'rails_helper'

RSpec.describe ReleaseHeldPayments, :type => :job do
  let(:held_payments) do
    2.times.map do
      FactoryGirl.build(:held_payment,
                         booking: build_booking_to_release
                        ).tap { |booking| booking.save(validate: false) }
    end
  end
  let!(:admin) { create(:admin) }

  subject(:release_held_payments) do
    ReleaseHeldPayments.new(
      query_method: PaymentsToRelease.new(bookings: Booking.all)
    )
  end

  context "with invalid input" do
    before { held_payments[0].booking.update_attribute(:disputed, true) }

    it "will continue running if one booking fails" do
      expect(held_payments[0].booking.paid).to be_falsey
      expect(held_payments[1].booking.paid).to be_falsey
      expect(held_payments[0].booking.completed).to be_falsey
      expect(held_payments[1].booking.completed).to be_falsey
      subject.call
      reload_payments
      expect(held_payments[0].booking.paid).to be_falsey
      expect(held_payments[1].booking.paid).to be_truthy
      expect(held_payments[0].booking.completed).to be_falsey
      expect(held_payments[1].booking.completed).to be_truthy
    end
  end

  context "with valid input" do
    it "will release all the funds of held payments" do
      expect(held_payments[0].booking.paid).to be_falsey
      expect(held_payments[1].booking.paid).to be_falsey
      expect(held_payments[0].booking.completed).to be_falsey
      expect(held_payments[1].booking.completed).to be_falsey
      subject.call
      reload_payments
      expect(held_payments[0].booking.paid).to be_truthy
      expect(held_payments[1].booking.paid).to be_truthy
      expect(held_payments[0].booking.completed).to be_truthy
      expect(held_payments[1].booking.completed).to be_truthy
    end
  end

  private

  def reload_payments
    held_payments.each { |payment| payment.reload }
  end

  def build_booking_to_release
    FactoryGirl.build(:uncompleted_booking, datetime: Time.now - 4.day).tap { |booking| booking.save(validate: false) }
  end
end
