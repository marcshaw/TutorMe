require 'rails_helper'

describe Conversation do

  subject {
    create(:conversation)
  }

  it "should have only one conversation between two users" do

    should validate_uniqueness_of(:sender_id).scoped_to(:recipient_id).ignoring_case_sensitivity

  end

end