require 'rails_helper'

describe Booking do

  let(:booking) { FactoryGirl.create(:booking) }
  subject { booking }

  context "with valid attributes" do
    it "has a valid course code" do
      booking.course_code = "Comp123"
      expect(booking).to be_valid
    end

    it "has a valid contact number" do
      booking.contact_number = "0270270270"
      expect(booking).to be_valid
    end

    it "has a valid price" do
      booking.price_cents = booking.tutor_profile.rate_cents * (booking.time_length/60.0)
      expect(booking).to be_valid
    end
  end

  context "with invalid attributes" do

    it "can not be completed and cancelled" do
      booking.cancelled = true
      booking.completed = true
      booking.valid?
      expect(booking).to have(1).error_on(:status_conflict)
      expect(booking.errors_on(:status_conflict).first).to match(/- The booking can not be completed and cancelled at the same time/)
    end

    it "not to be paid if booking is currently disputed" do
      booking.update!(disputed: true, completed: true)
      booking.paid = true
      booking.valid?
      expect(booking).to have(1).error_on(:status_conflict)
      expect(booking.errors_on(:status_conflict).first).to match(/The booking can not be paid when it is currently disputed/)
    end

    it "has a invalid course code that is to long" do
      booking.course_code = "Compp123"
      expect(booking).to have(1).error_on(:course_code)
    end

    it "has a invalid course code that has to many numbers" do
      booking.course_code = "Com2123"
      expect(booking).to have(1).error_on(:course_code)
    end

    it "has a invalid price" do
      booking.price = 9
      booking.time_length = 50
      expect(booking).to have(1).error_on(:price_cents)
    end

    it "has a datetime that is in the past" do
      booking.datetime = 1.day.ago
      expect(booking).to have(1).error_on(:datetime)
    end
  end

  it { is_expected.to validate_length_of(:contact_number).is_at_least(8).is_at_most(15) }

  #Validating presence
  it { is_expected.to validate_presence_of :time_length }
  it { is_expected.to validate_presence_of :contact_number }
  it { is_expected.to validate_presence_of :subject }
  it { is_expected.to validate_presence_of :course_code }
  it { is_expected.to validate_presence_of :price_cents }
  it { is_expected.to validate_presence_of :medium }
  it { is_expected.to validate_presence_of :tutoring_year }
  it { is_expected.to validate_presence_of :university }
  it { is_expected.to validate_presence_of :datetime }
  it { is_expected.to validate_presence_of :tutor_profile }
  it { is_expected.to validate_presence_of :student_profile }

  #Validating nil
  it { is_expected.to_not allow_values(nil).for(:course_code) }
  it { is_expected.to_not allow_values(nil).for(:time_length) }
  it { is_expected.to_not allow_values(nil).for(:contact_number) }
  it { is_expected.to_not allow_values(nil).for(:subject) }
  it { is_expected.to_not allow_values(nil).for(:course_code) }
  it { is_expected.to_not allow_values(nil).for(:price_cents) }
  it { is_expected.to_not allow_values(nil).for(:medium) }
  it { is_expected.to_not allow_values(nil).for(:tutoring_year) }
  it { is_expected.to_not allow_values(nil).for(:university) }
  it { is_expected.to_not allow_values(nil).for(:datetime) }
  it { is_expected.to_not allow_values(nil).for(:tutor_profile) }
  it { is_expected.to_not allow_values(nil).for(:student_profile) }
  it { is_expected.to_not allow_values(nil).for(:completed) }
  it { is_expected.to_not allow_values(nil).for(:disputed) }
  it { is_expected.to_not allow_values(nil).for(:cancelled) }
  it { is_expected.to_not allow_values(nil).for(:student_confirmed) }
  it { is_expected.to_not allow_values(nil).for(:tutor_confirmed) }

  it { is_expected.to_not allow_values('').for(:course_code) }
  it { is_expected.to_not allow_values('').for(:time_length) }
  it { is_expected.to_not allow_values('').for(:contact_number) }
  it { is_expected.to_not allow_values('').for(:subject) }
  it { is_expected.to_not allow_values('').for(:course_code) }
  it { is_expected.to_not allow_values('').for(:price) }
  it { is_expected.to_not allow_values('').for(:medium) }
  it { is_expected.to_not allow_values('').for(:tutoring_year) }
  it { is_expected.to_not allow_values('').for(:university) }
  it { is_expected.to_not allow_values('').for(:datetime) }
end
