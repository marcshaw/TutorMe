require 'rails_helper'

describe CourseCode do
  let(:course) { FactoryGirl.build(:course_code) }

  context "with valid attributes" do
    it "has a valid course code" do
      course.course_code = "Comp123"
      expect(course).to have(0).error_on(:course_code)
    end
  end

  context "with invalid attributes" do
    it "has a invalid course code that is to long" do
      course.course_code = "Compp123"
      expect(course).to have(1).error_on(:course_code)
    end

    it "has a invalid course code that has to many numbers" do
      course.course_code = "Com2123"
      expect(course).to have(1).error_on(:course_code)
    end
  end
end
