require 'rails_helper'

describe Review do
  subject(:review) { FactoryGirl.build_stubbed(:reviewed) }

  context "with valid attributes" do
    it "has a high valid rating" do
      review.rating = 5
      expect(review).to have(0).errors_on(:rating)
    end

    it "has a low valid rating" do
      review.rating = 1
      expect(review).to have(0).errors_on(:rating)
    end
  end

  context "with invalid attributes" do
    it "has to have a token" do
      review.token = nil
      expect(review).to have(1).error_on(:token)
    end

    it "has a rating that is too low" do
      review.rating = 0
      expect(review).to have(1).error_on(:rating)
    end

    it "has a rating that is too high" do
      review.rating = 6
      expect(review).to have(1).error_on(:rating)
    end
  end

  it { is_expected.to validate_length_of(:body).is_at_most(250) }
end

