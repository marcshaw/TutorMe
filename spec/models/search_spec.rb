require 'rails_helper'

describe Booking do

  subject(:search) { FactoryGirl.build_stubbed(:search) }

  context "with valid attributes" do

    it "has a valid course code" do
      search.course_code = "Comp123"
      expect(search).to have(0).errors_on(:course_code)
    end

    it "has a valid tutoring subject" do
      search.tutoring_subject = "Mathematics"
      expect(search).to have(0).error_on(:tutoring_subject)
    end

    it "has a valid university" do
      search.university = "Victoria University of Wellington"
      expect(search).to have(0).error_on(:university)
    end
  end

  context "with invalid attributes" do
    it "has a invalid course code that is to long" do
      search.course_code = "Compp123"
      expect(search).to have(1).error_on(:course_code)
    end

    it "has a invalid course code that has to many numbers" do
      search.course_code = "Com2123"
      expect(search).to have(1).error_on(:course_code)
    end

    it "has a invalid university" do
      search.university = "NoUni"
      expect(search).to have(1).error_on(:university)
    end
  end
end
