require 'rails_helper'

RSpec.describe Txn, type: :model do
  context "with invalid parameters" do
    let(:txn) { FactoryGirl.build(:valid_withdrawl) }

    it "will fail when not given an amount" do
      txn.gst = 0
      txn.amount_cents = nil
      expect(txn.errors_on(:amount_cents)).to include("can't be blank")
    end

    it "will fail when not given a txn type" do
      txn.txn_type = nil
      expect(txn).to have(1).error_on(:txn_type)
    end

    it "will fail when not associated with a profile" do
      txn.profile = nil
      expect(txn).to have(1).error_on(:profile)
    end

    it "will fail if the profile is in the negatives" do
      txn.profile.txns.create!(txn_type: Txn.txn_types[:withdrawl], amount_cents: txn.profile.balance_cents)
      expect(txn.errors_on(:amount_cents)).to include("The user should have a balance that is a positive number")
    end

    it "will fail if deposit is not larger than 2000 cents" do
      txn.amount_cents = 10
      txn.txn_type = Txn.txn_types[:deposit]
      expect(txn.errors_on(:amount_cents)).to include("must be greater than or equal to 2000")
    end

    it "will fail if deposit is larger than 5000 cents" do
      txn.amount_cents = 6000
      txn.txn_type = Txn.txn_types[:deposit]
      expect(txn.errors_on(:amount_cents)).to include("must be less than or equal to 5000")
    end

    it "will fail if withdrawl is less than or equal to 1 cent" do
      txn.txn_type = Txn.txn_types[:withdrawl]
      txn.amount_cents = -1
      expect(txn.errors_on(:amount_cents)).to include("must be greater than 0")
    end
  end

  context "with valid parameters" do
    it "will save the withdrawl to the database" do
      txn = FactoryGirl.build(:valid_withdrawl)
      expect { txn.save! }.to change { Txn.withdrawl.count }.by(1)
    end

    it "will save the deposit to the database" do
      txn = FactoryGirl.build(:valid_deposit)
      expect { txn.save! }.to change { Txn.deposit.count }.by(1)
    end

    it "will set the gst if not given" do
      txn = FactoryGirl.build(:valid_deposit)
      txn.save!
      expect(txn.gst).to eq(txn.amount_cents * 0.15)
    end

    it "will save a valid booking txn to the database" do
      txn = FactoryGirl.build(:valid_booking_txn)
      expect { txn.save! }.to change { Txn.booking.count }.by(1)
    end
  end
end
