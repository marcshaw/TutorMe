require 'rails_helper'

describe Profile do
  let(:profile) { FactoryGirl.create(:tutor) }
  subject { profile }

  context "with valid attributes" do
    it "has a valid tutoring subject" do
      profile.tutoring_subjects << FactoryGirl.create(:tutoring_subject)
      expect(profile).to have(0).error_on(:tutoring_subjects)
    end

    it "has a valid availability" do
      profile.availabilities << FactoryGirl.build(:availability)
      expect(profile).to have(0).error_on(:availabilities)
    end
  end

  context "with invalid attributes" do
    it "has a availability that doesn't exist" do
      profile.availabilities << Availability.new(:day => "Jimmy")
      expect(profile).to have(1).error_on(:availabilities)
    end
  end

  it { is_expected.to validate_length_of(:description).is_at_most(500).with_message("500 characters is the maximum allowed for the description") }
  #Validating presence
  it { is_expected.to validate_presence_of :mediums }
  it { is_expected.to validate_presence_of :university }
  it { is_expected.to validate_presence_of :rate_cents }
  it { is_expected.to validate_presence_of :tutoring_years }
  it { is_expected.to validate_presence_of :description }
  it { is_expected.to validate_presence_of :availabilities }
  it { is_expected.to validate_presence_of :first_name }
end
