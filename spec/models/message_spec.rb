require 'rails_helper'

describe Message do

  it { is_expected.to validate_presence_of :body }
  it { is_expected.to validate_presence_of :conversation_id }
  it { is_expected.to validate_presence_of :profile_id }
  it { is_expected.to validate_length_of(:body).is_at_most(1000).with_long_message("1000 characters is the maximum allowed for the description") }

end
