# Preview all emails at http://localhost:3000/rails/mailers/user_notifier
class UserNotifierPreview < ActionMailer::Preview
  def welcome_email
    Premailer::Rails::Hook.perform(UserNotifier.welcome_email(Profile.first))
  end

  def new_tutor_booking
    Premailer::Rails::Hook.perform(UserNotifier.new_tutor_booking(Profile.first, Profile.second))
  end

  def new_student_booking
    Premailer::Rails::Hook.perform(UserNotifier.new_student_booking(Profile.first, Profile.second))
  end

  def review_tutor
    Premailer::Rails::Hook.perform(UserNotifier.review_tutor(Review.last))
  end

  def confirmed_tutor_booking
    Premailer::Rails::Hook.perform(UserNotifier.confirmed_tutor_booking(Booking.first))
  end

  def confirmed_student_booking
    Premailer::Rails::Hook.perform(UserNotifier.confirmed_student_booking(Booking.first))
  end

  def reminder_email
    Premailer::Rails::Hook.perform(UserNotifier.reminder_email(Profile.first, Booking.first))
  end

  def cancelled_student_booking
    Premailer::Rails::Hook.perform(UserNotifier.cancelled_student_booking(Booking.first))
  end

  def cancelled_tutor_booking
    Premailer::Rails::Hook.perform(UserNotifier.cancelled_tutor_booking(Booking.first))
  end

  def week_prior_add_funds_warning
    Premailer::Rails::Hook.perform(UserNotifier.week_prior_add_funds_warning(Profile.first, Booking.first))
  end

  def three_days_prior_add_funds_warning
    Premailer::Rails::Hook.perform(UserNotifier.three_days_prior_add_funds_warning(Profile.first, Booking.first))
  end
end
