class DeviseMailerPreview < ActionMailer::Preview
  def confirmation_instructions
    DeviseMailer.confirmation_instructions(Profile.first, "token")
  end

  def reset_password_instructions
    DeviseMailer.reset_password_instructions(Profile.first, "faketoken", {})
  end

  def unlock_instructions
    #DeviseMailer.unlock_instructions(Profile.first, "faketoken", {})
  end
end
