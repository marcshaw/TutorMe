require 'rails_helper'

RSpec.describe CreateReview, :type => :service do
  let(:review) { FactoryGirl.create(:review) }

  subject(:create_review) { CreateReview.new(review_params, review) }

  context "when the parameters are invalid" do
    let(:review_params) { { body: "", rating: 5 } }

    it "will return a review with an error" do
      review = create_review.call
      expect(review).to have(1).error_on(:body)
    end
  end

  context "when the parameters are valid" do
    let(:review_params) { { body: "Great Guy.", rating: 5 } }

    it "will return a review with no error" do
      review = create_review.call
      expect(review).to be_valid
    end
  end
end

