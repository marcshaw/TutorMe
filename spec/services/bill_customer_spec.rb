require 'rails_helper'

RSpec.describe BillCustomer, :type => :service do
  let(:txn) { FactoryGirl.build(:valid_deposit) }
  let(:token) { "notvalid" }
  let(:stripe) { double(Stripe::Charge) }

  subject(:bill_customer) { BillCustomer.new(txn, token, payment_method: stripe) }

  it "will throw an error if the txn is saved" do
    txn.save!
    expect { subject.call }.to raise_error(InvalidParameters)
  end

  it "will not save the transaction if stripe throws exception" do
    expect(stripe).to receive(:create).with(any_args).and_raise(ActiveRecord::RecordInvalid.new)
    expect { subject.call }.to raise_error(ActiveRecord::RecordInvalid)
    expect(txn).to_not be_persisted
  end

  it "will not send request to stripe if txn fails" do
    txn.amount_cents = -(txn.profile.balance_cents + 10)
    expect(stripe).to_not receive(:create)
    expect { subject.call }.to raise_error(ActiveRecord::RecordInvalid)
    expect(txn).to_not be_persisted
  end

  it "will add the correct amount to the users account" do
    expect(stripe).to receive(:create).with(any_args)
    expect { subject.call }.to change { txn.profile.balance_cents }.by(txn.amount_cents)
  end
end
