require 'rails_helper'

RSpec.describe ReleaseHeldPayment, :type => :service do
  let(:held_payment) { FactoryGirl.create(:held_payment, booking: create(:uncompleted_booking)) }
  let(:booking) { held_payment.booking }
  let!(:admin) { create(:admin) }

  subject(:release_funds) { ReleaseHeldPayment.new(held_payment.booking) }

  context "with invalid input" do
    it "will throw an error when the booking is completed" do
      expect { subject.call }.to_not raise_error
      booking.completed = true
      expect { subject.call }.to raise_error(InvalidParameters)
    end

    it "will throw an error when the booking is disputed" do
      expect { subject.call }.to_not raise_error
      booking.disputed = true
      expect { subject.call }.to raise_error(InvalidParameters)
    end

    it "will throw an error when the booking is cancelled" do
      expect { subject.call }.to_not raise_error
      booking.cancelled = true
      expect { subject.call }.to raise_error(InvalidParameters)
    end

    it "will throw an error when the booking is paid" do
      expect { subject.call }.to_not raise_error
      booking.paid = true
      expect { subject.call }.to raise_error(InvalidParameters)
    end

    it "will throw an error when the booking is no persisted" do
      expect { subject.call }.to_not raise_error
      booking = FactoryGirl.build(:confirmed_booking)
      release_funds = ReleaseHeldPayment.new(booking)
      expect { release_funds.call }.to raise_error(InvalidParameters)
    end
  end

  context "with valid input" do
    let(:fee) { 0.925 }

    it "will create a transaction on the tutor profile" do
      expect { subject.call }.to change { booking.tutor_profile.txns.count }.by(1)
    end

    it "will add the funds to the tutor's profile" do
      expect { subject.call }.to change { booking.tutor_profile.balance_cents }.by(booking.price_cents * fee)
    end

    it "will add the rest of the funds to the admins profile" do
      expect { subject.call }.to change { admin.profile.balance_cents }.by(booking.price_cents * (1-fee).round(4))
    end

    it "will update the booking to be paid" do
      expect { subject.call }.to change { booking.paid }.from(false).to(true)
    end
  end
end
