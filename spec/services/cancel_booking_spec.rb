require 'rails_helper'

RSpec.describe CancelBooking, :type => :service do
  let(:booking) { FactoryGirl.create(:booking) }
  let(:from_job) { false }

  subject(:cancel_booking) { CancelBooking.new(booking, from_job: from_job) }

  context "with invalid input" do
    it "will throw an error when the booking is completed" do
      expect { subject.call }.to_not raise_error
      booking.completed = true
      expect { subject.call }.to raise_error(InvalidParameters)
    end

    it "will throw an error when the booking is cancelled" do
      expect { subject.call }.to_not raise_error
      booking.cancelled = true
      expect { subject.call }.to raise_error(InvalidParameters)
    end

    it "will throw an error when the booking is paid" do
      expect { subject.call }.to_not raise_error
      booking.held_payment = create(:held_payment, booking: booking)
      expect { subject.call }.to raise_error(InvalidParameters)
    end

    it "will throw an error when the booking is no persisted" do
      expect { subject.call }.to_not raise_error
      booking = FactoryGirl.build(:confirmed_booking)
      cancel_booking = CancelBooking.new(booking)
      expect { cancel_booking.call }.to raise_error(InvalidParameters)
    end
  end

  context "with valid input" do
    it "will update the booking to be cancelled" do
      expect { subject.call }.to change { booking.cancelled }.from(false).to(true)
    end

    context "when it is from a job" do
      let(:mail_double) { double }
      let(:from_job) { true }

      it "will deliver the mail now" do
        expect(UserNotifier).to receive(:cancelled_tutor_booking).and_return(mail_double)
        expect(mail_double).to receive(:deliver_now)
        subject.call
      end
    end

    context "when it is not from a job" do
      let(:mail_double) { double }
      let(:from_job) { false }

      it "will deliver the mail later" do
        expect(UserNotifier).to receive(:cancelled_tutor_booking).and_return(mail_double)
        expect(mail_double).to receive(:deliver_later)
        subject.call
      end
    end
  end
end
