require 'rails_helper'

RSpec.describe CompleteBooking, :type => :service do
  let(:booking) { FactoryGirl.create(:booking) }

  subject(:complete_booking) { CompleteBooking.new(booking) }

  context "with invalid input" do
    it "will throw an error when the booking is completed" do
      expect { subject.call }.to_not raise_error
      booking.completed = true
      expect { subject.call }.to raise_error(InvalidParameters)
    end

    it "will throw an error when the booking is cancelled" do
      expect { subject.call }.to_not raise_error
      booking.cancelled = true
      expect { subject.call }.to raise_error(InvalidParameters)
    end

    it "will throw an error when the booking is disputed" do
      expect { subject.call }.to_not raise_error
      booking.disputed = true
      expect { subject.call }.to raise_error(InvalidParameters)
    end

    it "will throw an error when the booking is no persisted" do
      expect { subject.call }.to_not raise_error
      booking = FactoryGirl.build(:confirmed_booking)
      cancel_booking = CancelBooking.new(booking)
      expect { cancel_booking.call }.to raise_error(InvalidParameters)
    end
  end

  context "with valid input" do
    it "will update the booking to be completed" do
      expect { subject.call }.to change { booking.completed }.from(false).to(true)
    end

    context "when it is the first booking between the users" do
      let(:mail) { instance_double(ActionMailer::MessageDelivery) }

      it "will create a new review and email the student" do
        expect(UserNotifier).to receive(:review_tutor).and_return(mail)
        expect(mail).to receive(:deliver_now)
        expect { subject.call }.to change { booking.tutor_profile.reviews.count }.by(1)
      end
    end

    context "when it is not the first review between the users" do
      before do
        old_booking = FactoryGirl.create(:booking, tutor_profile: booking.tutor_profile, student_profile: booking.student_profile)
        CompleteBooking.new(old_booking).call
      end

      it "will not create a new review and email the student" do
        expect(UserNotifier).to_not receive(:review_tutor)
        expect { subject.call }.to_not change { booking.tutor_profile.reviews }
      end
    end
  end
end
