require 'rails_helper'

RSpec.describe NewReview, :type => :service do
  let(:profile) { FactoryGirl.create(:tutor) }
  let(:reviewer) { FactoryGirl.create(:rich_profile) }
  let(:booking) { FactoryGirl.create(:completed_booking) }

  subject(:new_review) { NewReview.new(profile, reviewer, booking) }

  context "when a completed booking does not exist" do
    let(:booking) { FactoryGirl.create(:unconfirmed_booking) }

    it "does not create a new review" do
      expect { new_review.call }.to raise_error(InvalidParameters)
    end
  end

  context "when a completed booking exists" do
    let!(:booking) { FactoryGirl.create(:completed_booking, student_profile: reviewer, tutor_profile: profile) }

    it "does creates a new review" do
      expect { new_review.call }.to change { Review.all.count }.by(1)
    end

    it "will add the review to the profile" do
      review = new_review.call
      expect(profile.reviews).to include(review)
    end

    it "will set the reviewer to the correct profile" do
      review = new_review.call
      expect(review.reviewer).to eq(reviewer)
    end

    it "will set the correct booking" do
      review = new_review.call
      expect(review.booking).to eq(booking)
    end

    it "will create a token" do
      review = new_review.call
      expect(review).to be_valid
    end
  end
end

