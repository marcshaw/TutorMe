require 'rails_helper'

RSpec.describe ConfirmBooking, :type => :service do
  let(:booking) { FactoryGirl.create(:unconfirmed_booking) }

  subject(:confirm_booking) { ConfirmBooking.new(booking, profile) }

  context "when the tutor confirms the booking" do
    let (:profile) { booking.tutor_profile }

    it "will set the tutor to be confirmed" do
      expect { subject.call }.to change { booking.tutor_confirmed }.from(false).to(true)
    end
  end

  context "when the student confirms the booking" do
    let (:profile) { booking.student_profile }

    it "will set the tutor to be confirmed" do
      expect { subject.call }.to change { booking.student_confirmed }.from(false).to(true)
    end
  end

  context "when both profiles have confirmed the booking" do
    let(:mail) { instance_double(ActionMailer::MessageDelivery) }

    before do
      expect(UserNotifier).to receive(:confirmed_tutor_booking).with(booking).and_return(mail)
      expect(UserNotifier).to receive(:confirmed_student_booking).with(booking).and_return(mail)
      expect(mail).to receive(:deliver_later).exactly(2).times
    end

    let(:profile) { booking.tutor_profile }

    it "will send emails to both the student and tutor" do
      booking.student_confirmed = true
      subject.call
    end
  end
end
