require 'rails_helper'

RSpec.describe HoldPayment, :type => :service do
  let(:booking) { FactoryGirl.create(:confirmed_booking) }

  subject(:hold_payment) { HoldPayment.new(booking) }

  it "will throw an error if the booking is cancelled" do
    expect { subject.call }.to_not raise_error
    booking.cancelled = true
    expect { subject.call }.to raise_error(InvalidParameters)
  end

  it "will throw an error if another held payment exists for the booking" do
    expect { subject.call }.to_not raise_error
    booking.held_payment = HeldPayment.create!(booking: booking)
    expect { subject.call }.to raise_error(InvalidParameters)
  end

  it "will throw an error if the booking is complete" do
    expect { subject.call }.to_not raise_error
    booking.completed = true
    expect { subject.call }.to raise_error(InvalidParameters)
  end

  it "will throw an error if the booking is not confirmed" do
    expect { subject.call }.to_not raise_error
    booking.tutor_confirmed = false
    expect { subject.call }.to raise_error(InvalidParameters)
  end

  it "will throw an error if the booking is not persisted" do
    expect { subject.call }.to_not raise_error
    booking = FactoryGirl.build(:confirmed_booking)
    hold_payment = HoldPayment.new(booking)
    expect { hold_payment.call }.to raise_error(InvalidParameters)
  end

  context "when an error is thrown" do
    it "will not save a new txn" do
      booking.price_cents = (booking.student_profile.balance_cents + 10)
      txn_count = Txn.count
      expect { subject.call }.to raise_error(ActiveRecord::RecordInvalid)
      expect(Txn.count).to eq txn_count
    end

    it "will not save a new held fund" do
      booking.price_cents = (booking.student_profile.balance_cents + 10)
      held_funds_count = HeldPayment.count
      expect { subject.call }.to raise_error(ActiveRecord::RecordInvalid)
      expect(HeldPayment.count).to eq held_funds_count
    end
  end

  it "will create a new transaction on the student profile" do
    expect { subject.call }.to change { booking.student_profile.txns.count }.by(1)
  end

  it "will remove funds from the student profile" do
    expect { subject.call }.to change { booking.student_profile.balance_cents }.by(-booking.price_cents)
  end

  it "will create a new held fund" do
    expect { subject.call }.to change { HeldPayment.count }.by(1)
  end
end
