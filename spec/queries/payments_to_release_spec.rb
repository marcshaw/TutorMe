require 'rails_helper'

RSpec.describe PaymentsToRelease, :type => :query do

  subject(:funds_to_release) { PaymentsToRelease.new }

  before do
    FactoryGirl.create(:held_payment, booking: booking)
  end

  context "when the booking occured 4 days ago" do
    let(:booking) { FactoryGirl.build(:uncompleted_booking, booking_params).tap { |booking| booking.save(validate: false) } }
    let(:booking_params) { { datetime: Time.now - 4.days } }

    it "will return the booking" do
      expect(subject.call).to include(booking)
    end
  end

  context "when the booking occured 3 days and 5 minutes ago" do
    let(:booking) { FactoryGirl.build(:uncompleted_booking, booking_params).tap { |booking| booking.save(validate: false) } }
    let(:booking_params) { { datetime: Time.now - 3.days - 5.minutes} }

    it "will return the booking" do
      expect(subject.call).to include(booking)
    end
  end

  context "when the booking occured 2 days and 2 hours ago" do
    let(:booking) { FactoryGirl.build(:uncompleted_booking, booking_params).tap { |booking| booking.save(validate: false) } }
    let(:booking_params) { { datetime: Time.now - 2.days - 2.hours} }

    it "will not return the booking" do
      expect(subject.call).to_not include(booking)
    end
  end

  context "when the booking is disputed" do
    let(:booking) { FactoryGirl.build(:disputed_booking, datetime: Time.now - 4.days, completed: false).tap { |booking| booking.save(validate: false) } }

    it "will not return the booking by default" do
      expect(subject.call).to_not include(booking)
    end
  end

  context "when the booking is uncompleted and unpaid" do
    let(:booking) { FactoryGirl.build(:unpaid_booking, datetime: Time.now - 4.days, completed: false).tap { |booking| booking.save(validate: false) } }

    it "will not return the booking by default" do
      expect(subject.call).to include(booking)
    end
  end

  context "when the booking is paid" do
    let(:booking) { FactoryGirl.build(:paid_booking, datetime: Time.now - 4.days).tap { |booking| booking.save(validate: false) } }

    it "will not return the booking by default" do
      expect(subject.call).to_not include(booking)
    end
  end

  context "when the booking is completed" do
    let(:booking) { FactoryGirl.build(:completed_booking, datetime: Time.now - 4.days).tap { |booking| booking.save(validate: false) } }

    it "will not return the booking by default" do
      expect(subject.call).to_not include(booking)
    end
  end
end
