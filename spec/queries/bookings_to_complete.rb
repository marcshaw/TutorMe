require 'rails_helper'

RSpec.describe BookingsToComplete, :type => :query do

  subject(:bookings_to_complete) { BookingsToComplete.new }
  let(:booking) { FactoryGirl.create(:confirmed_booking, booking_params) }

  context "when the booking occured 4 days ago" do
    let(:booking_params) { { datetime: Time.now - 4.days } }

    it "will return the booking" do
      expect(subject.call).to include(booking)
    end
  end

  context "when the booking occured 3 days and 5 minutes ago" do
    let(:booking_params) { { datetime: Time.now - 3.days - 5.minutes} }

    it "will return the booking" do
      expect(subject.call).to include(booking)
    end
  end

  context "when the booking occured 2 days and 2 hours ago" do
    let(:booking_params) { { datetime: Time.now - 2.days - 2.hours} }

    it "will not return the booking" do
      expect(subject.call).to_not include(booking)
    end
  end

  context "when the booking is disputed" do
    it "will not return the booking" do
      booking = FactoryGirl.create(:disputed_booking, datetime: Time.now - 4.days, completed: true)
      expect(subject.call).to_not include(booking)
    end
  end

  it "will use uncompleted and unpaid by default" do
    booking = FactoryGirl.create(:unpaid_booking, datetime: Time.now - 4.days, completed: false)
    expect(subject.call).to include(booking)
  end

  it "will not use paid booking by default" do
    booking = FactoryGirl.create(:paid_booking, datetime: Time.now - 4.days)
    expect(subject.call).to_not include(booking)
  end
end
