require 'rails_helper'

RSpec.describe BookingsToTakePayment, :type => :query do

  subject(:bookings_to_take_payment) { BookingsToTakePayment.new }
  let(:valid_date) { Time.now + BookingsToTakePayment::DAYS_BEFORE }

  before do
    FactoryGirl.create(:held_payment, booking: booking)
  end

  context "when a booking is 2 days before" do
    let(:booking) { FactoryGirl.build(:confirmed_booking, datetime: Time.now, paid: false, cancelled: false ).tap { |x| x.save(validate: false) } }

    it "will not return a booking that is 2 days before" do
      expect(subject.call).to_not include(booking)
    end
  end

  context "when a booking is uncancelled, confirmed and unpaid" do
    let(:booking) { FactoryGirl.create(:unpaid_booking, datetime: valid_date) }

    it "will use uncancelled, confirmed and unpaid by default" do
      expect(subject.call).to include(booking)
    end
  end

  context "when a booking is paid" do
    let(:booking) { FactoryGirl.create(:paid_booking, datetime: valid_date) }

    it "will not use paid booking by default" do
      expect(subject.call).to_not include(booking)
    end
  end

  context "when a booking is unconfirmed" do
    let(:booking) { FactoryGirl.create(:unconfirmed_booking, datetime: valid_date) }

    it "will not use unconfirmed booking by default" do
      expect(subject.call).to_not include(booking)
    end
  end

  context "when a booking is cancelled" do
    let(:booking) { FactoryGirl.create(:cancelled_booking, datetime: valid_date)}

    it "will not use cancelled booking by default" do
      expect(subject.call).to_not include(booking)
    end
  end
end
