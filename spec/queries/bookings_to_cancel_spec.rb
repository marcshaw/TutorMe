require 'rails_helper'

RSpec.describe BookingsToCancel, :type => :query do

  subject(:bookings_to_cancel) { BookingsToCancel.new }

  it "will return the bookings that was meant to be 5 minutes ago" do
    booking = FactoryGirl.build(:unpaid_booking, datetime: Time.now - 5.minutes).tap { |booking| booking.save(validate: false) }
    expect(subject.call).to include(booking)
  end

  it "will return the bookings that are in the past" do
    booking = FactoryGirl.build(:unpaid_booking, datetime: Time.now - 2.days).tap { |booking| booking.save(validate: false) }
    expect(subject.call).to include(booking)
  end

  it "will not return the bookings that are further away than 1 day" do
    booking = FactoryGirl.build(:unpaid_booking, datetime: Time.now + 2.days).tap { |booking| booking.save(validate: false) }
    expect(subject.call).to_not include(booking)
  end

  it "will return the bookings that are 1 day away" do
    booking = FactoryGirl.build(:unpaid_booking, datetime: Time.now + 1.days - 1.minute).tap { |booking| booking.save(validate: false) }
    expect(subject.call).to include(booking)
  end

  it "will use uncancelled, uncompleted and unpaid by default" do
    booking = FactoryGirl.build(:unpaid_booking, datetime: Time.now - 2.days, completed: false, cancelled: false ).tap { |booking| booking.save(validate: false) }
    expect(subject.call).to include(booking)
  end

  it "will not use paid booking by default" do
    booking = FactoryGirl.build(:paid_booking, datetime: Time.now - 2.days).tap { |booking| booking.save(validate: false) }
    expect(subject.call).to_not include(booking)
  end

  it "will not use completed booking by default" do
    booking = FactoryGirl.build(:completed_booking, datetime: Time.now - 2.days).tap { |booking| booking.save(validate: false) }
    expect(subject.call).to_not include(booking)
  end

  it "will not use cancelled booking by default" do
    booking = FactoryGirl.build(:cancelled_booking, datetime: Time.now - 2.days).tap { |booking| booking.save(validate: false) }
    expect(subject.call).to_not include(booking)
  end
end
