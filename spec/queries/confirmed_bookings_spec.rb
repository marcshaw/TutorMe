require 'rails_helper'

RSpec.describe ConfirmedBookings, :type => :query do

  subject(:confirmed_bookings) { ConfirmedBookings.new(service_params) }

  let(:booking) { FactoryGirl.create(:confirmed_booking, booking_params) }

  context "when we are querying for confirmed tutor booking" do
    let(:booking_params) { { tutor_confirmed: true } }
    let(:service_params) { { tutor: true  } }

    it "will return confirmed tutor booking" do
      expect(subject.call).to include(booking)
    end
  end

  context "when we are querying for confirmed student booking" do
    let(:booking_params) { { student_confirmed: true } }
    let(:service_params) { { student: true  } }

    it "will return confirmed student booking" do
      expect(subject.call).to include(booking)
    end
  end

  context "when we are querying for unconfirmed tutor booking" do
    let(:booking_params) { { tutor_confirmed: false } }
    let(:service_params) { { tutor: false  } }

    it "will return confirmed student booking" do
      expect(subject.call).to include(booking)
    end
  end

  context "when we are querying for unconfirmed student booking" do
    let(:booking_params) { { student_confirmed: false } }
    let(:service_params) { { student: false  } }

    it "will return confirmed student booking" do
      expect(subject.call).to include(booking)
    end
  end
end
