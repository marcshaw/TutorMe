require 'rails_helper'
require 'support/controllers_helpers'

describe BookingsController do
  include Devise::Test::ControllerHelpers
  include ControllerHelpers

  before(:each) do
    @booking = FactoryGirl.create(:unconfirmed_booking)
  end

  describe "GET #confirm" do
    it "should confirm the tutor side of the booking" do
      sign_in @booking.tutor_profile
      get :confirm, params: { :profile_id => @booking.tutor_profile.id, :booking_id => @booking.id }
      @booking.reload
      expect(@booking.tutor_confirmed).to eq(true)
    end

    it "should confirm the student side of the booking" do
      sign_in @booking.student_profile
      get :confirm, params: { :profile_id => @booking.student_profile.id, :booking_id => @booking.id }
      @booking.reload
      expect(@booking.student_confirmed).to eq(true)
    end

    it "should not be confirmed because the user is not signed in" do
      get :confirm, params: { :profile_id => @booking.student_profile.id, :booking_id => @booking.id }
      @booking.reload
      expect(@booking.student_confirmed).to eq(false)
    end
  end
end

