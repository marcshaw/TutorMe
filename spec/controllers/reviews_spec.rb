require 'rails_helper'
require 'support/controllers_helpers'

describe ReviewsController do
  include Devise::Test::ControllerHelpers
  include ControllerHelpers

  before(:each) do
    @review = FactoryGirl.create(:review)
    @booking = FactoryGirl.create(:completed_booking, student_profile: @review.reviewer, tutor_profile: @review.profile)
  end

  describe "GET #edit" do
    context "with invalid parameters" do
      it "should not allow a user to edit a booking where they are not the reviewer" do
        sign_in @review.profile
        get :edit, params: { :profile_id => @booking.tutor_profile.id, :id => @review.token }
        expect(response).to redirect_to(error_path)
        expect(flash[:error]).to eq("There seems to be a mixup with reviews. Please contact us")
      end
    end

    context "with valid parameters" do
      it "should allow a user to edit a booking" do
        sign_in @review.reviewer
        get :edit, params: { :profile_id => @booking.tutor_profile.id, :id => @review.token }
        expect(response).to render_template(:edit)
      end
    end
  end
end

