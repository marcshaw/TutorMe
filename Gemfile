source 'https://rubygems.org' do

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
  gem 'rails', '5.0.2'
# Use SCSS for stylesheets
  gem 'sass-rails'
# Use Uglifier as compressor for JavaScript assets
  gem 'uglifier'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
  gem 'stripe'

  #javascript mixins
  gem 'bourbon'
  #Profiler
  gem "skylight"

  gem 'active_link_to'
  gem 'autoprefixer-rails'
  gem 'font-awesome-sass'

#Authentication
  gem 'devise'
  gem 'devise-bootstrap-views'

#Email
  gem 'sendgrid-ruby'
  gem 'nokogiri'
  gem 'premailer-rails'
  gem 'sendgrid-actionmailer'

#Attachment in the models (ie images)
  gem 'paperclip', '~> 5.0'
  gem 'aws-sdk', '~> 2.9.0'

  gem 'rspec-collection_matchers'

# Use jquery as the JavaScript library
  gem 'jquery-rails'
  gem 'jquery-ui-rails'

#Autocomplete search
  gem 'rails-jquery-autocomplete'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
 # gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
  gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
  gem 'bcrypt', '~> 3.1.7'

#Solves turbo links messing with autocomplete js
#  gem 'jquery-turbolinks'

  #Currency formatting gem
  gem 'autonumeric-rails'

#Currency gem
  gem 'money-rails'

  gem 'pg'
#Pagination Gem
  gem 'will_paginate', '~> 3.1.0'

  gem 'compass-rails'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
  gem 'capistrano', '~> 3.7', '>= 3.7.1'
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano-passenger', '~> 0.2.0'
  gem 'capistrano-rbenv', '~> 2.1'

  gem 'whenever', :require => false

  group :development, :test do
    # Call 'byebug' anywhere in the code to stop execution and get a debugger console
    gem 'byebug'
    gem "factory_girl_rails"
    gem 'rspec-rails'
    gem 'shoulda-matchers'
    gem "spring"
    gem 'cucumber-rails', :require => false
    gem 'poltergeist'
    #gem 'selenium-webdriver'
    gem 'database_cleaner'
    gem 'rails-controller-testing'
  end

  group :development do
    # Access an IRB console on exception pages or by using <%= console %> in views
    gem 'web-console', '~> 2.0'
    gem 'better_errors'
  end

  group :production do
  end

  # Windows does not include zoneinfo files, so bundle the tzinfo-data gem
  gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

end


#These are the bower packages.
#See https://rails-assets.org/ for info
source 'https://rails-assets.org' do

  gem 'rails-assets-bootstrap'
  gem 'rails-assets-jquery-bar-rating'
  gem 'rails-assets-nouislider'
  gem 'rails-assets-jquery.dotdotdot'
  gem 'rails-assets-malihu-custom-scrollbar-plugin'
  gem 'rails-assets-jquery-timepicker-jt'
end
