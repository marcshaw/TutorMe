require 'test_helper'

class ProfileTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
"#{
validates :course_codes, format: {with: /\A((([a-zA-Z]{1,4}[\d]{3})[,]?)|)+\z/,
                                  message: "Please use up to 4 letters, followed by 3 numbers. Ie, Comp102 "}
validates :description, length: {maximum: 500,
                                 too_long: "%{count} characters is the maximum allowed for the description"}

validates :mediums , presence: true, allow_blank: false, if: 'active_tutor == true'
validates :university , presence: true, allow_blank: false, if: 'active_tutor == true'
validates :rate_cents , presence: true, allow_blank: false, if: 'active_tutor == true'
validates :tutoring_years , presence: true, allow_blank: false, if: 'active_tutor == true'
validates :description , presence: true, allow_blank: false, if: 'active_tutor == true'
validates :tutoring_subjects , presence: true, allow_blank: false, if: 'active_tutor == true'
validates :availability , presence: true, allow_blank: false, if: 'active_tutor == true'

validate :tutoring_subjects_exist
validate :validate_availability
}"