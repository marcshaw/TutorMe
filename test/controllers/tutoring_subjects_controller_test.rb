require 'test_helper'

class TutoringSubjectsControllerTest < ActionController::TestCase
  setup do
    @tutoring_subject = tutoring_subjects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tutoring_subjects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tutoring_subject" do
    assert_difference('TutoringSubject.count') do
      post :create, tutoring_subject: { name: @tutoring_subject.name }
    end

    assert_redirected_to tutoring_subject_path(assigns(:tutoring_subject))
  end

  test "should show tutoring_subject" do
    get :show, id: @tutoring_subject
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tutoring_subject
    assert_response :success
  end

  test "should update tutoring_subject" do
    patch :update, id: @tutoring_subject, tutoring_subject: { name: @tutoring_subject.name }
    assert_redirected_to tutoring_subject_path(assigns(:tutoring_subject))
  end

  test "should destroy tutoring_subject" do
    assert_difference('TutoringSubject.count', -1) do
      delete :destroy, id: @tutoring_subject
    end

    assert_redirected_to tutoring_subjects_path
  end
end
