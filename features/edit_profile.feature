Feature: Edit my profile
  Becoming an active tutor
  Stop tutoring

  @javascript
  Scenario: I can become an active tutor
    Given I login as a user
    And there exists a tutor
    When I click the "Profile" link
    And I click the "Edit Profile" link
    And I enter my profile details
    And I click the "Submit" button
    And I login as the tutor
    And I click the "Search" link
    And I enter "Comp222" into "search[tutoring_subject]"
    And I click the "Search" button
    Then I should see the users name

  @javascript
  Scenario: I can stop being a tutor
    Given I login as a user
    And there exists a tutor
    When I click the "Search" link
    And I enter "Comp123" into "search[tutoring_subject]"
    And I click the "Search" button
    Then I should see the tutors name
    When I login as the tutor
    And I click the "Profile" link
    And I click the "Edit Profile" link
    And I deselect the active tutor box
    And I click the "Submit" button
    And I click the "Logout" link
    And I login as a user
    When I click the "Search" link
    And I enter "Comp123" into "search[tutoring_subject]"
    And I click the "Search" button
    Then I should not see the tutors name
