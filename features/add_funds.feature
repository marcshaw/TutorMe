Feature: Add Funds
  Funds will be added to their total if the transaction succeeds
  Funds will not be added to their total if the transaction fails

  @javascript
  Scenario: Add $20 to my account
    Given I login as a user
    And I click the "Funds" link
    When I click the "Add funds" link
    And I enter in valid credit card details
    And I enter "20" into the amount
    And I click the "Pay" button
    Then I should see my Deposit of "20"

  @javascript
  Scenario: Fail to add $10 to my account
    Given I login as a user
    And I click the "Funds" link
    When I click the "Add funds" link
    And I enter in valid credit card details
    And I enter "10" into the amount
    And I click the "Pay" button
    Then I should see "Please enter an amount between $20 - $50"
    And there should be no Deposit's for the user

  @javascript
  Scenario: Fail the CVC check
    Given I login as a user
    And I click the "Funds" link
    When I click the "Add funds" link
    And I enter in credit card details where the CVC check fails
    And I enter "20" into the amount
    And I click the "Pay" button
    Then I should see "Your card's security code is incorrect."
    And there should be no Deposit's for the user

  @javascript
  Scenario: Credit card is declined
    Given I login as a user
    And I click the "Funds" link
    When I click the "Add funds" link
    And I enter in credit card details that will be declined
    And I enter "20" into the amount
    And I click the "Pay" button
    Then I should see "Your card was declined."
    And there should be no Deposit's for the user

  @javascript
  Scenario: Credit card is declined for fradulent reasons
    Given I login as a user
    And I click the "Funds" link
    When I click the "Add funds" link
    And I enter in credit card details that will be declined for fradulent reasons
    And I enter "20" into the amount
    And I click the "Pay" button
    Then I should see "Your card was declined."
    And there should be no Deposit's for the user

  @javascript
  Scenario: Credit card is declined for incorrect cvc
    Given I login as a user
    And I click the "Funds" link
    When I click the "Add funds" link
    And I enter in credit card details that will be declined for incorrect cvc
    And I enter "20" into the amount
    And I click the "Pay" button
    Then I should see "Your card's security code is incorrect."
    And there should be no Deposit's for the user

  @javascript
  Scenario: Credit card is declined for expired card
    Given I login as a user
    And I click the "Funds" link
    When I click the "Add funds" link
    And I enter in credit card details that will be declined for expired card
    And I enter "20" into the amount
    And I click the "Pay" button
    Then I should see "Your card has expired."
    And there should be no Deposit's for the user

  @javascript
  Scenario: Credit card is declined for processing error
    Given I login as a user
    And I click the "Funds" link
    When I click the "Add funds" link
    And I enter in credit card details that will be declined for processing error
    And I enter "20" into the amount
    And I click the "Pay" button
    Then I should see "An error occurred while processing your card. Try again in a little bit."
    And there should be no Deposit's for the user
