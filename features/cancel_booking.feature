Feature: Cancel a new booking

  @javascript
  Scenario: A user cancels a booking
    Given I login as a user
    And there exists a tutor
    And I navigate to the users bookings
    And I click the "Student" link
    And I should see "NO UNCONFIRMED BOOKINGS"
    And I navigate to the tutors profile
    And I click the "Book A Session" link
    When I enter in the bookings details
    And I click the "Send Booking Request" button
    And I cancel the booking
    And I refresh the page
    And I should see "THE BOOKING HAS BEEN CANCELLED"
