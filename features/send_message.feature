Feature: Send a tutor a booking
  Send a message through the messages url
  Send a message from the search
  Send a message from the tutors profile

  @javascript
  Scenario: Message a user from their profile
    Given I login as a user
    And there exists a tutor
    And I navigate to the tutors profile
    When I send a message through the popup saying "hello" to the tutor
    And I click the "Messages" link
    Then I should not see "NO CONVERSATIONS"
    And I see my message to the tutor

  @javascript
  Scenario: Message a user from their profile
    Given I login as a user
    And there exists a tutor
    And I navigate to "search/new"
    And I select the "In Person" checkbox
    And I click the "Search" button
    When I send a message through the popup saying "hello" to the tutor
    And I click the "Messages" link
    Then I should not see "NO CONVERSATIONS"
    And I see my message to the tutor

  @javascript
  Scenario: Message a user from the messages link
    Given I login as a user
    And there exists a tutor
    And I navigate to "search/new"
    And I select the "In Person" checkbox
    And I click the "Search" button
    When I send a message through the popup saying "hello" to the tutor
    And I click the "Messages" link
    And I send a message through the messages ui saying "hi" to the tutor
    Then I should see "hi"
