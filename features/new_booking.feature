Feature: Create a new booking
  The created booking will display for both users

  @javascript
  Scenario: A user creates a booking
    Given I login as a user
    And there exists a tutor
    And I navigate to the users bookings
    And I click the "Student" link
    And I should see "NO UNCONFIRMED BOOKINGS"
    And I navigate to the tutors profile
    And I click the "Book A Session" link
    When I enter in the bookings details
    And I click the "Send Booking Request" button
    And I navigate to the users bookings
    And I click the "Student" link
    Then I should not see "NO UNCONFIRMED BOOKINGS"

    When I login as the tutor
    And I navigate to the tutors bookings
    Then I should not see "NO UNCONFIRMED BOOKINGS"

  @javascript
  Scenario: A user creates a booking
    Given I login as a user
    And there exists a tutor
    And I navigate to the users bookings
    And I click the "Student" link
    And I should see "NO UNCONFIRMED BOOKINGS"
    And I navigate to "search/new"
    And I select the "In Person" checkbox
    And I click the "Search" button
    And I click the "View profile" link
    And I click the "Book A Session" link
    When I enter in the bookings details
    And I click the "Send Booking Request" button
    And I navigate to the users bookings
    And I click the "Student" link
    Then I should not see "NO UNCONFIRMED BOOKINGS"

    When I login as the tutor
    And I navigate to the tutors bookings
    Then I should not see "NO UNCONFIRMED BOOKINGS"

  @javascript
  Scenario: A user creates a booking that is confirmed
    Given I login as a user
    And there exists a tutor
    And I navigate to the users bookings
    And I click the "Student" link
    And I should see "NO UNCONFIRMED BOOKINGS"
    And I navigate to the tutors profile
    And I click the "Book A Session" link
    When I enter in the bookings details
    And I click the "Send Booking Request" button
    And I click the "Confirm" link
    And I login as the tutor
    And I navigate to the tutors bookings
    And I click the "View Booking" link
    And I click the "Confirm" link
    And I navigate to the tutors bookings
    Then I should not see "NO CONFIRMED BOOKINGS"

    When I click the "Logout" link
    And I login as a user
    And I navigate to the users bookings
    And I click the "Student" link
    Then I should not see "NO CONFIRMED BOOKINGS"
