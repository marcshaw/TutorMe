Feature: I can leave a review for a tutor and see it on their profile

  @javascript
  Scenario: I can find a tutor using a course code
    Given I login as a user
    And there exists a tutor
    And there exists a booking between the user and the tutor
    And there exists a review for the booking
    And I navigate to the review
    When I give the tutor 5 stars
    And I enter "Good Guy. Top Bloke" into "review[body]"
    And I click the "Create Review" button
    And I navigate to the tutors profile
    And I should see one review
    And I should see a 5 star review
    And I should see "Good Guy. Top Bloke"
