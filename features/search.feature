Feature: Search for a tutor
  Based on course code
  Based on subject
  Based on university
  Based on price
  Based on tutoring year
  Tutor on the second page

  @javascript
  Scenario: I can find a tutor using a course code
    Given I login as a user
    And there exists a tutor
    And there exists another tutor that tutors "Math123"
    When I click the "Search" link
    And I enter "Comp123" into "search[tutoring_subject]"
    And I click the "Search" button
    Then I should see the tutors name
    And I should not see the other tutors name

  @javascript
  Scenario: I can find a tutor using a subject
    Given I login as a user
    And there exists a tutor
    And there exists another tutor that tutors "Math123"
    When I click the "Search" link
    And I enter "Mathematics" into "search[tutoring_subject]"
    And I click the "Search" button
    Then I should see the tutors name
    And I should not see the other tutors name

  @javascript
  Scenario: I can find a tutor based on price
    Given I login as a user
    And there exists a tutor
    And there exists another tutor that tutors "comp123"
    When I click the "Search" link
    And I enter "Comp123" into "search[tutoring_subject]"
    And I click the "Search" button
    Then I should see the tutors name
    And I should see the other tutors name
    When I set the max price to be $40
    And I click the "Search" button
    Then I should see the tutors name
    And I should not see the other tutors name

  @javascript
  Scenario: I can find a tutor based on university
    Given I login as a user
    And there exists a tutor
    And there exists another tutor that tutors "comp123"
    When I click the "Search" link
    And I enter "Victoria University of Wellington" into "search[university]"
    And I click the "Search" button
    Then I should see the tutors name
    And I should not see the other tutors name

  @javascript
  Scenario: I can find a tutor based on year
    Given I login as a user
    And there exists a tutor
    And there exists another tutor that tutors "comp123"
    When I click the "Search" link
    And I search based on First Year tutors
    And I click the "Search" button
    Then I should see the tutors name
    And I should not see the other tutors name

  @javascript
  Scenario: I can find a tutor on the second page
    Given I login as a user
    And there exists a tutor
    And there exists 20 tutors that tutors "comp123"
    When I click the "Search" link
    And I enter "Comp123" into "search[tutoring_subject]"
    And I click the "Search" button
    Then I should see the tutors name
    When I click the next page button
    Then I should not see the tutors name
