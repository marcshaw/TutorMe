Given(/^there exists a tutor$/) do
  @tutor = FactoryGirl.create(:tutor)
end

Given(/^I navigate to the users bookings$/) do
  step("I navigate to \"profiles/#{@user.id}/bookings\"")
end

Given(/^I navigate to the tutors profile$/) do
  step("I navigate to \"profiles/#{@tutor.id}/\"")
end

When(/^I enter in the bookings details$/) do
  step("I click the \".dopbcp-next-btn\" class")
  step("I click the \"0_#{monday_in_next_month}\" id")
  step("I enter \"10:30am\" into \"booking[time]\"")
  step("I enter \"027027027\" into \"booking[contact_number]\"")
  step("I select \"Mathematics\" from the \"booking[subject]\" select box")
  step("I enter \"Comp123\" into \"booking[course_code]\"")
  step("I enter \"Vic\" into \"booking[university]\"")
end

When(/^I login as the tutor$/) do
  step('I click the "Logout" link')
  step('I navigate to "/"')
  step('I click the "Sign In" link')
  step("I enter \"#{@tutor.email}\" into \"profile_email\"")
  step('I enter "marcmarc" into "profile_password"')
  step('I click the "Sign in" button')
end

When(/^I navigate to the tutors bookings$/) do
  step("I navigate to \"profiles/#{@tutor.id}/bookings\"")
end

When(/^I refresh the page$/) do
  visit(current_url)
end

When(/^I cancel the booking$/) do
  step('I click the "Cancel Booking" link')
  step('I click the "Yes" link')
end


def monday_in_next_month
  today = Time.now
  next_month = Time.new(today.year, today.month + 1, 15)
  next_month.next_week(:monday).strftime("%F")
end
