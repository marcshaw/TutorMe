Given(/^I login as a user$/) do
  @user ||= FactoryGirl.create(:profile)
  step('I navigate to "/"')
  step('I click the "Sign In" link')
  step("I enter \"#{@user.email}\" into \"profile_email\"")
  step('I enter "marcmarc" into "profile_password"')
  step('I click the "Sign in" button')
end

Given(/^I enter "([^"]*)" into "([^"]*)"$/) do |value, field|
  fill_in(field, with: value)
end

Given(/^I select "([^"]*)" from the "([^"]*)" select box$/) do |value, field|
  select(value, from: field)
end

Given(/^I navigate to "([^"]*)"$/) do |url|
  visit(url)
end

Given(/^I click the "([^"]*)" link$/) do |link|
  click_link(link)
end

Given(/^I click the "([^"]*)" class$/) do |class_name|
  Capybara.using_wait_time(20) do
    find(:css, class_name).click
  end
end

Given(/^I click the "([^"]*)" id$/) do |id|
  find_by_id(id).click
end

When(/^I click the "([^"]*)" button$/) do |button|
  click_button(button)
end

Then(/^I should see "([^"]*)"$/) do |content|
  Capybara.using_wait_time(20) do
    expect(page).to have_content(content)
  end
end

Then(/^I should not see "([^"]*)"$/) do |content|
  expect(page).to_not have_content(content)
end

When(/^I select the "([^"]*)" checkbox$/) do |checkbox|
  check(checkbox)
end

When(/^I deselect the "([^"]*)" checkbox$/) do |checkbox|
  uncheck(checkbox)
end

When(/^I pause$/) do
  byebug
end
