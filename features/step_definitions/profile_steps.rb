When(/^I enter my profile details$/) do
  step("I enter \"Hi!\" into \"profile[description]\"")
  step("I select the \"profile_tutoring_year_ids_1\" checkbox")
  step("I select the \"profile_active_tutor\" checkbox")
  step("I select the \"profile_medium_ids_1\" checkbox")
  step("I select the \"profile_availability_ids_1\" checkbox")
  step("I enter \"0242232322\" into \"profile[phone_number]\"")
  step("I enter \"20\" into \"profile[rate_cents]\"")
  step("I enter \"Comp222\" into \"profile[course_codes]\"")
  step("I enter \"Victoria University of Wellington\" into \"profile[university]\"")
end

When(/^I deselect the active tutor box$/) do
  step("I deselect the \"profile_active_tutor\" checkbox")
end
