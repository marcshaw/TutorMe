Given(/^there exists a booking between the user and the tutor$/) do
  @booking = FactoryGirl.create(:completed_booking, tutor_profile: @tutor, student_profile: @user)
end

Given(/^there exists a review for the booking$/) do
  @review = NewReview.new(@booking.tutor_profile, @booking.student_profile, @booking).call
end

Given(/^I navigate to the review$/) do
  step("I navigate to \"#{edit_profile_review_path(@booking.tutor_profile.id, @review.token)}\"")
end

When(/^I give the tutor (\d+) stars$/) do |arg1|
  page.execute_script("$('#review_rating').val('5')")
end

Then(/^I should see one review$/) do
  expect(page).to have_css('.review', count: 1)
end

Then(/^I should see a (\d+) star review$/) do |arg1|
  Capybara.ignore_hidden_elements = false
  expect(page).to have_css(".br-current-rating", text: "5")
  Capybara.ignore_hidden_elements = true
end
