When(/^I send a message through the popup saying "([^"]*)" to the tutor$/) do |message|
  @message = message
  step("I click the \"Message #{@tutor.first_name}\" link")
  step("I enter \"#{message}\" into \"message\"")
  step("I click the \"Send\" link")
  sleep(1)
  step("I click the \"title\" id")
end

Then(/^I see my message to the tutor$/) do
  step("I click the \".name\" class")
  step("I should see \"#{@message}\"")
end

When(/^I send a message through the messages ui saying "([^"]*)" to the tutor$/) do |message|
  step("I click the \".name\" class")
  find(:css, '.message_input').set message
  find(:css, '.send').trigger('click')
end
