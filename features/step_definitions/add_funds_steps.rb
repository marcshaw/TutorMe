When(/^I enter in valid credit card details$/) do
  step("I enter default credit card info with the card number as \"4242424242424242\"")
end

When(/^I enter "([^"]*)" into the amount$/) do |amount|
  step("I enter \"#{amount}\" into \"txn_amount_cents\"")
end

Then(/^I should see my (Deposit|Withdrawl) of "([^"]*)"$/) do |type, amount|
  Capybara.using_wait_time(20) do
    expect(page).to have_content(type)
    expect(page).to have_content(amount)
  end
end

Then(/^there should be no (Deposit|Withdrawl)'s for the user$/) do |type|
  step("I click the \"Funds\" link")
  expect(page).not_to have_content(type)
end

When(/^I enter in credit card details where the CVC check fails$/) do
  step("I enter default credit card info with the card number as \"4000000000000101\"")
end

When(/^I enter in credit card details that will be declined$/) do
  step("I enter default credit card info with the card number as \"4000000000000002\"")
end

When(/^I enter in credit card details that will be declined for fradulent reasons$/) do
  step("I enter default credit card info with the card number as \"4100000000000019\"")
end

When(/^I enter in credit card details that will be declined for incorrect cvc$/) do
  step("I enter default credit card info with the card number as \"4000000000000127\"")
end

When(/^I enter in credit card details that will be declined for expired card$/) do
  step("I enter default credit card info with the card number as \"4000000000000069\"")
end

When(/^I enter in credit card details that will be declined for processing error$/) do
  step("I enter default credit card info with the card number as \"4000000000000119\"")
end

When(/^I enter default credit card info with the card number as "([^"]*)"$/) do |number|
  step('I enter "marc" into "cardholder-name"')

  stripe_iframe = all('iframe').last
  Capybara.within_frame stripe_iframe do
    step("I enter \"#{number}\" into \"cardnumber\"")
    step('I enter "1219" into "exp-date"')
    step('I enter "424" into "cvc"')
  end
end
