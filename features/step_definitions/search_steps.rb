Then(/^I should see the tutors name$/) do
  step("I should see \"#{@tutor.first_name.humanize}\"")
end

Given(/^there exists another tutor that tutors "([^"]*)"$/) do |course_code|
  @other_tutor = FactoryGirl.create(:tutor, first_name: "jimm",
                                    tutoring_subjects: [FactoryGirl.create(:tutoring_subject, name: "Physics")],
                                    mediums: [FactoryGirl.create(:medium, name: "Online")],
                                    course_codes: [FactoryGirl.create(:course_code, course_code: course_code)],
                                    university: FactoryGirl.create(:university, name: "Massey University Wellington"),
                                    tutoring_years: [FactoryGirl.create(:tutoring_year, name: "Second")],
                                    rate_cents: 8000
                                   )
end

Then(/^I should not see the other tutors name$/) do
  step("I should not see \"#{@other_tutor.first_name.humanize}\"")
end

Then(/^I should see the other tutors name$/) do
  step("I should see \"#{@other_tutor.first_name.humanize}\"")
end

When(/^I set the max price to be \$(\d+)$/) do |price|
  page.execute_script("$('#search_max_rate_cents').val(50)")
end

When(/^I search based on First Year tutors$/) do
  step("I select the \"search_tutoring_year_1\" checkbox")
end

Given(/^there exists (\d+) tutors that tutors "([^"]*)"$/) do |number, course_code|
  (0..number.to_i).each do |i|
    FactoryGirl.create(:tutor, first_name: "john#{i}")
  end
end

When(/^I click the next page button$/) do
  step("I click the \".next_page\" class")
end

Then(/^I should not see the tutors name$/) do
  step("I should not see \"#{@tutor.first_name.humanize}\"")
end

Then(/^I should see the users name$/) do
  step("I should see \"#{@user.first_name.humanize}\"")
end
