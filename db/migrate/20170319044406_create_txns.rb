class CreateTxns < ActiveRecord::Migration
  def change
    create_table :txns do |t|
      t.timestamps null: false
      t.monetize :amount, null: false
      t.integer :txn_type, null: false
      t.decimal :gst
      t.belongs_to :profile, null: false
    end
  end
end
