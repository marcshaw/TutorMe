class CreateCourseCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :course_codes do |t|
      t.string :course_code, index: true
      t.belongs_to :profile
    end
  end
end
