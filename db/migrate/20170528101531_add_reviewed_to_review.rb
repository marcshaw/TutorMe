class AddReviewedToReview < ActiveRecord::Migration[5.0]
  def change
    add_column :reviews, :reviewed, :boolean, null: false, default: false
  end
end
