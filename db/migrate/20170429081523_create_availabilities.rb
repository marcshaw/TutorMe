class CreateAvailabilities < ActiveRecord::Migration[5.0]
  def change
    create_table :availabilities do |t|
      t.string :day
      t.belongs_to :profile
      t.timestamps null: false
    end
  end
end
