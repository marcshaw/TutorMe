class AddProfileToTutoringSubjects < ActiveRecord::Migration[5.0]
  def change
    add_reference :tutoring_subjects, :profile, index: true
  end
end
