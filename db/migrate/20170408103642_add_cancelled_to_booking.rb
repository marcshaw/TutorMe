class AddCancelledToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :cancelled, :boolean, null: false, default: false
  end
end
