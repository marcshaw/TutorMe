class CreateTutoringSubjects < ActiveRecord::Migration
  def change
    create_table :tutoring_subjects do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
