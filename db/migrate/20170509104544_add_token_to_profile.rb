class AddTokenToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :token, :text
  end
end
