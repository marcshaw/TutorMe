class CreateHeldFunds < ActiveRecord::Migration[5.0]
  def change
    create_table :held_funds do |t|
      t.belongs_to :booking, null: false
      t.boolean :tutor_accepted, null: false, default: false
      t.boolean :student_accepted, null: false, default: false
    end
  end
end
