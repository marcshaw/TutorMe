class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :body
      t.integer :rating
      t.references :reviewer
      t.belongs_to :profile
      t.timestamps null: false
    end
  end
end
