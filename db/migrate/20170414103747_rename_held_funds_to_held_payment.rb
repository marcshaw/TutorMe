class RenameHeldFundsToHeldPayment < ActiveRecord::Migration[5.0]
  def change
    rename_table :held_funds, :held_payments
  end
end
