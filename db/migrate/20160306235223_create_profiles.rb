class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|

      t.timestamps null: false
      t.text :description
      t.string :course_codes, array:true
      t.attachment :avatar
      t.boolean :active_tutor, default: false
      t.belongs_to :user
      t.string :tutoring_subjects, array:true
      t.monetize :rate
      t.string :first_name
      t.belongs_to :university
      t.string :availability, array:true
    end
  end
end
