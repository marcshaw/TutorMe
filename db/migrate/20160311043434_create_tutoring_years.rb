class CreateTutoringYears < ActiveRecord::Migration
  def change
    create_table :tutoring_years do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
