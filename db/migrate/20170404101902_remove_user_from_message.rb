class RemoveUserFromMessage < ActiveRecord::Migration[5.0]
  def change
    remove_reference :messages, :user
    add_reference :messages, :profile
  end
end
