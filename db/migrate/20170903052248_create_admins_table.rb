class CreateAdminsTable < ActiveRecord::Migration[5.0]
  def change
    create_table :admins do |t|
      t.belongs_to :profile
      t.timestamps null: false
    end
  end
end
