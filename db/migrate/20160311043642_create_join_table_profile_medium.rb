class CreateJoinTableProfileMedium < ActiveRecord::Migration
  def change
    create_join_table :media, :profiles do |t|
      # t.index [:medium_id, :tutor_id]
      # t.index [:tutor_id, :medium_id]
    end
  end
end
