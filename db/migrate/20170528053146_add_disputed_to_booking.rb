class AddDisputedToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :disputed, :boolean, null: false, default: false
  end
end
