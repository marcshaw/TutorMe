class CreateJoinTableProfilesTutoringSubjects < ActiveRecord::Migration[5.0]
  def change
    create_join_table :profiles, :tutoring_subjects do |t|
      #t.index [:profile_id, :tutoring_subject_id]
      #t.index [:tutoring_subject_id, :profile_id]
    end
  end
end
