class RemoveColumnsFromProfile < ActiveRecord::Migration[5.0]
  def change
    remove_column :profiles, :course_codes
    remove_column :profiles, :tutoring_subjects
    remove_column :profiles, :availability
  end
end
