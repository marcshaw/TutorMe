class AddCompletedToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :completed, :boolean, null: false, default: false
  end
end
