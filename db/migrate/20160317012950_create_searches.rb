class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.monetize :min_rate
      t.monetize :max_rate
      t.string :university
      t.string :tutoring_subject
      t.string :media
      t.string :course_code
      t.string :tutoring_year
      t.timestamps null: false
    end
  end
end
