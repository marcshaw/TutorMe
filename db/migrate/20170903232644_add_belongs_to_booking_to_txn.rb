class AddBelongsToBookingToTxn < ActiveRecord::Migration[5.0]
  def change
    add_reference :txns, :booking, index: true, null: true
  end
end
