class RemoveUserFromProfile < ActiveRecord::Migration[5.0]
  def change
    remove_belongs_to :profiles, :user
  end
end
