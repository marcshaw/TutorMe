class AddTutorRateToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :tutor_rate, :decimal, null: false
  end
end
