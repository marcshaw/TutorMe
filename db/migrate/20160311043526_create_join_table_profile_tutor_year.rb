class CreateJoinTableProfileTutorYear < ActiveRecord::Migration
  def change
    create_join_table :tutoring_years, :profiles do |t|
      # t.index [:tutoring_year_id, :tutor_id]
      # t.index [:tutor_id, :tutoring_year_id]
    end
  end
end
