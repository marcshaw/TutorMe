class CreateBookingMessages < ActiveRecord::Migration
  def change
    create_table :booking_messages do |t|
      t.string :message

      t.references :from, index: true
      t.belongs_to :booking
      t.timestamps null: false
    end
  end
end
