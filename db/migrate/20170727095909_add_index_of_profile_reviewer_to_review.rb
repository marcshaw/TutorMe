class AddIndexOfProfileReviewerToReview < ActiveRecord::Migration[5.0]
  def change
    add_index :reviews, [:profile_id, :reviewer_id], unique: true
  end
end
