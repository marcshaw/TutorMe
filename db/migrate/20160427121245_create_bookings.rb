class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.datetime :datetime
      t.integer :time_length
      t.string :contact_number

      t.string :subject
      t.string :course_code
      t.monetize :price

      t.string :medium
      t.string :tutoring_year
      t.string :university

      t.references :tutor_profile
      t.references :student_profile

      t.boolean :student_confirmed, default: false
      t.boolean :tutor_confirmed, default: false

      t.timestamps null: false
    end
  end
end
