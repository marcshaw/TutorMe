class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.belongs_to :profile, null: false
      t.integer :notification_type, null: false
      t.integer :notification_type_id, null: false
      t.boolean :seen, null: false, default: false
      t.timestamps null: false
    end
  end
end
