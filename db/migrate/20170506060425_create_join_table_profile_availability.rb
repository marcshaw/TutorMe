class CreateJoinTableProfileAvailability < ActiveRecord::Migration[5.0]
  def change
    create_join_table :profiles, :availabilities do |t|
      # t.index [:profile_id, :availability_id]
      # t.index [:availability_id, :profile_id]
    end
  end
end
