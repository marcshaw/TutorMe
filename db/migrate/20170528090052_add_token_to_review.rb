class AddTokenToReview < ActiveRecord::Migration[5.0]
  def change
    add_column :reviews, :token, :text, null: false
    add_index :reviews, :token
  end
end
