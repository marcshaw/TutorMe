# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170903232644) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.integer  "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["profile_id"], name: "index_admins_on_profile_id", using: :btree
  end

  create_table "availabilities", force: :cascade do |t|
    t.string   "day"
    t.integer  "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["profile_id"], name: "index_availabilities_on_profile_id", using: :btree
  end

  create_table "availabilities_profiles", id: false, force: :cascade do |t|
    t.integer "profile_id",      null: false
    t.integer "availability_id", null: false
  end

  create_table "booking_messages", force: :cascade do |t|
    t.string   "message"
    t.integer  "from_id"
    t.integer  "booking_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["from_id"], name: "index_booking_messages_on_from_id", using: :btree
  end

  create_table "bookings", force: :cascade do |t|
    t.datetime "datetime"
    t.integer  "time_length"
    t.string   "contact_number"
    t.string   "subject"
    t.string   "course_code"
    t.integer  "price_cents",        default: 0,     null: false
    t.string   "price_currency",     default: "NZD", null: false
    t.string   "medium"
    t.string   "tutoring_year"
    t.string   "university"
    t.integer  "tutor_profile_id"
    t.integer  "student_profile_id"
    t.boolean  "student_confirmed",  default: false
    t.boolean  "tutor_confirmed",    default: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.boolean  "paid",               default: false, null: false
    t.boolean  "cancelled",          default: false, null: false
    t.boolean  "completed",          default: false, null: false
    t.boolean  "disputed",           default: false, null: false
    t.decimal  "tutor_rate",                         null: false
  end

  create_table "conversations", force: :cascade do |t|
    t.integer  "sender_id",      null: false
    t.integer  "recipient_id",   null: false
    t.datetime "latest_message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "course_codes", force: :cascade do |t|
    t.string  "course_code"
    t.integer "profile_id"
    t.index ["course_code"], name: "index_course_codes_on_course_code", using: :btree
    t.index ["profile_id"], name: "index_course_codes_on_profile_id", using: :btree
  end

  create_table "held_payments", force: :cascade do |t|
    t.integer "booking_id",                       null: false
    t.boolean "tutor_accepted",   default: false, null: false
    t.boolean "student_accepted", default: false, null: false
    t.index ["booking_id"], name: "index_held_payments_on_booking_id", using: :btree
  end

  create_table "media", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "media_profiles", id: false, force: :cascade do |t|
    t.integer "medium_id",  null: false
    t.integer "profile_id", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.text     "body"
    t.integer  "conversation_id"
    t.boolean  "read",            default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "profile_id"
    t.index ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
    t.index ["profile_id"], name: "index_messages_on_profile_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "profile_id",                           null: false
    t.integer  "notification_type",                    null: false
    t.integer  "notification_type_id",                 null: false
    t.boolean  "seen",                 default: false, null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["profile_id"], name: "index_notifications_on_profile_id", using: :btree
  end

  create_table "profiles", force: :cascade do |t|
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.text     "description"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean  "active_tutor",           default: false
    t.integer  "rate_cents",             default: 0,     null: false
    t.string   "rate_currency",          default: "NZD", null: false
    t.string   "first_name"
    t.integer  "university_id"
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.text     "phone_number"
    t.text     "token"
    t.index ["confirmation_token"], name: "index_profiles_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_profiles_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_profiles_on_reset_password_token", unique: true, using: :btree
  end

  create_table "profiles_tutoring_subjects", id: false, force: :cascade do |t|
    t.integer "profile_id",          null: false
    t.integer "tutoring_subject_id", null: false
  end

  create_table "profiles_tutoring_years", id: false, force: :cascade do |t|
    t.integer "tutoring_year_id", null: false
    t.integer "profile_id",       null: false
  end

  create_table "reviews", force: :cascade do |t|
    t.string   "body"
    t.integer  "rating"
    t.integer  "reviewer_id"
    t.integer  "profile_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.text     "token",                       null: false
    t.boolean  "reviewed",    default: false, null: false
    t.integer  "booking_id"
    t.index ["booking_id"], name: "index_reviews_on_booking_id", using: :btree
    t.index ["profile_id", "reviewer_id"], name: "index_reviews_on_profile_id_and_reviewer_id", unique: true, using: :btree
    t.index ["token"], name: "index_reviews_on_token", using: :btree
  end

  create_table "searches", force: :cascade do |t|
    t.integer  "min_rate_cents",    default: 0,     null: false
    t.string   "min_rate_currency", default: "NZD", null: false
    t.integer  "max_rate_cents",    default: 0,     null: false
    t.string   "max_rate_currency", default: "NZD", null: false
    t.string   "university"
    t.string   "tutoring_subject"
    t.string   "media"
    t.string   "course_code"
    t.string   "tutoring_year"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "tutoring_subjects", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "profile_id"
    t.index ["profile_id"], name: "index_tutoring_subjects_on_profile_id", using: :btree
  end

  create_table "tutoring_years", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "txns", force: :cascade do |t|
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "amount_cents",    default: 0,     null: false
    t.string   "amount_currency", default: "NZD", null: false
    t.integer  "txn_type",                        null: false
    t.decimal  "gst"
    t.integer  "profile_id",                      null: false
    t.integer  "booking_id"
    t.index ["booking_id"], name: "index_txns_on_booking_id", using: :btree
  end

  create_table "universities", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "reviews", "bookings"
end
