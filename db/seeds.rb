# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

TutoringSubject.create([{ name: 'Accounting' } ,{ name: 'Architecture' }, { name: 'Biology' },{ name: 'Biomedical Sciences' },{ name: 'Chemistry' },
                        { name: 'Computer Science' }, { name: 'Economics/Finance' }, { name: 'Engineering' },{ name: 'English' } ,
                        { name: 'French' }, { name: 'Geography' }, { name: 'Health' },{ name: 'Information Systems' },{ name: 'Law' },{ name: 'Management' },
                        { name: 'Mandarin' }, { name: 'Marketing' },{ name: 'Mathematics' },{ name: 'Physics' }, { name: 'Psychology' }, { name: 'Social Sciences' },{ name: 'Spanish' } ])

TutoringYear.create([{ name: 'First Year'}, { name: 'Second Year'}, { name: 'Third Year'} ,{ name: 'Fourth Year'}, { name: 'Other' }])

Availability.create([{ day: 'Mon' }, { day: 'Tue'}, { day: 'Wed'}, { day: 'Thur'}, { day: 'Fri'}, { day: 'Sat'}, { day: 'Sun'}])
Medium.create([{ name: 'Online' }, { name: 'In Person'}])

University.create([{name: 'Victoria University of Wellington'} ,{name: 'Auckland University'}, {name: 'Waikato University'},
                   {name: 'Massey University Wellington'},  {name: 'Massey University Palmerston North'},
                   {name: 'Otago University'}, {name: 'Canterbury University'},{name: 'Auckland University of Technology'}, { name: 'Other' }])

admin_email = ENV['ADMIN_EMAIL'] || 'marctest@hotmail.com'
admin_pw = ENV['ADMIN_PASSWORD'] || 'testpassword'

admin = Profile.create!(first_name: "Marc", email: admin_email, password: admin_pw, password_confirmation: admin_pw)
admin.skip_confirmation!
admin.save!

Admin.create!(profile: admin)

if Rails.env == 'development'
  def create_new_user_active_tutor(name)

    profile = Profile.new
    profile.email = name + '@envy17.com'
    profile.password = 'marcmarc'
    profile.password_confirmation = 'marcmarc'
    profile.skip_confirmation!
    profile.active_tutor=false
    profile.first_name = name
    profile.phone_number = '123456789'
    profile.university = University.find_by(name: "Victoria University of Wellington")
    profile.description = "Hi!

I'm Patrick, a Masters Maths Student at UoB. I have extensive experience in tutoring both GCSE and A Level Maths/Further Maths and also experience teaching university students up to third year at the university.

I dropped 1 mark over all of my Maths A-Level and I promise you that I can teach you the exact way to successfully sit the exams, focusing on just a few specific things.


I look forward to hearing from you!
    "
    profile.tutoring_years=TutoringYear.all
    profile.mediums=Medium.all
    profile.rate_cents = 2000

    profile.save!

    profile.tutoring_subjects << TutoringSubject.find_by(:name => "Mathematics")
    profile.tutoring_subjects << TutoringSubject.find_by(:name => "Physics")
    profile.availabilities << Availability.find_by(:day => "Mon")
    profile.availabilities << Availability.find_by(:day => "Tue")
    profile.course_codes.create!(course_code: "comp123")

    profile.update!(active_tutor: true)

    return profile

  end

  def create_booking_no_date(profile1, profile2)
    booking = Booking.new
    booking.student_confirmed = false
    booking.tutor_confirmed = false

    booking.cancelled = false
    booking.paid = false
    booking.student_profile = profile2
    booking.tutor_profile = profile1

    booking.contact_number = "027027027"
    booking.course_code = "Comp123"
    booking.medium = "Skype"
    booking.price_cents = 2000
    booking.subject = "Mathematics"
    booking.time_length = 60
    booking.university = "Victoria University of Wellington"
    booking.tutoring_year = "First"

    return booking
  end

  def create_past_booking(profile1, profile2)
    booking = create_booking_no_date(profile1,profile2)

    booking.datetime = Time.now - 1.year
    booking.tutor_rate = profile1.rate_cents

    booking.save(validation: false)
    booking

  end

  def create_future_booking(profile1, profile2)
    booking = create_booking_no_date(profile1,profile2)

    booking.datetime = Time.now + 1.year
    booking.tutor_rate = profile1.rate_cents

    booking.save!
    profile1.notifications.build(notification_type: Notification.notification_types[:booking], notification_type_id: booking.id, seen: false)
    booking
  end

  def create_new_user_inactive_tutor(name)

    profile = Profile.new
    profile.email = name + '@hotmail.com'
    profile.password = 'marcmarc'
    profile.password_confirmation = 'marcmarc'
    profile.skip_confirmation!
    profile.active_tutor=false
    profile.first_name = name
    profile.university = University.find_by(name: "Victoria University of Wellington")
    profile.save!

    return profile

  end


  def create_conversation_with_messages(main_profile, other_profile)

    conversation = Conversation.new()
    conversation.sender = other_profile
    conversation.recipient = main_profile
    conversation.latest_message = Time.now
    conversation.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Hello there, how are you"
    message1.profile = main_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Good tahnsk!"
    message1.profile = other_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Hello there, how are you"
    message1.profile = main_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Good tahnsk!"
    message1.profile = other_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Hello there, how are you"
    message1.profile = main_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Good tahnsk!"
    message1.profile = other_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Hello there, how are you"
    message1.profile = main_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Good tahnsk!"
    message1.profile = other_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Hello there, how are you"
    message1.profile = main_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Good tahnsk!"
    message1.profile = other_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Hello there, how are you"
    message1.profile = main_profile
    message1.save!

    message1 = Message.new()
    message1.conversation = conversation
    message1.body="Good tahnsk!"
    message1.profile = other_profile
    message1.save!

    main_profile.notifications.build(notification_type: Notification.notification_types[:message], notification_type_id: conversation.id, seen: false)
  end

  puts "======================SEEDING DATA FOR MANUAL TESTING===================="

  user2 = create_new_user_active_tutor("marc")

  user = create_new_user_active_tutor("jimmy")

  create_future_booking(user,user2)
  create_past_booking(user,user2)

  booking = create_future_booking(user2,user)
  create_past_booking(user2,user)

  @review = Review.new
  @review.body = "Bestest person I know... Top Block"
  @review.reviewer = user
  @review.rating = 4
  @review.token = SecureRandom.base58(24)
  @review.booking = booking
  @review.profile = user2
  @review.save!

  user2.reviews << @review
  user2.save!


  create_conversation_with_messages(user2,user)

  user = create_new_user_active_tutor("mike")

  booking = create_future_booking(user,user2)
  create_past_booking(user,user2)

  @review = Review.new
  @review.body = "Bestest person I know... Top Block"
  @review.reviewer = user2
  @review.rating = 4
  @review.token = SecureRandom.base58(24)
  @review.booking = booking
  @review.profile = user
  @review.save!

  user2.reviews << @review
  user2.save!

  booking = create_future_booking(user2,user)
  create_past_booking(user2,user)

  @review = Review.new
  @review.body = "Bestest person I know... Top Block"
  @review.reviewer = user
  @review.rating = 4
  @review.token = SecureRandom.base58(24)
  @review.booking = booking
  @review.profile = user2
  @review.save!

  user2.reviews << @review
  user2.save!

  user = create_new_user_active_tutor("haihai")
  create_conversation_with_messages(user2,user)

  user = create_new_user_inactive_tutor("haihahaui")
  create_conversation_with_messages(user2,user)

  user = create_new_user_inactive_tutor("johnzs")
  create_conversation_with_messages(user2,user)

  user = create_new_user_inactive_tutor("blue")
  create_conversation_with_messages(user2,user)

  user = create_new_user_inactive_tutor("green")
  create_conversation_with_messages(user2,user)
end
