class ConversationInformation

  attr_reader :times, :profiles

  def initialize
    @profiles = []
    @times = []
    yield(self) if block_given?
  end

  def add_time_and_profile(time, profile)
    times << time
    profiles << profile
  end
end

