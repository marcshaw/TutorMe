class CreateBookingMessage
  def initialize(booking, message_params, current_profile, conversation)
    @booking = booking
    @message_params = message_params
    @current_profile = current_profile
    @conversation = conversation
  end

  def call
    @message = @booking.booking_messages.new(@message_params)
    @message.booking = @booking
    @message.from = @current_profile
    other_profile = @conversation.other_profile(@current_profile)

    begin
      BookingMessage.transaction do
        @message.save!
        convo_message = @conversation.messages.new
        convo_message.body = @message_params[:message]
        convo_message.profile = @current_profile
        convo_message.save!
        @conversation.latest_message = convo_message.created_at;
        @conversation.save!
        other_profile.notifications.build(notification_type: Notification.notification_types[:message], notification_type_id: @conversation.id, seen: false)
        other_profile.save!
      end
    rescue ActiveRecord::RecordInvalid
    end

    @message.persisted?
  end
end

