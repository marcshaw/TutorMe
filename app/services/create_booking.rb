class CreateBooking
  def initialize(sanitized_booking_params, current_profile, tutor_profile)
    @booking_params = sanitized_booking_params
    @student_profile = current_profile
    @tutor_profile = tutor_profile
  end

  def call
    Booking.new(@booking_params) do |booking|
      booking.student_profile = @student_profile
      booking.tutor_profile = @tutor_profile
      booking.price_cents = @tutor_profile.rate_cents * ((@booking_params[:time_length].to_d) / 60)
      booking.tutor_rate = @tutor_profile.rate_cents
      if booking.save
        UserNotifier.new_tutor_booking(@tutor_profile, @student_profile).deliver_later
        UserNotifier.new_student_booking(@tutor_profile, @student_profile).deliver_later
        @tutor_profile.notifications.build(notification_type: Notification.notification_types[:booking], notification_type_id: booking.id, seen: false)
        @tutor_profile.save!
      end
    end
  end
end
