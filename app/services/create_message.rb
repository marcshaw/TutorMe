class CreateMessage

  attr_reader :error, :message

  def initialize(current_profile, other_profile, conversation_id, message_text)
    @current_profile = current_profile
    @other_profile = other_profile
    @conversation_id = conversation_id
    @message_text = message_text
  end

  def call
    set_conversation

    if(@conversation != nil)
      @message = @conversation.messages.new(body: @message_text)
      @message.profile = @current_profile

      if @message.save!
        @conversation.latest_message = @message.created_at;
        @conversation.save!
        @other_profile.notifications.build(notification_type: Notification.notification_types[:message], notification_type_id: @conversation.id, seen: false)
        @other_profile.save!
      else
        @error = "Message failed to save. Try to send it again, otherwise refresh the page and try again"
      end
    end
  end

  protected

  def set_conversation
    if(@other_profile == nil)
      @conversation = nil
      @error = "Could not find the other user. Refresh the page and try again"
      #When conversation id is not known and may not exist

    elsif(@conversation_id == '-1')
      @conversation = Conversation.between(@current_profile.id , @other_profile)[0]
      if(@conversation == nil)
        @conversation = CreateConversation.new(@current_profile, @other_profile).call
      end

    else
      if(@conversation == nil)
        @conversation = Conversation.find_by(id: @conversation_id)
      end

      if(@conversation == nil ||
         @conversation.sender != @current_profile && @conversation.recipient != @current_profile)

        @error = "Sending to a invalid conversation. Refresh the page and try again"
        @conversation = nil
      end
    end
  end
end

