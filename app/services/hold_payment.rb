class HoldPayment
  def initialize(booking)
    @booking = booking
  end

  def call
    if @booking.cancelled || @booking.completed || !@booking.confirmed || @booking.held_payment.present?
      Rails.logger.error "The booking can not be cancelled or completed and must be confirmed"
      raise InvalidParameters.new("The booking can not be cancelled or completed and must be confirmed")
    end

    if !@booking.persisted?
      Rails.logger.error "The booking needs to be persisted"
      raise InvalidParameters.new("Booking needs to be persisted")
    end

    HeldPayment.transaction do
      @booking.student_profile.txns.create!(txn_type: Txn.txn_types[:booking], amount_cents: -@booking.price_cents)
      @booking.create_held_payment!
    end
  end
end
