class UpdateBooking
  #TODO see createbooking for same questions about passing in booking_params
  def initialize(new_booking_params, booking)
    @new_booking_params = new_booking_params
    @booking = booking
  end

  def call
    @booking.tutor_confirmed = false
    @booking.student_confirmed = false
    if @booking.update(@new_booking_params)
      notifications
    end
  end

  private

  def notifications
    @booking.tutor_profile.notifications.build(notification_type: Notification.notification_types[:booking], notification_type_id: @booking.id, seen: false)
    @booking.student_profile.notifications.build(notification_type: Notification.notification_types[:booking], notification_type_id: @booking.id, seen: false)
    #TODO SURROND IN A BEGIN BLOCK
    @booking.tutor_profile.save!
    @booking.student_profile.save!
  end
end
