class BuildMessagePreviews
  def initialize(conversations)
    @conversations = conversations
  end

  def call
    previews = []
    #TODO Compress the query?
    @conversations.each do |conversation|
      messages = Message.where('conversation_id = ?' , conversation.id).order(created_at: 'desc').limit(1);

      if(!messages.empty?)
        previews.push(messages[0].body[0..8] + "...");
      end
    end
    previews
  end
end

