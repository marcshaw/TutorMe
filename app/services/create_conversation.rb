class CreateConversation
  def initialize(current_profile, other_profile)
    @current_profile = current_profile
    @other_profile = other_profile
  end

  def call
    Conversation.new do |conversation|
      conversation.recipient = @current_profile
      conversation.sender = @other_profile
      conversation.save!
    end
  end
end

