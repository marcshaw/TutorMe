class BuildConversationInformation
  def initialize(conversations, current_profile)
    @conversations = conversations
    @current_profile = current_profile
  end

  def call
    ConversationInformation.new do |info|
      #TODO Compress the query?
      @conversations.each do |conversation|
        profile_to_add =
          if conversation.sender_id == @current_profile.id
            Profile.find(conversation.recipient_id)
          else
            Profile.find(conversation.sender_id)
          end
        info.add_time_and_profile(conversation.latest_message.strftime("%H:%M %p"), profile_to_add)
      end
    end
  end
end

