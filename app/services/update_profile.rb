class UpdateProfile
  def initialize(profile, profile_params, university_name, course_codes, tutoring_subjects, rate_dollars)
    @profile = profile
    @profile_params = profile_params
    @university_name = university_name
    @course_codes = course_codes
    @tutoring_subjects = tutoring_subjects
    @rate_dollars = rate_dollars
  end

  def call
    set_university
    set_tutoring_subjects
    set_course_codes
    set_token if @profile.token.nil?
    @profile.rate_cents = @rate_dollars * 100
    if @profile.errors.count == 0
      @profile.update(@profile_params)
    end

    @profile
  end

  private

  def set_token
    @profile.token = SecureRandom.base58(24)
  end

  def set_tutoring_subjects
    add_tutoring_subjects
    remove_tutoring_subjects
  end

  def add_tutoring_subjects
    @tutoring_subjects.split(',').each do |subject|
      tutoring_subject = TutoringSubject.find_by(name: subject)
      if tutoring_subject.nil?
        @profile.errors.add(:tutoring_subjects, "not valid (#{subject}). Please recheck, and contact us if the error is still there.")
      else
        new_subject = @profile.tutoring_subjects.none? { |saved_subject| saved_subject.name == tutoring_subject.name }
        if new_subject
          @profile.tutoring_subjects << tutoring_subject
        end
      end
    end
  end

  def remove_tutoring_subjects
    current_subjects = @tutoring_subjects.split(',')
    to_remove = @profile.tutoring_subjects.reject { |subject| current_subjects.include?(subject.name) }
    to_remove.each { |subject| @profile.tutoring_subjects.delete(subject) }
  end

  def set_course_codes
    add_course_codes
    remove_course_codes
  end

  def add_course_codes
    @course_codes.split(',').each do |code|
      new_course = @profile.course_codes.none? { |saved_code| saved_code.course_code == code.downcase.strip }
      if new_course
        course_code = @profile.course_codes.build(course_code: code)
        unless course_code.valid?
          @profile.errors.add(:course_codes, "Course Code (#{code}) invalid - #{course_code.errors.full_messages.first}")
        end
      end
    end
  end

  def remove_course_codes
    current_courses = @course_codes.split(',').map { |code| code.downcase.strip }
    to_remove = @profile.course_codes.reject { |course| current_courses.include?(course.course_code) }
    to_remove.each { |course| course.delete }
  end

  def set_university
    if @university_name.present?
      university = University.find_by(name: @university_name)
      if university.nil?
        @profile.errors.add(:university, "not valid (#{@university_name}). Please recheck, and contact us if the error is still there.")
      else
        @profile.university = university
      end
    end
  end
end

