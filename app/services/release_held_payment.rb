class ReleaseHeldPayment
  def initialize(booking)
    @booking = booking
  end

  def call
    if @booking.cancelled || @booking.disputed || @booking.completed || @booking.paid
      Rails.logger.error "The booking can not be cancelled, disuputed or completed and must be confirmed"
      raise InvalidParameters.new("The booking can not be cancelled or completed and must be confirmed")
    end

    if !@booking.persisted?
      Rails.logger.error "The booking needs to be persisted"
      raise InvalidParameters.new("Booking needs to be persisted")
    end

    held_payment = HeldPayment.find_by!(booking: @booking)

    HeldPayment.transaction do
      tutor_payment = @booking.price_cents * 0.925
      admin_payment = @booking.price_cents - tutor_payment
      create_admin_payment(admin_payment)
      create_tutor_payment(tutor_payment)
    end
  end

  private

  def create_tutor_payment(tutor_payment)
    @booking.tutor_profile.txns.create!(txn_type: Txn.txn_types[:booking], amount_cents: tutor_payment, booking: @booking)
    @booking.update!(paid: true)
  end

  def create_admin_payment(admin_payment)
    Admin.first.profile.txns.create!(txn_type: Txn.txn_types[:fee], amount_cents: admin_payment)
  end
end
