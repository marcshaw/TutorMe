class ConfirmBooking
  def initialize(booking, profile)
    @booking = booking
    @profile = profile
  end

  def call
    if @profile.id == @booking.student_profile.id
      @booking.student_confirmed = true
    else
      @booking.tutor_confirmed = true
    end

    begin
      Booking.transaction do
        saved = @booking.save!
        if @booking.tutor_confirmed == true &&
          @booking.student_confirmed == true
          UserNotifier.confirmed_tutor_booking(@booking).deliver_later
          UserNotifier.confirmed_student_booking(@booking).deliver_later
        end
      end
    rescue ActiveRecord::RecordInvalid => e
      Rails.logger.error "A booking failed to be confirmed"
      Rails.logger.error @booking.to_json
      raise e
    end
  end
end
