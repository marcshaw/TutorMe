class NewReview
  def initialize(tutor, reviewer, booking)
    @tutor = tutor
    @reviewer = reviewer
    @booking = booking
  end

  def call
    if @booking.nil? || @booking.not_completed?
      Rails.logger.error "Failed to create a new review -- the student and tutor didnt have a booking"
      Rails.logger.error "Student: #{@reviewer.id}"
      Rails.logger.error "Reviewer: #{@tutor.id}"
      raise InvalidParameters.new("Failed to create a new review -- the student and tutor didnt have a booking")
    end

    Review.transaction do
      Review.new do |review|
        review.reviewer = @reviewer
        review.booking = @booking
        @tutor.reviews << review
        review.generate_token
        review.save!
        @tutor.save!
      end
    end
  end
end
