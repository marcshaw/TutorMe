class CompleteBooking
  def initialize(booking)
    @booking = booking
  end

  def call
    if @booking.cancelled || @booking.completed || @booking.disputed
      Rails.logger.error "The booking can not be cancelled, disputed or completed"
      raise InvalidParameters.new("The booking can not be cancelled, disputed or completed")
    end

    if !@booking.persisted?
      Rails.logger.error "The booking needs to be persisted"
      raise InvalidParameters.new("Booking needs to be persisted")
    end

    Booking.transaction do
      @booking.update!(completed: true)

      unless review_exists?
        review = NewReview.new(@booking.tutor_profile, @booking.student_profile, @booking).call
        UserNotifier.review_tutor(review).deliver_now
      end
    end
  end

  private

  def review_exists?
    Review.where("profile_id = ? AND reviewer_id = ?", @booking.tutor_profile.id, @booking.student_profile.id).any?
  end
end
