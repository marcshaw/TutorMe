class CreateReview
  def initialize(review_params, review)
    @review_params = review_params
    @review = review
  end

  def call
    @review.reviewed = true
    @review.update(@review_params)
    @review
  end
end
