class BillCustomer
  def initialize(txn, token, payment_method: Stripe::Charge)
    @txn = txn
    @token = token
    @payment_method = payment_method
  end

  def call
    #TODO make sure the booking is of type `booking`
    if @txn.persisted?
      Rails.logger.error "Something tried to send a saved transaction to bill customer service"
      raise InvalidParameters.new("Bill Customer was called with a saved txn")
    end

    begin
      Stripe.api_key = ENV['PRIVATE_STRIPE_KEY']

      Txn.transaction do
        @txn.save!

        @charge = @payment_method.create(
          :amount => @txn.amount_cents,
          :currency => "nzd",
          :description => "Tutrme",
          :source => @token,
        )
      end
    rescue Stripe::CardError => e
      # Since it's a decline, Stripe::CardError will be caught
      body = e.json_body
      err  = body[:error]

      Rails.logger.error "Status is: #{e.http_status}"
      Rails.logger.error "Type is: #{err[:type]}"
      Rails.logger.error "Code is: #{err[:code]}"
      # param is '' in this case
      Rails.logger.error "Param is: #{err[:param]}"
      Rails.logger.error "Message is: #{err[:message]}"

      raise Stripe::CreditCardDeclined.new(err[:message])
    rescue Stripe::InvalidRequestError => e
      Rails.logger.error "Create subscription failed due to Stripe::InvalidRequestError : #{e.message}"

      raise Stripe::CreditCardException.new(e.message)
    rescue Stripe::AuthenticationError => e
      Rails.logger.error "Authentication with Stripe's API failed"
      Rails.logger.error "(maybe you changed API keys recently)"

      raise Stripe::CreditCardException.new(e.message)
    rescue Stripe::APIConnectionError => e
      Rails.logger.error "Network communication with Stripe failed"

      raise Stripe::CreditCardException.new(e.message)
    rescue Stripe::StripeError => e
      Rails.logger.error "Display a very generic error to the user, and maybe send yourself an email"

      raise Stripe::CreditCardException.new(e.message)
    rescue ActiveRecord::RecordInvalid => e
      Rails.logger.error "A transaction failed to save"
      Rails.logger.error @txn.to_json

      raise e
    end
  end
end
