class CancelBooking
  def initialize(booking, from_job: false)
    @booking = booking
    @from_job = from_job
  end

  def call
    if @booking.cancelled || @booking.completed || @booking.paid?
      Rails.logger.error "The booking can not be cancelled, paid, or completed"
      raise InvalidParameters.new("The booking can not be cancelled, paid or completed")
    end

    if !@booking.persisted?
      Rails.logger.error "The booking needs to be persisted"
      raise InvalidParameters.new("Booking needs to be persisted")
    end

    Booking.transaction do
      @booking.update!(cancelled: true)
      deliver_mail
    end
  end

  private

  def create_mail
    [UserNotifier.cancelled_tutor_booking(@booking), UserNotifier.cancelled_student_booking(@booking)]
  end

  def deliver_mail
    if @from_job
      create_mail.each { |mail| mail.deliver_now }
    else
      create_mail.each { |mail| mail.deliver_later }
    end
  end
end
