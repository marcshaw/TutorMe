class CancelBookings
  attr_reader :service, :query_method

  def initialize(query_method: BookingsToCancel.new, service: CancelBooking)
    @query_method = query_method
    @service = service
  end

  def call
    Rails.logger.info "Running Cancel bookings Job"
    puts "Running Cancel bookings Job"
    @query_method.call.each do |booking|
      begin
        Rails.logger.info "Cancelling booking for id: #{booking.id}"
        puts "Cancelling booking for id: #{booking.id}"
        @service.new(booking, from_job: true).call
      rescue Exception => e
        Rails.logger.error "Failed to cancel booking for #{booking}"
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.inspect
      end
    end
  end
end
