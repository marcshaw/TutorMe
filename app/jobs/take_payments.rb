class TakePayments
  attr_reader :service, :query_method

  def initialize(query_method: BookingsToTakePayment.new, service: HoldPayment)
    @query_method = query_method
    @service = service
  end

  def call
    raise "Take Payments job should not be run"
    @query_method.call.each do |booking|
      begin
        @service.new(booking).call
      rescue Exception => e
        Rails.logger.error "Failed to take funds from #{booking.student_profile}"
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.inspect
      end
    end
  end

end
