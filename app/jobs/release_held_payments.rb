class ReleaseHeldPayments
  def initialize(query_method: PaymentsToRelease.new, release_payments: ReleaseHeldPayment, complete_bookings: CompleteBooking)
    @query_method = query_method
    @release_payments = release_payments
    @complete_bookings = complete_bookings
  end

  def call
    log_start
    @query_method.call.each do |booking|
      begin
        log_booking(booking)
        Booking.transaction do
          @release_payments.new(booking).call
          @complete_bookings.new(booking).call
        end
      rescue StandardError => e
        log_error(e, booking)
      end
    end
  end

  private

  def log_start
    Rails.logger.info "Running Release Held Payments Job"
    puts "Running Release Held Payments Job"
  end

  def log_booking(booking)
    Rails.logger.info "Release held payment for booking_id: #{booking.id}"
    puts "Release held payment for booking_id: #{booking.id}"
  end

  def log_error(error, booking)
    Rails.logger.error "Failed to release funds for #{booking.tutor_profile}"
    Rails.logger.error error.message
    Rails.logger.error error.backtrace.inspect
  end
end
