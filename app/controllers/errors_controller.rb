class ErrorsController < ApplicationController
  # GET /
  def index
    render template: 'static/error'
  end
end
