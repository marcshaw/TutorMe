class ProfilesController < ApplicationController
  before_action :authenticate_profile!
  before_action :set_profile, only: [:show]
  before_action :set_university, only: [:show]

  autocomplete :tutoring_subject, :name
  autocomplete :university, :name

  def show
    @reviews = @profile.reviews.paginate(:page => params[:page]).per_page(6)
  end

  def update
    @profile = UpdateProfile.new(current_profile,
                                 profile_params,
                                 params[:profile][:university],
                                 params[:profile][:course_codes],
                                 params[:profile][:tutoring_subjects],
                                 params[:profile][:rate_cents].to_i).call

    if @profile.errors.present?
      set_university
      render :edit
    else
      redirect_to profile_path, notice: 'User was successfully updated.'
    end
  end

  def edit
    @profile = current_profile
    set_university
  end

  private

  def profile_params
    params.require(:profile).permit(:description, :avatar, :active_tutor, :phone_number,
                                    :tutoring_year_ids => [], :medium_ids => [], :availability_ids => [])
  end

  def set_profile
    profile_selected = Profile.find_by(id: params[:id])

    @profile =
      if profile_selected == nil
        current_profile
      else
        if profile_selected.active_tutor == false
          current_profile
        else
          profile_selected
        end
      end
  end

  def set_university
    #If no university is associated, just show empty string
    @university_value =
      if @profile.university == nil
        ""
      else
        @profile.university.name
      end
  end
end

