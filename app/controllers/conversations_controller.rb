class ConversationsController < ApplicationController
  before_action :authenticate_profile!
  before_action :set_current_profile, only: [:index, :show]

  def index
    find_conversations_information
    set_message_previews
  end

  def show
    @conversation = Conversation.between(current_profile.id, params[:id]).first

    if @conversation == nil ||
        (@conversation.sender_id != current_profile.id && @conversation.recipient_id != current_profile.id)

      find_conversations_information
      @current_profile.errors.add(:id, "Invalid Conversation Selected")
      render :index

    else
      @other_profile = @conversation.sender_id == current_profile.id ? Profile.find_by(id: @conversation.recipient_id) : Profile.find_by(id: @conversation.sender_id)

      params[:limit] = params[:limit] == nil ? 15 : params[:limit]

      @messages = @conversation.messages.order('created_at desc').limit(params[:limit])

      #Is used to tell the user they can load more messages or not
      @loadedAllMessages = @messages.length == @conversation.messages.count ? true : false

      respond_to do |format|
        format.js
      end
    end
  end

  private

  def conversation_params
    params.permit(:sender_id, :recipient_id)
  end

  def set_current_profile
    @current_profile = current_profile
  end

  def set_message_previews
    @previews = BuildMessagePreviews.new(@conversations).call
  end

  def find_conversations_information
    @conversations = Conversation.where("recipient_id = ? or sender_id = ?", current_profile.id, current_profile.id).order(latest_message: 'desc')
    @conversation_info = BuildConversationInformation.new(@conversations, current_profile).call
  end
end
