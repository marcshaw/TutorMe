class HomeController < ApplicationController
  # GET /
  def index
    if profile_signed_in?
      render 'dashboard/index'
    else
      render 'static/index'
    end

  end
end
