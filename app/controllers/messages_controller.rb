class MessagesController < ApplicationController
  before_action :authenticate_profile!

  def create
    other_profile = Profile.find_by!(id: params[:message]['profile_id'])
    message = CreateMessage.new(current_profile,
                                other_profile,
                                params[:conversation_id],
                                params[:message][:body])

    message.call

    if message.error.present?
      head status: 500, json: { errors: message.error }
    else
      redirect_to conversation_path(id: params[:message]['profile_id'])
    end
  end
end

