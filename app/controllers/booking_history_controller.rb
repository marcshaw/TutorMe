class BookingHistoryController < ApplicationController
  before_action :authenticate_profile!

  def index
    tutoring_bookings = Booking.where('tutor_profile_id = ?' , current_profile.id)
    student_bookings = Booking.where('student_profile_id = ?' , current_profile.id)

    if(tutoring_bookings != nil && tutoring_bookings.size > 1 )
      past = PastBookings.new(tutoring_bookings).call
      @past_tutoring_bookings = past.paginate(:page => params[:tutor_past_page], :per_page => 6)
    end

    if(student_bookings != nil && student_bookings.size > 1)
      past = PastBookings.new(student_bookings).call
      @past_student_bookings = past.paginate(:page => params[:student_past_page], :per_page => 6)
    end
  end
end
