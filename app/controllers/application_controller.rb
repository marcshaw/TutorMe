class ApplicationController < ActionController::Base
  include HeaderHelper
  helper_method :current_user

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout :determine_layout

  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)
    sign_in_url = new_profile_session_url
    if request.referer == sign_in_url
      super
    else
      stored_location_for(resource) || request.referer || root_path
    end
  end

  def after_sign_out_path_for(resource)
    root_path
  end

  def current_user
    current_profile
  end

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name])
  end

  def determine_layout
    if profile_signed_in?
      "base_private"
    elsif devise_controller?
      "base_devise"
    else
      "base_public"
    end
  end

end
