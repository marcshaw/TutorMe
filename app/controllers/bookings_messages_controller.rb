class BookingsMessagesController < ApplicationController
  before_action :authenticate_profile!
  before_action :set_booking
  before_action :set_other_profile
  before_action :set_conversation, only: [:create]

  def index
    @messages = @booking.booking_messages.order(created_at: 'desc')
  end

  def create
    if(@booking.tutor_profile != current_profile && @booking.student_profile != current_profile)
      redirect_to error_path, :flash => { :error => "You tried to send a message on a booking that isn't yours" }
    end

    if CreateBookingMessage.new(@booking, booking_message_params, current_profile, @conversation).call
      redirect_to profile_booking_bookings_messages_path(@booking)
    else
      render status: 500, json: { errors: "Conversation failed to save" }
    end
  end

  private

  def booking_message_params
    params.require(:booking_message).permit(:message)
  end

  def set_booking
    @booking = Booking.find_by(id: params[:booking_id])
  end

  def set_other_profile
    @other_profile =
      if @booking.tutor_profile == current_profile
        @booking.student_profile
      else
        @booking.tutor_profile
      end
  end

  def set_conversation
    @conversation = Conversation.between(current_profile.id, @other_profile)[0]
    if (@conversation == nil)
      @conversation = CreateConversation.new(current_profile, @other_profile).call
    end
  end
end

