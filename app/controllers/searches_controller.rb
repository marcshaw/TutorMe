class SearchesController < ApplicationController
  before_action :authenticate_profile!

  def new
    @search = Search.new
  end

  def show
    @search = Search.create(search_params)
    @search.course_code = search_params[:tutoring_subject] if search_params[:tutoring_subject]
    @tutors = SearchProfiles.new(current_profile.id, @search).call.paginate(:page => params[:page], :per_page => 9)

    respond_to do |format|
      format.js
    end
  end

  private

  def search_params
    params.require(:search).permit(:university, :min_rate_cents, :max_rate_cents, :course_code, :tutoring_subject,
                                   :tutoring_year => [], :media => [])
  end
end
