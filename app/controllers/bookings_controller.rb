class BookingsController < ApplicationController
  before_action :authenticate_profile!
  before_action :set_booking, only: [:show, :edit, :update, :destroy, :confirm, :cancel, :pay]
  before_action :set_profile, only: [:new, :create, :show, :edit, :update, :confirm, :index, :cancel, :pay]
  before_action :set_other_profile, only: [:new, :create, :show, :edit, :update, :cancel, :pay]
  before_action :action_accessible_by_user, except: [:index, :new, :create]
  before_action :editable_booking, only: [:update, :edit]

  def index
    #TODO Always show the current_profile? or throw error page if trying to access someone elses

    #TODO Order by date and time
    tutoring_bookings = Booking.where('tutor_profile_id = ?' , current_profile.id)
    student_bookings = Booking.where('student_profile_id = ?' , current_profile.id)

    #TODO Make it only change out the current/past that it needs to for pagination of index

    if(tutoring_bookings != nil && tutoring_bookings.size > 0)
      confirmed = ConfirmedBookings.new(bookings: tutoring_bookings, tutor: true).call
      confirmed = confirmed.uncancelled
      @confirmed_tutoring_bookings = confirmed.paginate(page: params[:tutor_confirmed_page], per_page: 6)

      unconfirmed = ConfirmedBookings.new(bookings: tutoring_bookings, tutor: false).call
      unconfirmed = unconfirmed.uncancelled
      @unconfirmed_tutoring_bookings = unconfirmed.paginate(page: params[:tutor_unconfirmed_page], per_page: 6)
    end

    if(student_bookings != nil && student_bookings.size > 0)
      confirmed = ConfirmedBookings.new(bookings: student_bookings, student: true).call
      confirmed = confirmed.uncancelled
      @confirmed_student_bookings = confirmed.paginate(page: params[:student_confirmed_page], per_page: 6)

      unconfirmed = ConfirmedBookings.new(bookings: student_bookings, student: false).call
      unconfirmed = unconfirmed.uncancelled
      @unconfirmed_student_bookings = unconfirmed.paginate(page: params[:student_unconfirmed_page], per_page: 6)
    end
  end

  def show
    @messages = @booking.booking_messages.order(created_at: 'desc')

    if @booking.datetime.past?
      @afterDate = true;
    else
      @afterDate = false;
    end

    if params[:seen]
      noti = current_profile.booking_notification(@booking.id)
      noti.update(seen: true) if noti
    end
  end

  def new
    if(@other_profile.id == current_profile.id)
      redirect_to error_path, :flash => { :error => "You can't make a booking with yourself :)" }
    end
    @booking = Booking.new
    @availability = @other_profile.availabilities.map(&:day)
  end

  def edit
    @availability = @other_profile.availabilities.map(&:day)
  end

  def create
    @booking = CreateBooking.new(booking_params, current_profile, @other_profile).call
    if @booking.persisted?
      redirect_to profile_booking_path(@profile, @booking), notice: 'Booking was successfully created.'
    else
      @availability = @other_profile.availabilities.map(&:day)
      render :new
    end
  end

  def update
    respond_to do |format|
      if UpdateBooking.new(booking_params, @booking).call
        format.html { redirect_to profile_booking_path(@profile, @booking), notice: 'Booking was successfully updated.' }
      else
        @availability = @other_profile.availabilities.map(&:day)
        format.html { render :edit }
      end
    end
  end

  def confirm
    if ConfirmBooking.new(@booking, @profile).call
      render :partial => 'confirmation'
    else
      render :partial => 'confirmation', notice: 'Failed to confirm'
    end
  end

  def cancel
    CancelBooking.new(@booking).call
    redirect_to action: "show", id: params[:booking_id], profile_id: params[:profile_id]
  end

  def pay
    if current_profile == @booking.student_profile
      if @profile.balance_cents > @booking.price_cents
        HoldPayment.new(@booking).call
      else
        flash[:error] = "Please add money to your account"
      end
    end

    redirect_to action: "show", id: params[:booking_id], profile_id: params[:profile_id]
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_booking
    id = params[:id] == nil ? params[:booking_id] : params[:id]
    @booking = Booking.find(id)
  end

  def editable_booking
    if @booking.completed? || @booking.cancelled?
      redirect_to error_path, :flash => { :error => "You can't edit this booking" }
    end
  end

  def set_profile
    @profile = current_profile
  end

  def set_other_profile
    @other_profile =
      (@booking && (@booking.tutor_profile == current_profile ? @booking.student_profile : @booking.tutor_profile)) ||
      @other_profile = Profile.find(params[:profile_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def booking_params
    params.require(:booking).permit(:subject, :datetime, :course_code, :price, :medium, :tutoring_year, :university, :time_length, :contact_number, :message)
  end

  def action_accessible_by_user
    if(current_profile.id != @booking.tutor_profile.id &&
        current_profile.id != @booking.student_profile.id)
      redirect_to error_path, :flash => { :error => "You tried to access a booking that isn't yours" }
    end
  end
end
