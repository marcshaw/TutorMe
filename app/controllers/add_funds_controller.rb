class AddFundsController < ApplicationController
  before_action :authenticate_profile!
  before_action :set_profile, only: [:new, :create, :show, :edit, :update, :confirm, :index]

  def index
    @txns = @profile.txns.paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @txn = Txn.new
  end

  def create
    @txn = @profile.txns.build(amount: params[:txn][:amount_cents])
    @txn.gst = @txn.amount * 0.15
    @txn.txn_type = Txn.txn_types[:deposit]

    begin
      @charge = BillCustomer.new(@txn, params[:token]).call
      redirect_to profile_add_funds_path(@profile)
    rescue Stripe::CreditCardDeclined => e
      handle_exception(e.message)
    rescue Stripe::CreditCardException
      handle_exception("Something went wrong with the payment. Please try again in 5 minutes. Contact us if this continues to happen")
    rescue ActiveRecord::RecordInvalid => e
      handle_exception("Please make sure amount is between $20 - $50. If issues continue to occur, please contact us.")
    end
  end

  private

  def handle_exception(message)
    @error = message
    @txn = Txn.new
    render 'new'
  end

  def set_profile
    @profile = current_profile
  end
end
