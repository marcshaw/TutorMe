class ReviewsController < ApplicationController
  before_action :authenticate_profile!
  before_action :set_review, only: [:edit, :update]
  before_action :check_permission, only: [:edit, :update]

  def index
    @reviews = @profile.reviews.paginate(:page => params[:page]).per_page(6)
    respond_to do |format|
      format.js
    end
  end

  def edit
    @profile = @review.profile
    render :edit
  end

  def update
    @review = CreateReview.new(review_params, @review).call
    if @review.valid?
      redirect_to @review.profile
    else
      @profile = @review.profile
      render :edit
    end
  end

  private

  def set_review
    @review = Review.find_by(token: params[:id])
  end

  def check_permission
    if @review.reviewer != current_profile
      redirect_to error_path, :flash => { :error => "There seems to be a mixup with reviews. Please contact us" }
    end

    if Review.between(@review.profile, @review.reviewer).count > 1
      redirect_to error_path, :flash => { :error => "There seems to be a mixup with reviews. Please contact us" }
    end
  end

  def review_params
    params.require(:review).permit(:body, :rating)
  end
end
