class RegistrationsController < Devise::RegistrationsController

  def show
  end

  protected

  def after_inactive_sign_up_path_for(resource)
    signed_up_path()
  end
end
