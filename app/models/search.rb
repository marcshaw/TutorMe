class Search < ActiveRecord::Base
  validates :course_code, format: {with: /\A((([a-zA-Z]{1,4}[\d]{3}))|)\z/,
                                   message: "Please use up to 4 letters, followed by 3 numbers. Ie, Comp102 "}

  validate :validate_university

  def empty?
    empty_rate? && empty_media? &&
      empty_university? && empty_course_code? &&
      empty_tutoring_years? && empty_tutoring_subject?
  end

  protected

  def empty_rate?
    min_rate_cents == 0 && max_rate_cents == 100
  end

  def empty_media?
    media.delete("^0-9,").split(',').empty?
  end

  def empty_university?
    university.blank?
  end

  def empty_course_code?
    course_code.blank?
  end

  def empty_tutoring_years?
    tutoring_year.delete("^0-9,").split(',').empty?
  end

  def empty_tutoring_subject?
    tutoring_subject.blank?
  end

  def validate_university
    if university.present?
      university = University.find_by(name: self.university)
      if university == nil
        self.errors.add(:university, " contains a non-valid university. Please contact us to add yours.")
      end
    end
  end
end
