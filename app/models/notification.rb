class Notification < ActiveRecord::Base
  belongs_to :profile

  enum notification_type: [ :booking, :message ]

  scope :unseen, -> { where(seen: false) }
end
