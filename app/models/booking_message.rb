class BookingMessage < ActiveRecord::Base
  belongs_to :from, :class_name => "Profile"
  belongs_to :booking
end
