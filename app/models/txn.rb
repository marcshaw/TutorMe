class Txn < ActiveRecord::Base
  belongs_to :profile
  belongs_to :booking, optional: true

  monetize :amount_cents

  enum txn_type: [ :withdrawl, :deposit, :booking, :fee ]

  validates_presence_of :amount_cents, :gst, :txn_type, :profile
  validates :amount_cents, numericality: { greater_than_or_equal_to: 2000, less_than_or_equal_to: 5000 }, if: 'self.txn_type == "deposit"'
  validates :amount_cents, numericality: { greater_than: 0 }, if: 'self.txn_type == "withdrawl"'
  validate :balance_not_in_negatives, if: 'self.txn_type == "withdrawl" || self.txn_type == "booking"'

  before_validation :set_gst

  def balance_not_in_negatives

    if(amount_cents && profile)
      amount = (txn_type == "withdrawl" ? amount_cents : -amount_cents)
      self.errors.add(:amount_cents, "The user should have a balance that is a positive number") unless (profile.balance_cents - amount >= 0)
    end
  end

  def set_gst
    self.gst ||= self.amount_cents * 0.15
  end
end
