class Review < ActiveRecord::Base
  belongs_to :reviewer, :class_name => 'Profile'
  belongs_to :profile
  belongs_to :booking

  validates_presence_of :token, :booking_id, :reviewer_id, :profile_id

  validates :rating, format: {with: /\A[1-5]{1}\z/,
                                   message: "Please choose an appropriate rating"},
                                   if: :reviewed
  validates :body, length: { minimum: 5,
                             maximum: 250,
                             wrong_length: "The review body should be between 5 - 250 characters"},
                             if: :reviewed

  validates_uniqueness_of :profile_id, scope: :reviewer_id

  scope :between, -> (profile_id, reviewer_id) do
    where("profile_id = ? AND reviewer_id = ?", profile_id, reviewer_id)
  end

  def generate_token
    self.token = SecureRandom.uuid
  end
end
