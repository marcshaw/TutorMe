class HeldPayment < ActiveRecord::Base
  validates_presence_of :booking_id
  validates :tutor_accepted, :student_accepted, inclusion: [true, false]

  belongs_to :booking

  def funds_released?
    self.booking.paid?
  end
end
