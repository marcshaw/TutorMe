class CourseCode < ActiveRecord::Base
  belongs_to :profile

  before_validation :downcase_course_code
  before_validation :remove_white_space

  validate :matches_regex

  def matches_regex
    regex = Regexp.new('\A((([a-zA-Z]{1,4}[\d]{3})[,]?)|){1}\z').freeze
    valid = regex.match(self.course_code)
    self.errors.add(:course_code, "should be up to 4 letters, followed by 3 numbers (eg Comp102)") unless valid
  end

  def remove_white_space
    self.course_code.strip!
  end

  def downcase_course_code
    self.course_code.downcase!
  end
end
