class Profile < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_and_belongs_to_many :tutoring_years
  has_and_belongs_to_many :mediums
  has_and_belongs_to_many :tutoring_subjects
  has_and_belongs_to_many :availabilities
  belongs_to :university
  has_many :reviews
  has_many :txns
  has_many :notifications
  has_many :course_codes

  attr_readonly :first_name

  monetize :rate_cents

  validates_associated :university
  validates_associated :mediums
  validates_associated :tutoring_years
  validates_associated :course_codes

  validates :description, length: {
                            maximum: 500,
                            too_long: "%{count} characters is the maximum allowed for the description"
                          },
                          if: 'active_tutor == true'

  validates :first_name , presence: true, allow_blank: false
  validates :mediums , presence: true, allow_blank: false, if: 'active_tutor == true'
  validates :phone_number , presence: true, allow_blank: false, if: 'active_tutor == true'
  validates :university , presence: true, allow_blank: false, if: 'active_tutor == true'
  validates :rate_cents , presence: true, allow_blank: false, if: 'active_tutor == true'
  validates :tutoring_years , presence: true, allow_blank: false, if: 'active_tutor == true'
  validates :description , presence: true, allow_blank: false, if: 'active_tutor == true'
  validates :availabilities , presence: true, allow_blank: false, if: 'active_tutor == true'

  validate :tutoring_subjects_exist, if: 'active_tutor == true'
  validate :validate_availability, if: 'active_tutor == true'

  validates :phone_number, presence: true,
            numericality: true,
            length: { minimum: 8, maximum: 15 },
            if: 'active_tutor == true'

  has_attached_file :avatar,
    styles: { medium: ["300x300>", :jpg], thumb: ["120x120", :jpg], original: ["100%", :jpg] },
    default_url: "missing.png",
    s3_protocol: "https",
    path: lambda { |attachment| ":class/:attachment/:id/#{attachment.instance.token}/:style.:extension" }

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates_attachment_size :avatar, less_than: 1.megabytes
  validates_attachment_file_name :avatar, matches: [/png\Z/, /jpe?g\Z/]

  DAYS = ['Mon','Tue','Wed','Thur','Fri','Sat','Sun']

  def balance_cents
    (txns.deposit.sum('amount_cents') - txns.withdrawl.sum('amount_cents')) + txns.booking.sum('amount_cents') + txns.fee.sum('amount_cents')
  end

  def balance_dollars
    ((txns.deposit.sum('amount_cents') - txns.withdrawl.sum('amount_cents')) + txns.booking.sum('amount_cents')) / 100
  end

  def booking_notification(id)
    notifications.unseen.booking.find_by(notification_type_id: id)
  end

  def after_confirmation
    UserNotifier.welcome_email(self).deliver_later
  end

  #========== Validations ==========

  def tutoring_subjects_exist
    return false if !tutoring_subjects || tutoring_subjects.empty?

    valid = self.tutoring_subjects.all? do |subject|
      if TutoringSubject.find_by(name: subject.name).nil?
        self.errors.add(:tutoring_subjects, " contains a non-valid subject")
      end
    end
  end

  def validate_availability
    valid = self.availabilities.all? do |availability|
      DAYS.include?(availability.day)
    end

    self.errors.add(:availabilities, "should be between Monday - Sunday") unless valid
  end
end
