class Message < ActiveRecord::Base

  belongs_to :conversation
  belongs_to :profile

  validates_presence_of :body, :conversation_id, :profile_id
  validates :body, length: {maximum: 1000,
                                   too_long: "%{count} characters is the maximum allowed for the description"}

  def message_time
    self.created_at.strftime("%m/%d/%y at %l:%M %p")
  end
end
