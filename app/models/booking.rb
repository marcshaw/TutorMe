class Booking < ActiveRecord::Base

  monetize :price_cents

  belongs_to :tutor_profile, :class_name => "Profile"
  belongs_to :student_profile, :class_name => "Profile"

  has_one :held_payment
  has_one :review

  has_many :booking_messages, dependent: :destroy

  validates :cancelled, :completed, :paid, :disputed, :student_confirmed, :tutor_confirmed, inclusion: [true, false]

  validates_presence_of :time_length,:contact_number,:subject,:course_code,:price_cents,
                        :medium,:tutoring_year,:university,:datetime,:tutor_profile,:student_profile,:tutor_rate

  validates :contact_number, :presence => {:message => 'Invalid Phone Number'},
            :numericality => true,
            :length => { :minimum => 8, :maximum => 15 }

  validates :course_code, format: {with: /\A((([a-zA-Z]{1,4}[\d]{3})[,]?)|)+\z/,
                                   message: "Please use up to 4 letters, followed by 3 numbers. Ie, Comp102 "}

  validate :total_price
  validate :not_complete_and_cancelled
  validate :not_paid_when_disputed
  validate :datetime_cannot_be_in_the_past

  scope :not_disputed, -> { where(disputed: false) }
  scope :disputed, -> { where(disputed: true) }
  scope :payment_not_complete, -> { where(paid: false) }
  scope :payment_held, -> { joins("LEFT OUTER JOIN held_payments ON held_payments.booking_id = bookings.id").where("held_payments.id IS NOT null") }
  scope :payment_not_held, -> { joins("LEFT OUTER JOIN held_payments ON held_payments.booking_id = bookings.id").where("held_payments.id IS null") }
  scope :uncancelled, -> { where(cancelled: false) }
  scope :completed, -> { where(completed: true) }
  scope :uncompleted, -> { where(completed: false) }
  scope :confirmed, -> { where(student_confirmed: true, tutor_confirmed: true) }


  def confirmed
    student_confirmed && tutor_confirmed
  end

  def unpaid?
    held_payment.nil?
  end

  def paid?
    held_payment.present?
  end

  def other_profile(profile)
    profile == tutor_profile ? student_profile : tutor_profile
  end

  def date
    datetime.to_date
  end

  def time
    datetime.strftime("%I:%M%p")
  end

  def datetime_formatted
    datetime.strftime("%v %r")
  end

  def not_completed?
    !completed
  end

  def not_cancelled?
    !cancelled
  end

  def not_paid?
    !paid
  end
  #===============Validations============

  def not_paid_when_disputed
    if(!paid_was && paid && disputed_was)
      self.errors.add(:status_conflict, "- The booking can not be paid when it is currently disputed")
    end
  end

  def not_complete_and_cancelled
    if(completed && cancelled)
      self.errors.add(:status_conflict, " - The booking can not be completed and cancelled at the same time")
    end
  end

  def datetime_cannot_be_in_the_past
    if (new_record? || not_cancelled? && not_completed? && not_paid?) && datetime.present? && datetime < Date.today
      errors.add(:datetime, "can't be in the past")
    end
  end

  def total_price
    return false unless (tutor_profile && time_length && price_cents )

    costPerHour = tutor_rate

    if((costPerHour * ( time_length / 60.0 )) != price_cents)
      self.errors.add(:price_cents,"The total price does not match the tutors per hour and time length selected");
    end
  end
end
