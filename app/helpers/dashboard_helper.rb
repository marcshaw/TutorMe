module DashboardHelper
  include ActionView::Helpers::UrlHelper

  def display_notifications
    raw(
      if current_profile.notifications.unseen.empty?
        content_tag :span, "No new notifications"
      else
        current_profile.notifications.unseen.map do |notification|
          display_notification(notification)
        end.join
      end
    )
  end

  private

  def display_notification(notification)
    if notification.booking?
      display_booking(Booking.find(notification.notification_type_id), notification)
    else
      display_message(Conversation.find(notification.notification_type_id), notification)
    end
  end

  def display_booking(booking, notification)
    content_tag :div, "" do |div|
      content_tag(:i, "", class: "fa fa-lg fa-users") +
        content_tag(:span, "Booking with #{other_users_name_booking(booking)} updated") +
        link_to('View Booking', profile_booking_path(profile_id: current_profile, id: notification.notification_type_id, seen: true), class: 'btn-mod btn-small btn-w font-alt', style: 'margin-left: 10px; padding: 0px 15px')
    end
  end

  def display_message(convo, notification)
    notification.update(seen: true)
    content_tag :div, "" do |div|
      content_tag(:i, "", class: "fa fa-lg fa-comments-o") +
        content_tag(:span, "New messages from #{other_users_name_conversation(convo)}")
    end
  end

  def other_users_name_booking(booking)
    booking.other_profile(current_profile).first_name
  end

  def other_users_name_conversation(convo)
    convo.other_profile(current_profile).first_name
  end
end
