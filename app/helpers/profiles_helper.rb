module ProfilesHelper

  def rate_to_dollars
    @profile.rate.dollars
  end

  #TODO Rename and DRY these methods
  def display_course_codes_show
    @profile.course_codes.map { |course| course.course_code }.try(:to_sentence, last_word_connector: ", ", two_words_connector: ", ")
  end

  def display_course_codes_edit
    @profile.course_codes.map { |course| course.course_code }.try(:to_sentence, last_word_connector: ",", two_words_connector: ",", words_connector: ",")
  end

  def display_tutoring_subjects_show
    @profile.tutoring_subjects.map { |subject| subject.name }.try(:to_sentence, last_word_connector: ", ", two_words_connector: ", ")
  end

  def display_tutoring_subjects_edit
    @profile.tutoring_subjects.map { |subject| subject.name }.try(:to_sentence, last_word_connector: ",", two_words_connector: ",", words_connector: ",")
  end
end
