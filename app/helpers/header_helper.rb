module HeaderHelper
  def navigation_classes
    if controller_name.eql?('home')
      'main-nav dark transparent stick-fixed'
    else
      'main-nav light stick-fixed'
    end
  end
end
