module BookingsHelper
  def options_for_time_length
    [
        ['30 Minutes', '30'],
        ['45 Minutes', '45'],
        ['60 Minutes', '60'],
        ['75 Minutes', '75']
    ]
  end

  def options_for_subject
    subjects = @other_profile.tutoring_subjects.map(&:name)
    dropDownSubjects = [[]]

    subjects.each_with_index { |subject, i|
      dropDownSubjects << [subject,subject]
    }
    return dropDownSubjects
  end

  def options_for_year
    years = @other_profile.tutoring_years
    dropDownYears = [[]]

    years.each_with_index { |year, i|
      dropDownYears[0].push(year.name,year.name);
    }
    return dropDownYears
  end

  def options_for_medium
    medias = @other_profile.mediums
    dropDownMedias = [[]]

    medias.each_with_index { |media, i|
      dropDownMedias[0].push(media.name,media.name);
    }
    return dropDownMedias
  end

  def format_date(booking)
    booking.datetime.strftime("%B #{ActiveSupport::Inflector.ordinalize(booking.datetime.day)}, %Y")
  end

  def set_time(booking)
    booking.datetime.strftime("%I:%M%p") if booking.datetime
  end

  def set_date(booking)
    booking.datetime.to_date if booking.datetime
  end
end
