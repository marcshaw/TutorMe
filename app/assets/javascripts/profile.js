/**
 * Created by marc on 04-Apr-16.
 */

$(function () {
    $('#profile_avatar').on('change', function (event) {
        var files = event.target.files;
        if (files.length == 0) {
            $('#target').html("");
            return;
        }
        var image = files[0]
        var reader = new FileReader();
        reader.onload = function (file) {
            var img = new Image();
            img.src = file.target.result;
            img.style = "width: 250px; height: 250px;";
            $('#target').html(img);
        }
        reader.readAsDataURL(image);

    });
});

$(function () {
    var txt = $('.description'),
        hiddenDiv = $(document.createElement('div')),
        content = null;

    if (txt.length != 0) {
        txt.addClass('txtstuff');
        hiddenDiv.addClass('hiddendiv common');

        $('.profile_form').append(hiddenDiv);

        content = $(txt).val();

        content = content.replace(/\n/g, '<br>');
        hiddenDiv.html(content + '<br class="lbr">');

        $(txt).css('height', hiddenDiv.height());



        txt.on('keyup', function (e) {

            content = $(this).val();

            splitContent = content.split('\n');
            length = splitContent.length;

            newValue = "";

            if(length > 16){
                for(i = 0 ; i < 16 ; i ++ ){
                    newValue += splitContent[i] + "\n";
                }
                content = newValue;
                $(this).val(newValue);
            }



            content = content.replace(/\n/g, '<br>');
            hiddenDiv.html(content + '<br class="lbr">');

            $(this).css('height', hiddenDiv.height());

        });
    }
});