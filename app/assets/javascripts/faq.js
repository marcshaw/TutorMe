jQuery(document).ready(function($){
  if($(".cd-faqs").length){
    faqTrigger = $('.cd-faq-trigger'),

    //show faq content clicking on faqTrigger
    faqTrigger.on('click', function(event){
      event.preventDefault();
      $(this).next('.cd-faq-content').slideToggle(200).end().parent('li').toggleClass('content-visible');
    });

    if($(".cd-faq-categories").length){
      //update these values if you change these breakpoints in the style.css file (or _layout.scss if you use SASS)
      var MqM= 768,
      MqL = 1024;

      var faqsSections = $('.cd-faq-group'),
      faqsContainer = $('.cd-faq-items'),
      faqsBase = $('.cd-faq-base'),
      faqsCategoriesContainer = $('.cd-faq-categories'),
      faqsCategories = faqsCategoriesContainer.find('a'),
      closeFaqsContainer = $('.cd-close-panel');

      //select a faq section
      faqsCategories.on('click', function(event){
        event.preventDefault();
        var selectedHref = $(this).attr('href'),
        target= $(selectedHref);
        if( $(window).width() < MqM) {
          faqsContainer.scrollTop(0).addClass('slide-in').children('ul').removeClass('selected').end().children(selectedHref).addClass('selected');
          closeFaqsContainer.addClass('move-left');
          $('body').addClass('cd-overlay');
          bind_click();
        } else {
          $('body,html').animate({ 'scrollTop': target.offset().top - 80}, 200);
        }
      });

      //close faq lateral panel - mobile only
      function bind_click(){
        $('body').bind('click touchstart', function(event){
          var container = $(".cd-faq-items");
          var navContainer = $(".full-wrapper");
          var categoriesContainer = $(".cd-faq-categories");
          if (!navContainer.is(event.target)&&
              !container.is(event.target) &&
              categoriesContainer.has(event.target).length === 0 &&
              container.has(event.target).length === 0 &&
              navContainer.has(event.target).length === 0){
            closePanel(event);
            container.unbind( 'click', document );
          }
        });
      }
      faqsBase.on("swiperight", function() {
        closePanel(event);
      });

      //update category sidebar while scrolling
      $(window).on('scroll', function(){
        if ( $(window).width() > MqL ) {
          (!window.requestAnimationFrame) ? updateCategory() : window.requestAnimationFrame(updateCategory);
        }
      });

      $(window).on('resize', function(){
        if($(window).width() <= MqL) {
          faqsCategoriesContainer.removeClass('is-fixed').css({
            '-moz-transform': 'translateY(0)',
            '-webkit-transform': 'translateY(0)',
            '-ms-transform': 'translateY(0)',
            '-o-transform': 'translateY(0)',
            'transform': 'translateY(0)',
          });
        }
        if( faqsCategoriesContainer.hasClass('is-fixed') ) {
          faqsCategoriesContainer.css({
            'left': faqsContainer.offset().left,
          });
        }
      });

      function closePanel(e) {
        e.preventDefault();
        faqsContainer.removeClass('slide-in').find('li').show();
        closeFaqsContainer.removeClass('move-left');
        $('body').removeClass('cd-overlay');
      }

      function updateCategory(){
        updateCategoryPosition();
        updateSelectedCategory();
      }

      function updateCategoryPosition() {
        var top = $('.cd-faq').offset().top,
        height = jQuery('.cd-faq').height() - jQuery('.cd-faq-categories').height(),
        margin = 80;
        if( top - margin <= $(window).scrollTop() && top - margin + height > $(window).scrollTop() ) {
          var leftValue = faqsCategoriesContainer.offset().left,
            widthValue = faqsCategoriesContainer.width();
          faqsCategoriesContainer.addClass('is-fixed').css({
            'left': leftValue,
            'top': margin,
            '-moz-transform': 'translateZ(0)',
            '-webkit-transform': 'translateZ(0)',
            '-ms-transform': 'translateZ(0)',
            '-o-transform': 'translateZ(0)',
            'transform': 'translateZ(0)',
          });
        } else if( top - margin + height <= $(window).scrollTop()) {
          var delta = top - margin + height - $(window).scrollTop();
          faqsCategoriesContainer.css({
            '-moz-transform': 'translateZ(0) translateY('+delta+'px)',
            '-webkit-transform': 'translateZ(0) translateY('+delta+'px)',
            '-ms-transform': 'translateZ(0) translateY('+delta+'px)',
            '-o-transform': 'translateZ(0) translateY('+delta+'px)',
            'transform': 'translateZ(0) translateY('+delta+'px)',
          });
        } else {
          faqsCategoriesContainer.removeClass('is-fixed').css({
            'left': 0,
            'top': 0,
          });
        }
      }

      function updateSelectedCategory() {
        faqsSections.each(function(){
          var actual = $(this),
          margin = parseInt($('.cd-faq-title').eq(1).css('marginTop').replace('px', '')),
          activeCategory = $('.cd-faq-categories a[href="#'+actual.attr('id')+'"]'),
          topSection = (activeCategory.parent('li').is(':first-child')) ? 0 : Math.round(actual.offset().top);

          if ( ( topSection - 81 <= $(window).scrollTop() ) && ( Math.round(actual.offset().top) + actual.height() + margin - 81 > $(window).scrollTop() ) ) {
            activeCategory.addClass('selected');
          }else {
            activeCategory.removeClass('selected');
          }
        });
      }
    }
  }
});
