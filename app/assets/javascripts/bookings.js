
function updateTime(){
    var selected = $("#booking_time_length :selected").val();
    $('#time_requested').html(selected + " Minutes");

    var total = ( selected / 60 ) * $("#hidden_rate").html();
    $('#total_cost').html("$" + total );

    $("#booking_price").val(total);

}

$(function(){
    updateTime();
});


$(function(){
    $(".booking_convo").mCustomScrollbar({
        theme: "minimal-dark",
        axis:"y",
        alwaysShowScrollbar: 1,
        autoHideScrollbar: false
    });

    $(".booking_convo").mCustomScrollbar("scrollTo","last");

    $("#time_select").timepicker({
        'disableTextInput': true,
        'step': 15
    });

    //We are enabling any disabled inputs so that then they will be submitted with the form instead of being blank
    $('form').bind('submit', function () {
        $(this).find(':input').prop('disabled', false);
    });
});

function sendBookingMessage(profile_id, booking_id){

    var message = $(".message_input").val();

    if(message.length == 0){
        return
    }

    $.ajax({
        type: "POST",
        url: "/profiles/" + profile_id + "/bookings/" + booking_id + "/bookings_messages",
        async: true,
        format: "js",
        /*        dataType: "HTML",*/
        data: {"booking_message[message]": message},
        success: function(data, status, xhr) {
            $(".message_input").val("");
            createBookingMessageScrollBar();
        },
        error: function(data, status, xhr){

            response = JSON.parse(data.responseText);

            //TODO Format correctly
            $("#notice").html("Error sending message: " + response.errors)

        }
    })
}

function createBookingMessageScrollBar() {
    $(".booking_convo").mCustomScrollbar({
        theme: "minimal-dark",
        axis:"y",
        alwaysShowScrollbar: 1,
        autoHideScrollbar: false
    });

    $(".booking_convo").mCustomScrollbar("scrollTo","last");

}

function confirmBooking(profile_id, booking_id){

    $.ajax({
        type: "GET",
        url: "/profiles/" + profile_id + "/bookings/" + booking_id + "/confirm",
        async: true,
        success: function(data) {
            $('#confirmation_button').html(data);
        }
    });


}

var activeSelected = "tutor";

function changeBookings(bookingType){

    if(bookingType != activeSelected){

        var student_selector = $('#student_selector');
        var tutor_selector = $('#tutor_selector');

        var tutor_bookings = $('#tutor_bookings');
        var student_bookings = $('#student_bookings');

        if(bookingType == "tutor"){
            student_selector.addClass("unselected_bookings")
            tutor_selector.removeClass("unselected_bookings")

            tutor_bookings.removeClass("hidden_bookings")
            student_bookings.addClass("hidden_bookings")

            activeSelected = "tutor"
        }
        else{
            student_selector.removeClass("unselected_bookings")
            tutor_selector.addClass("unselected_bookings")

            tutor_bookings.addClass("hidden_bookings")
            student_bookings.removeClass("hidden_bookings")

            activeSelected = "student"
        }
    }
}

//Function To Display Popup
function popUp(type, message, error){

  $("#title").html(message);
  $("#popUpError").html(error);
  $("#popUpMsg").show();

  if(type == "pay") {
    $("#payment_button").show();
  } else {
    $("#cancel_button").show();
  }

  $(document).mouseup(function(e)
      {
        var container = $("#popupContact");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
          $("#popUpMsg").hide();
          $("#popUpDone").hide();
          $("#popupContact").unbind( 'click', document );
        }
      });

}

//Function to Hide Popup
function div_hide() {
  $("#popUpMsg").hide();
  $("#popUpDone").hide();
  $("#payment_button").hide();
  $("#cancel_button").hide();
}
