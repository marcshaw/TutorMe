/**
 * Created by marc on 10-Apr-16.
 */


$('.left .person').mousedown(function () {
    if ($(this).hasClass('.active')) {
        return false;
    } else {
        $('.chat').removeClass('active-chat');
        $('.left .person').removeClass('active');
        $(this).addClass('active');
        $('.chat').addClass('active-chat');
    }
});

//Holding the other users ID of the conversation we have selected
currentConvoProfile = -1;

//Holding the conversation id
convo_id = -1;

//Initial number of messages loaded
convoLimit = 15;

function loadConvo(profileId,convoId) {

  if (profileId != currentConvoProfile) {

    //New Conversation
    currentConvoProfile = profileId;
    convo_id = convoId;
    convoLimit = 15;

    $.ajax({
      type: "GET",
      url: "/conversations/" + currentConvoProfile,
      async: true,
      data: {"limit": convoLimit},
      success: function() {
        createScrollBar();
      }
        })
    }

}

function sendMessage(){

  if($(".message_input").val() != ""){
    $.ajax({
        type: "POST",
        url: "/conversations/" + convo_id + "/messages",
        async: true,
        data: {"message[body]": $(".message_input").val(), "message[profile_id]" : currentConvoProfile},
        success: function() {
            $(".message_input").val("");
            createScrollBar();
            //TODO Remove error message
        },
        error: function(){
            //TODO Show error message
        }
    })
  }
}

function loadMoreConvo(){

    convoLimit += 15;

    $.ajax({
        type: "GET",
        url: "/conversations/" + currentConvoProfile,
        data: {"limit": convoLimit},
        async: true,
        success: function() {
            createScrollBar();
        }
    })

}

function createScrollBar() {
    $(".chat").mCustomScrollbar({
        theme: "minimal-dark",
        axis:"y",
        alwaysShowScrollbar: 1,
        autoHideScrollbar: false,
        mouseWheel:{ preventDefault: true }
    });

    $(".chat").mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});

    $(".people").mCustomScrollbar({
        theme: "minimal-dark",
        axis:"y",
        alwaysShowScrollbar: 1,
        autoHideScrollbar: false,
        mouseWheel:{ preventDefault: true }
    });
}

$(function(){
    $(".people").mCustomScrollbar({
        theme: "minimal-dark",
        axis:"y",
        alwaysShowScrollbar: 1,
        autoHideScrollbar: false
    });
});
