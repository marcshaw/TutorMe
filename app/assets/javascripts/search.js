//Holds the id of the other user. Used when we are sending messages from the search screen.
otherProfile = -1;

function send() {
    $.ajax({
        type: "POST",
        url: "/conversations/-1/messages",
        async: true,
        data: {"message[body]": $(".message_input").val(), "message[profile_id]" : otherProfile},
        success: function() {
            $(".message_input").val("");
            $("#popUpMsg").hide();
            $("#popUpDone").show();
        },
        error: function(data){
            $("#message_error").html("Error sending message. Please try again")
        }
    })
}



//Function To Display Popup
function div_show(name,otherProfileID) {
    otherProfile = otherProfileID;
    $("#title").text("Message " + name);
    $("#popUpMsg").show();

    $(document).mouseup(function(e)
        {
          var container = $("#popupContact");

          // if the target of the click isn't the container nor a descendant of the container
          if (!container.is(e.target) && container.has(e.target).length === 0)
          {
            $("#popUpMsg").hide();
            $("#popUpDone").hide();
            $("#popupContact").unbind( 'click', document );
          }
        });

}
//Function to Hide Popup
function div_hide() {
  $("#popUpMsg").hide();
  $("#popUpDone").hide();
}
