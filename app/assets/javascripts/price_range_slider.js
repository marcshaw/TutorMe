/**
 * Created by michaelwinton on 5/04/16.
 */

$(function () {

    var priceRangeSlider = document.getElementById('price-range-slider');

    if (typeof(priceRangeSlider) != 'undefined' && priceRangeSlider != null) {
        noUiSlider.create(priceRangeSlider, {
            start: [0, 100],
            step: 1,
            range: {
                'min': [0],
                'max': [100]
            },
            margin: 10,
            connect: true,
            format: {
                to: function (value) {
                    return value;
                },
                from: function (value) {
                    return value;
                }
            }
        });

        //the actual inputs used in the form
        var inputRangeValues = [
            document.getElementById('search_min_rate_cents'),
            document.getElementById('search_max_rate_cents')
        ];

        //the values rendered on screen
        var priceRangeValues = [
            document.getElementById('slider-value-lower'),
            document.getElementById('slider-value-upper')
        ];

        priceRangeSlider.noUiSlider.on('update', function (values, handle) {
            priceRangeValues[handle].innerHTML = values[handle];
            inputRangeValues[handle].value = values[handle];
        });
    }

});