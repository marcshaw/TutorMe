var newCol = false;


//TODO - Buy a copy http://codecanyon.net/item/booking-calendar-pro-jquery-plugin/full_screen_preview/244645

(function ($) {
    'use strict';

    $.fn.DOPFrontendBookingCalendarPRO = function (options) {
        /*
         * Private variables.
         */
        var getAva = $('#availability').text();

        var days = parseAvailability(getAva);
        var previousClicked;

        var Data = {
                "reinitialize": false,
                "calendar": {
                    "data": {
                        "bookingStop": 0,
                        "dateType": 1,
                        "language": "en",
                        "languages": [],
                        "view": false
                    },
                    "text": {
                        "available": "Available",
                        "nextMonth": "Next month",
                        "previousMonth": "Previous month",
                        "removeMonth": "Remove month view",
                        "unavailable": "Unavailable"
                    }
                },
                "days": {
                    "data": {
                        "available": [true, true, true, true, true, true, true],
                        "first": 1,
                        "morningCheckOut": false,
                        "multipleSelect": true
                    },
                    "text": {
                        "names": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                        "shortNames": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]
                    }
                },
                "ID": 0,
                "months": {
                    "data": {"no": 1},
                    "text": {
                        "names": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                        "shortNames": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
                    }
                }
            },
            Container = this,
            ID = 0,

// ***************************************************************************** Main methods.

// 1. Main methods.

            methods = {
                init: function () {
                    /*
                     * Initialize jQuery plugin.
                     */
                    return this.each(function () {
                        if (options) {
                            $.extend(Data, options);
                        }

                        if (!$(Container).hasClass('dopbcp-initialized')
                            || Data['reinitialize']) {
                            $(Container).addClass('dopbcp-initialized');
                            methods.parse();
                        }
                    });
                },
                parse: function () {
                    /*
                     * Parse calendar options.
                     */
                    methods_calendar.data = Data['calendar']['data'];
                    methods_calendar.text = Data['calendar']['text'];

                    methods_days.data = Data['days']['data'];
                    methods_days.text = Data['days']['text'];

                    methods_months.data = Data['months']["data"];
                    methods_months.text = Data['months']["text"];

                    Container.html('<div class="dopbcp-loader"></div>');

                    /*
                     * Init months data.
                     */
                    methods_months.init();

                    /*
                     * Load schedule.
                     */
                    methods_schedule.parse(new Date().getFullYear());
                }
            },

// 2. Schedule

            methods_schedule = {
                vars: {schedule: {}},

                parse: function () {

                    methods_calendar.vars.display = false;
                    methods_calendar.display();
                    methods_components.init();

                    methods_calendar.vars.firstYearLoaded = true;

                }

            },

// 3. Components

            methods_components = {
                init: function () {
                    /*
                     * Initialize calendar components.
                     */
                    /*
                     * Initialize today date.
                     */
                    methods_calendar.vars.todayDate = new Date();
                    methods_calendar.vars.todayDay = methods_calendar.vars.todayDate.getDate();
                    methods_calendar.vars.todayMonth = methods_calendar.vars.todayDate.getMonth() + 1;
                    methods_calendar.vars.todayYear = methods_calendar.vars.todayDate.getFullYear();

                    /*
                     * Initialize start date.
                     */
                    methods_calendar.vars.startDate = new Date(new Date().getTime() + methods_calendar.data['bookingStop'] * 60 * 1000);
                    methods_calendar.vars.currMonth = methods_calendar.vars.startDate.getMonth() + 1;
                    methods_calendar.vars.currYear = methods_calendar.vars.startDate.getFullYear();
                    methods_calendar.vars.startDay = methods_calendar.vars.startDate.getDate();
                    methods_calendar.vars.startMonth = methods_calendar.vars.startDate.getMonth() + 1;
                    methods_calendar.vars.startYear = methods_calendar.vars.startDate.getFullYear();

                    /*
                     * Tooltip
                     */
                    methods_tooltip.display();

                    /*
                     * Calendar
                     */
                    methods_calendar.navigation.init();
                    methods_calendar.init(methods_calendar.vars.startYear,
                        methods_calendar.vars.startMonth);



                }
            },

// 6. Tooltip

            methods_tooltip = {
                display: function () {

                    methods_tooltip.init();
                },
                init: function () {
                    /*
                     * Initialize information tooltip.
                     */
                    var $tooltip = $('#DOPBCPCalendar-tooltip' + ID),
                        h,
                        xPos = 0,
                        yPos = 0;

                    if (!prototypes.isTouchDevice()) {
                        /*
                         * Position the tooltip depending on mouse position.
                         */
                        $(document).mousemove(function (e) {
                            xPos = e.pageX + 15;
                            yPos = e.pageY - 10;

                            /*
                             * Tooltip height.
                             */
                            h = $tooltip.height()
                                + parseFloat($tooltip.css('padding-top'))
                                + parseFloat($tooltip.css('padding-bottom'))
                                + parseFloat($tooltip.css('border-top-width'))
                                + parseFloat($tooltip.css('border-bottom-width'));

                            if ($(document).scrollTop() + $(window).height() < yPos + h + 10) {
                                yPos = $(document).scrollTop() + $(window).height() - h - 10;
                            }

                            $tooltip.css({
                                'left': xPos,
                                'top': yPos
                            });
                        });
                    }
                    else {
                        /*
                         * Hide tooltip when you touch it.
                         */
                        $tooltip.unbind('touchstart');
                        $tooltip.bind('touchstart', function (e) {
                            e.preventDefault();
                            methods_tooltip.clear();
                        });
                    }
                },
                clear: function () {
                    /*
                     * Clear information display.
                     */
                    $('#DOPBCPCalendar-tooltip' + ID).css('display', 'none');
                }
            },

// ***************************************************************************** Calendar

// 8. Calendar

            methods_calendar = {
                data: {},
                text: {},
                vars: {
                    currMonth: new Date(),
                    currYear: new Date(),
                    display: true,
                    firstYearLoaded: false,
                    startDate: new Date(),
                    startDay: new Date(),
                    startMonth: new Date(),
                    startYear: new Date(),
                    todayDate: new Date(),
                    todayDay: new Date(),
                    todayMonth: new Date(),
                    todayYear: new Date()
                },

                display: function () {
                    /*
                     * Display calendar.
                     */
                    var HTML = new Array();

                    /*
                     * Message HTML
                     */
                    HTML.push('<div class="DOPBCPCalendar-info-message" id="DOPBCPCalendar-info-message' + ID + '">');
                    HTML.push('    <div class="dopbcp-icon"></div>');
                    HTML.push('    <div class="dopbcp-text"></div>');
                    HTML.push('    <div class="dopbcp-timer"></div>');
                    HTML.push('    <a href="javascript:void(0)" onclick="jQuery(\'#DOPBCPCalendar-info-message' + ID + '\').stop(true, true).fadeOut(300)" class="dopbcp-close"></a>');
                    HTML.push('</div>');

                    /*
                     *  Calendar HTML.
                     */
                    HTML.push('<div class="DOPBCPCalendar-wrapper notranslate dopbcp-initialized" id="DOPBCPCalendar' + ID + '">');
                    HTML.push(' <div class="DOPBCPCalendar-container">');
                    HTML.push('    <div class="DOPBCPCalendar-navigation">');
                    HTML.push('        <div class="dopbcp-month-year"></div>');
                    HTML.push('        <a href="javascript:void(0)" class="dopbcp-next-btn"><span class="dopbcp-info">' + methods_calendar.text['nextMonth'] + '</span></a>');
                    HTML.push('        <a href="javascript:void(0)" class="dopbcp-previous-btn"><span class="dopbcp-info">' + methods_calendar.text['previousMonth'] + '</span></a>');
                    HTML.push('        <div class="dopbcp-week">');
                    HTML.push('            <div class="dopbcp-day split-names">Mon</div>');
                    HTML.push('            <div class="dopbcp-day split-names">Tue</div>');
                    HTML.push('            <div class="dopbcp-day split-names">Wed</div>');
                    HTML.push('            <div class="dopbcp-day split-names">Thur</div>');
                    HTML.push('            <div class="dopbcp-day split-names">Fri</div>');
                    HTML.push('            <div class="dopbcp-day split-names">Sat</div>');
                    HTML.push('            <div class="dopbcp-day split-names">Sun</div>');
                    HTML.push('        </div>');
                    HTML.push('    </div>');
                    HTML.push('    <div class="DOPBCPCalendar-calendar"></div>');
                    HTML.push(' </div>');
                    HTML.push('</div>');


                    Container.html(HTML.join(''));
                },
                init: function (year,
                                month) {
                    /*
                     * Initialize calendar.
                     *
                     * @param year (Number): year to be displayed
                     * @param month (Number): month to be displayed
                     */
                    var i;

                    methods_calendar.vars.currYear = new Date(year, month, 0).getFullYear();
                    methods_calendar.vars.currMonth = month;

                    /*
                     * Initialize add/remove buttons.
                     */
                    if (methods_months.vars.no > 1) {
                        $('.DOPBCPCalendar-navigation .dopbcp-remove-btn', Container).css('display', 'block');
                    }
                    else {
                        $('.DOPBCPCalendar-navigation .dopbcp-remove-btn', Container).css('display', 'none');
                    }


                    /*
                     * Initialize previous button.
                     */
                    if (year !== methods_calendar.vars.startYear
                        || month !== methods_calendar.vars.startMonth) {
                        $('.DOPBCPCalendar-navigation .dopbcp-previous-btn', Container).css('display', 'block');
                    }

                    if (year === methods_calendar.vars.startYear
                        && month === methods_calendar.vars.startMonth) {
                        $('.DOPBCPCalendar-navigation .dopbcp-previous-btn', Container).css('display', 'none');
                    }
                    methods_day.vars.previousStatus = '';
                    methods_day.vars.previousBind = 0;

                    if (Container.width() <= 400) {
                        $('.DOPBCPCalendar-navigation .dopbcp-month-year', Container).html(methods_months.text['names'][(methods_calendar.vars.currMonth % 12 !== 0 ? methods_calendar.vars.currMonth % 12 : 12) - 1] + ' ' + methods_calendar.vars.currYear);
                    }
                    else {
                        $('.DOPBCPCalendar-navigation .dopbcp-month-year', Container).html(methods_months.text['names'][(methods_calendar.vars.currMonth % 12 !== 0 ? methods_calendar.vars.currMonth % 12 : 12) - 1] + ' ' + methods_calendar.vars.currYear);
                    }
                    $('.DOPBCPCalendar-calendar', Container).html('');

                    for (i = 1; i <= methods_months.vars.no; i++) {
                        methods_month.display(methods_calendar.vars.currYear,
                            month = month % 12 !== 0 ? month % 12 : 12,
                            i);
                        month++;

                        if (month % 12 === 1) {
                            methods_calendar.vars.currYear++;
                            month = 1;
                        }
                    }

                    methods_days.displaySelection();
                    methods_day.events.init();


                },

                navigation: {
                    init: function () {
                        /*
                         * Initialize calendar navigation.
                         */
                        methods_calendar.navigation.events();
                    },

                    events: function () {
                        /*
                         * Initialize calendar navigation events.
                         */
                        /*
                         * Previous button event.
                         */
                        $('.DOPBCPCalendar-navigation .dopbcp-previous-btn', Container).unbind('click touchstart');
                        $('.DOPBCPCalendar-navigation .dopbcp-previous-btn', Container).bind('click touchstart', function () {
                            methods_calendar.init(methods_calendar.vars.startYear,
                                methods_calendar.vars.currMonth - 1);

                            if (methods_calendar.vars.currMonth === methods_calendar.vars.startMonth) {
                                $('.DOPBCPCalendar-navigation .dopbcp-previous-btn', Container).css('display', 'none');
                            }
                        });

                        /*
                         * Next button event.
                         */
                        $('.DOPBCPCalendar-navigation .dopbcp-next-btn', Container).unbind('click touchstart');
                        $('.DOPBCPCalendar-navigation .dopbcp-next-btn', Container).bind('click touchstart', function () {
                            methods_calendar.init(methods_calendar.vars.startYear,
                                methods_calendar.vars.currMonth + 1);
                            $('.DOPBCPCalendar-navigation .dopbcp-previous-btn', Container).css('display', 'block');
                        });

                        /*
                         * Add button event.
                         */
                    }
                }
            },

// 9. Months

            methods_months = {
                data: {},
                text: {},
                vars: {
                    maxAllowed: 6,
                    no: 1
                },

                init: function () {
                    /*
                     * Initialize months data.
                     */
                    methods_months.vars.no = methods_months.data['no'];
                }
            },

            methods_month = {
                display: function (year,
                                   month,
                                   position) {
                    /*
                     * Display month.
                     *
                     * @param year (Number): the year that has the month to be initialized
                     * @param month (Number): month to be initialized
                     * @param position (Number): month's position in display order
                     *
                     * @return months HTML
                     */
                    var cmonth,
                        cday,
                        cyear,
                        d,
                        day = methods_day.default(),
                        HTML = new Array(),
                        i,
                        prevDay,
                        noDays = new Date(year, month, 0).getDate(),
                        noDaysPrevMonth = new Date(year, month - 1, 0).getDate(),
                        firstDay = new Date(year, month - 1, 2 - methods_days.data['first']).getDay(),
                        lastDay = new Date(year, month - 1, noDays - methods_days.data['first'] + 1).getDay(),
                        schedule = methods_schedule.vars.schedule,
                        totalDays = 0;

                    if (position > 1) {
                        HTML.push('<div class="DOPBCPCalendar-month-year">' + methods_months.text['names'][(month % 12 !== 0 ? month % 12 : 12) - 1] + ' ' + year + '</div>');
                    }
                    HTML.push('<div class="DOPBCPCalendar-month" id="DOPBCPCalendar-month-' + ID + '-' + position + '">');


                    HTML.push('<div class = "col-md-12 col-sm-12">');
                    /*
                     * Display previous month days.
                     */
                    for (i = (firstDay === 0 ? 7 : firstDay) - 1; i >= 1; i--) {
                        totalDays++;

                        d = new Date(year, month - 2, noDaysPrevMonth - i + 1);
                        cyear = d.getFullYear();
                        cmonth = prototypes.getLeadingZero(d.getMonth() + 1);
                        cday = prototypes.getLeadingZero(d.getDate());
                        day = schedule[cyear + '-' + cmonth + '-' + cday] !== undefined ? schedule[cyear + '-' + cmonth + '-' + cday] : methods_day.default(prototypes.getWeekDay(cyear + '-' + cmonth + '-' + cday));

                        prevDay = prototypes.getPrevDay(cyear + '-' + cmonth + '-' + cday);
                        methods_day.vars.previousStatus = methods_day.vars.previousStatus === '' ? (schedule[prevDay] !== undefined ? schedule[prevDay]['status'] : 'none') : methods_day.vars.previousStatus;
                        methods_day.vars.previousBind = methods_day.vars.previousBind === 0 ? (schedule[prevDay] !== undefined ? schedule[prevDay]['group'] : 0) : methods_day.vars.previousBind;

                        if (methods_calendar.vars.startMonth === month
                            && methods_calendar.vars.startYear === year) {
                            HTML.push(methods_day.display('dopbcp-past-day',
                                ID + '_' + cyear + '-' + cmonth + '-' + cday,
                                d.getDate(),
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                'none'));
                        }
                        else {
                            HTML.push(methods_day.display('dopbcp-last-month' + (position > 1 ? ' dopbcp-mask' : ''),
                                position > 1 ? ID + '_' + cyear + '-' + cmonth + '-' + cday + '_last' : ID + '_' + cyear + '-' + cmonth + '-' + cday,
                                d.getDate(),
                                day['available'],
                                day['bind'],
                                day['info'],
                                day['info_body'],
                                day['info_info'],
                                day['price'],
                                day['promo'],
                                day['status']));
                        }

                        if(newCol){
                            newCol = false;
                            HTML.push('</div> <div class = "col-md-10 col-sm-12">');
                        }
                    }

                    /*
                     * Display current month days.
                     */
                    for (i = 1; i <= noDays; i++) {
                        totalDays++;

                        d = new Date(year, month - 1, i);
                        cyear = d.getFullYear();
                        cmonth = prototypes.getLeadingZero(d.getMonth() + 1);
                        cday = prototypes.getLeadingZero(d.getDate());
                        day = schedule[cyear + '-' + cmonth + '-' + cday] !== undefined ? schedule[cyear + '-' + cmonth + '-' + cday] : methods_day.default(prototypes.getWeekDay(cyear + '-' + cmonth + '-' + cday));

                        if (methods_calendar.vars.startMonth === month
                            && methods_calendar.vars.startYear === year
                            && methods_calendar.vars.startDay > d.getDate()) {
                            HTML.push(methods_day.display('dopbcp-past-day',
                                ID + '_' + cyear + '-' + cmonth + '-' + cday,
                                d.getDate(),
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                'none'));
                        }
                        else {
                            HTML.push(methods_day.display('dopbcp-curr-month',
                                ID + '_' + cyear + '-' + cmonth + '-' + cday,
                                d.getDate(),
                                day['available'],
                                day['bind'],
                                day['info'],
                                day['info_body'],
                                day['info_info'],
                                day['price'],
                                day['promo'],
                                day['status']));
                        }

                        if(newCol){
                            newCol = false;
                            HTML.push('</div> <div class = "col-md-12 col-sm-12">');
                        }
                    }

                    /*
                     * Display next month days.
                     */
                    for (i = 1; i <= (totalDays + 7 < 42 ? 14 : 7) - lastDay; i++) {

                        if(newCol){
                            newCol = false;
                            HTML.push('</div> <div class = "col-md-12 col-sm-12">');
                        }

                        d = new Date(year, month, i);
                        cyear = d.getFullYear();
                        cmonth = prototypes.getLeadingZero(d.getMonth() + 1);
                        cday = prototypes.getLeadingZero(d.getDate());
                        day = schedule[cyear + '-' + cmonth + '-' + cday] !== undefined ? schedule[cyear + '-' + cmonth + '-' + cday] : methods_day.default(prototypes.getWeekDay(cyear + '-' + cmonth + '-' + cday));

                        HTML.push(methods_day.display('dopbcp-next-month' + (position < methods_months.vars.no ? ' dopbcp-hide' : ''),
                            position < methods_months.vars.no ? ID + '_' + cyear + '-' + cmonth + '-' + cday + '_next' : ID + '_' + cyear + '-' + cmonth + '-' + cday,
                            d.getDate(),
                            day['available'],
                            day['bind'],
                            day['info'],
                            day['info_body'],
                            day['info_info'],
                            day['price'],
                            day['promo'],
                            day['status']));

                    }

                    newCol = false;
                    $('.DOPBCPCalendar-calendar', Container).append(HTML.join(''));
                }
            },

// 10. Days

            methods_days = {
                data: {},
                text: {},
                vars: {
                    selectionEnd: '',
                    selectionInit: false,
                    selectionStart: '',
                    no: 0
                },

                displaySelection: function (id) {
                    /*
                     * Display selected days "selection".
                     *
                     * @param id (String): current day ID (ID_YYYY-MM-DD)
                     */
                    var $day,
                        day;

                    id = id === undefined ? methods_days.vars.selectionEnd : id;


                    if (methods_days.vars.selectionStart !== '') {
                        if (id < methods_days.vars.selectionStart) {
                            $($('.DOPBCPCalendar-day', Container).get().reverse()).each(function () {
                                if ($(this).attr('id').split('_').length === 2) {
                                    $day = $(this);
                                    day = this;

                                    /*
                                     * Add selection if day is available.
                                     */
                                    if ($day.attr('id') <= methods_days.vars.selectionStart
                                        && $day.attr('id') >= id
                                        && !$day.hasClass('dopbcp-mask')
                                        && !$day.hasClass('dopbcp-past-day')) {

                                        if(previousClicked != null && $day[0] == previousClicked[0]){
                                            if($day.hasClass('dopbcp-selected')){
                                                previousClicked.removeClass('dopbcp-selected');
                                                previousClicked = null;

                                            }
                                        }
                                        else {

                                            $day.addClass('dopbcp-selected');

                                            //Getting the selected date
                                            var date = $day[0].id.split('_')[1].split('-')

                                            //Truncating leading zeros
                                            date[1] = date[1].replace(/^0+/, '')
                                            date[2] = date[2].replace(/^0+/, '')

                                            console.log($('#booking_date_2i :selected').val())

                                            //Setting the date on the dropdown
                                            $("#booking_datetime_1i").val(parseInt(date[0]))
                                            $("#booking_datetime_2i").val(parseInt(date[1]))
                                            $("#booking_datetime_3i").val(parseInt(date[2]))



                                            if (previousClicked != null) {
                                                previousClicked.removeClass('dopbcp-selected');
                                                previousClicked = $day;
                                            }
                                            else {
                                                previousClicked = $day;
                                            }
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            $('.DOPBCPCalendar-day', Container).each(function () {
                                if ($(this).attr('id').split('_').length === 2) {
                                    $day = $(this);
                                    day = this;

                                    /*
                                     * Add selection if day is available.
                                     */
                                    if ($day.attr('id') >= methods_days.vars.selectionStart
                                        && $day.attr('id') <= id
                                        && !$day.hasClass('dopbcp-mask')
                                        && !$day.hasClass('dopbcp-past-day')) {

                                        if(previousClicked != null && $day[0] == previousClicked[0]){
                                            if($day.hasClass('dopbcp-selected')){
                                                previousClicked.removeClass('dopbcp-selected');
                                                previousClicked = null;
                                            }
                                        }
                                        else {

                                            //Getting the selected date
                                            var date = $day[0].id.split('_')[1].split('-')

                                            //Truncating leading zeros
                                            date[1] = date[1].replace(/^0+/, '')
                                            date[2] = date[2].replace(/^0+/, '')

                                            //Setting the date on the dropdown
                                            $("#booking_datetime_1i").val(date[0])
                                            $("#booking_datetime_2i").val(date[1])
                                            $("#booking_datetime_3i").val(date[2])


                                            $day.addClass('dopbcp-selected');
                                            if (previousClicked != null) {
                                                previousClicked.removeClass('dopbcp-selected');
                                                previousClicked = $day;
                                            }
                                            else {
                                                previousClicked = $day;
                                            }
                                        }

                                    }
                                }
                            });
                        }
                    }
                },

            },


            methods_day = {
                vars: {
                    previousBind: 0,
                    previousStatus: ''
                },

                display: function (type,
                                   id,
                                   day,
                                   available,
                                   bind,
                                   info,
                                   infoBody,
                                   infoInfo,
                                   price,
                                   promo,
                                   status) {
                    /*
                     * Display day.
                     *
                     * @param type (String): day type (past-day, last-month, curr-month, next-month)
                     * @param id (String): day ID (ID_YYYY-MM-DD)
                     * @param day (String): current day
                     * @param available (Number): number of available items for current day
                     * @param bind (Number): day bind status
                     *                       "0" none
                     *                       "1" binded at the start of a group
                     *                       "2" binded in a group group
                     *                       "3" binded at the end of a group
                     * @param info (String): day info
                     * @param infoBody (String): day body info
                     * @param infoInfo (String): day tooltip info
                     * @param price (Number): day price
                     * @param promo (Number): day promotional price
                     * @param status (String): day status (available, booked, special, unavailable)
                     *
                     * @retun day HTML
                     */
                    var dayHTML = Array(),
                        contentLine1 = '&nbsp;',
                        contentLine2 = '&nbsp;';

                    methods_days.vars.no++;

                    var dayAsInt = methods_days.vars.no % 7;

                    if(days.indexOf(dayAsInt) != -1){
                        status = "available";
                    }
                    else{
                        status = "unavailable";
                    }

                    if (type !== 'dopbcp-past-day') {
                        switch (status) {
                            case 'available':
                                type += ' dopbcp-available';

                                if (bind === 0
                                    || bind === 1) {
                                    if (available > 1) {
                                        contentLine2 = available + ' ' + '<span class="dopbcp-no-available-text">' + methods_calendar.text['availableMultiple'] + '</span>';
                                    }
                                    else if (available === 1) {
                                        contentLine2 = available + ' ' + '<span class="dopbcp-no-available-text">' + methods_calendar.text['available'] + '</span>';
                                    }
                                    else {
                                        contentLine2 = '<span class="dopbcp-text">' + methods_calendar.text['available'] + '</span>';
                                    }
                                }
                                break;
                            case 'unavailable':
                                type += ' dopbcp-unavailable';
                                contentLine2 = '<span class="dopbcp-no-available-text">' + methods_calendar.text['unavailable'] + '</span>';
                                break;
                        }

                        /*
                         * Add pointer cursor.
                         */
                        if (type.indexOf('mask') !== -1) {
                            /*
                             * No pointer cursor.
                             */
                            type += ' dopbcp-no-cursor-pointer';
                        }
                        else {
                            if ((!methods_days.data['morningCheckOut']
                                && (status === 'available'
                                || status === 'special'))) {
                                type += ' dopbcp-cursor-pointer';
                            }
                        }
                    }

                    if (dayAsInt === 1) {
                        type += ' dopbcp-first-column';
                    }
                    if (dayAsInt === 0) {
                        type += ' dopbcp-last-column';
                        newCol = true;
                    }

                    dayHTML.push('<div class="split-days DOPBCPCalendar-day ' + type + '" id="' + id + '">');
                    dayHTML.push('  <div class="dopbcp-bind-middle dopbcp-group' + ((status === 'available' || status === 'special') ? bind : '0') + (bind === 3 && methods_days.data['morningCheckOut'] && (status === 'available' || status === 'special') ? ' dopbcp-extended' : '') + (methods_day.vars.previousBind === 3 && methods_days.data['morningCheckOut'] && (methods_day.vars.previousStatus === 'available' || methods_day.vars.previousStatus === 'special') ? ' dopbcp-extended' : '') + '">');
                    dayHTML.push('      <div class="dopbcp-head">');
                    dayHTML.push('          <div class="dopbcp-co dopbcp-' + (methods_days.data['morningCheckOut'] ? methods_day.vars.previousStatus : status) + '"></div>');
                    dayHTML.push('          <div class="dopbcp-ci dopbcp-' + status + '"></div>');
                    dayHTML.push('          <div class="dopbcp-day">' + day + '</div>');
                    dayHTML.push('      </div>');
                    dayHTML.push('      <div class="dopbcp-body">');
                    dayHTML.push('          <div class="dopbcp-co dopbcp-' + (methods_days.data['morningCheckOut'] ? methods_day.vars.previousStatus : status) + '"></div>');
                    dayHTML.push('          <div class="dopbcp-ci dopbcp-' + status + '"></div>');
                    dayHTML.push('          <div class="dopbcp-price">' + contentLine1 + '</div>');
                    dayHTML.push('          <br class="DOPBCPCalendar-clear" />');
                    dayHTML.push('          <div class="dopbcp-available">' + contentLine2 + '</div>');
                    dayHTML.push('      </div>');
                    dayHTML.push('  </div>');
                    dayHTML.push('</div>');

                    if (type !== 'dopbcp-past-day') {
                        methods_day.vars.previousStatus = status;
                        methods_day.vars.previousBind = bind;
                    }
                    else {
                        methods_day.vars.previousStatus = 'none';
                        methods_day.vars.previousBind = 0;
                    }

                    return dayHTML.join('');
                },
                default: function (day) {
                    /*
                     * Day default data.
                     *
                     * @param day (Date): this day
                     *
                     * @return JSON with default data
                     */
                    return {
                        "available": "",
                        "bind": 0,
                        "info": "",
                        "info_body": "",
                        "info_info": "",
                        "notes": "",
                        "price": 0,
                        "promo": 0,
                        "status": methods_days.data['available'][day] ? "none" : "unavailable"
                    };
                },

                events: {
                    init: function () {
                        /*
                         * Initialize days events.
                         */
                        if (!methods_calendar.data['view']) {
                                methods_day.events.selectSingle();
                        }
                        else {
                            $('.DOPBCPCalendar-day .dopbcp-co', Container).css('cursor', 'default');
                            $('.DOPBCPCalendar-day .dopbcp-ci', Container).css('cursor', 'default');
                        }

                        if (!prototypes.isTouchDevice()) {
                            if (!methods_calendar.data['view']) {
                                /*
                                 * Days hover events, not available for devices with touchscreen.
                                 */
                                $('.DOPBCPCalendar-day', Container).hover(function () {
                                    var day = $(this);

                                    if (methods_days.vars.selectionInit) {
                                        methods_days.displaySelection(day.attr('id'));
                                    }
                                }, function () {
                                    methods_tooltip.clear();
                                });
                            }

                            /*
                             * Info icon events.
                             */
                            $('.DOPBCPCalendar-day .dopbcp-info', Container).hover(function () {
                                methods_tooltip.set($(this).attr('id').split('_')[1],
                                    '',
                                    'info');
                            }, function () {
                                methods_tooltip.clear();
                            });

                            /*
                             * Body info events.
                             */
                            $('.DOPBCPCalendar-day .dopbcp-info-body', Container).hover(function () {
                                methods_tooltip.set($(this).attr('id').split('_')[1],
                                    '',
                                    'info-body');
                            }, function () {
                                methods_tooltip.clear();
                            });
                        }
                        else {
                            var xPos = 0,
                                yPos = 0,
                                touch;

                            /*
                             * Info icon events on devices with touchscreen.
                             */
                            $('.DOPBCPCalendar-day .dopbcp-info', Container).unbind('touchstart');
                            $('.DOPBCPCalendar-day .dopbcp-info', Container).bind('touchstart', function (e) {
                                e.preventDefault();
                                touch = e.originalEvent.touches[0];
                                xPos = touch.clientX + $(document).scrollLeft();
                                yPos = touch.clientY + $(document).scrollTop();
                                $('#DOPBCPCalendar-tooltip' + ID).css({
                                    'left': xPos,
                                    'top': yPos
                                });
                                methods_tooltip.set($(this).attr('id').split('_')[1],
                                    '',
                                    'info');
                            });

                            /*
                             * Body info events on devices with touchscreen.
                             */
                            $('.DOPBCPCalendar-day .dopbcp-info-body', Container).unbind('touchstart');
                            $('.DOPBCPCalendar-day .dopbcp-info-body', Container).bind('touchstart', function (e) {
                                e.preventDefault();
                                touch = e.originalEvent.touches[0];
                                xPos = touch.clientX + $(document).scrollLeft();
                                yPos = touch.clientY + $(document).scrollTop();
                                $('#DOPBCPCalendar-tooltip' + ID).css({
                                    'left': xPos,
                                    'top': yPos
                                });
                                methods_tooltip.set($(this).attr('id').split('_')[1],
                                    '',
                                    'info-body');
                            });
                        }
                    },

                    selectSingle: function () {
                        /*
                         * Select single day event.
                         */
                        /*
                         * Days click event.
                         */
                        $('.DOPBCPCalendar-day', Container).unbind('click touchstart');
                        $('.DOPBCPCalendar-day', Container).bind('click touchstart', function () {
                            var day = $(this),
                                dayThis = this,
                                sDate,
                                sDay,
                                sMonth,
                                sYear;

                            /*
                             * Add a small delay of 10 miliseconds for hover selection to display properly.
                             */
                            setTimeout(function () {
                                /*
                                 * Check if the day has availability set.
                                 */
                                if ((day.hasClass('dopbcp-available')
                                    || day.hasClass('dopbcp-special'))
                                    && $('.dopbcp-bind-middle', dayThis).hasClass('dopbcp-group0')) {
                                    /*
                                     * Select one day.
                                     */
                                    methods_days.vars.selectionInit = false;
                                    methods_days.vars.selectionStart = day.attr('id');
                                    methods_days.vars.selectionEnd = day.attr('id');

                                    sDate = methods_days.vars.selectionStart.split('_')[1];
                                    sYear = sDate.split('-')[0];
                                    sMonth = parseInt(sDate.split('-')[1], 10) - 1;
                                    sDay = sDate.split('-')[2];

                                    $('#DOPBCPCalendar-check-in' + ID).val(sDate);
                                    $('#DOPBCPCalendar-check-in-view' + ID).val(methods_calendar.data['dateType'] === 1 ?
                                    methods_months.text['names'][sMonth] + ' ' + sDay + ', ' + sYear :
                                    sDay + ' ' + methods_months.text['names'][sMonth] + ' ' + sYear);

                                    methods_days.displaySelection(methods_days.vars.selectionEnd);

                                }
                            }, 10);
                        });
                    }
                }
            },

// ***************************************************************************** Prototypes

// 23. Prototypes

            prototypes = {
// Actions
                isTouchDevice: function () {
                    /*
                     * Detect touchscreen devices.
                     *
                     * @return true/false
                     */
                    var os = navigator.platform;

                    if (os.toLowerCase().indexOf('win') !== -1) {
                        return window.navigator.msMaxTouchPoints;
                    }
                    else {
                        return 'ontouchstart' in document;
                    }
                },
                getPrevDay: function (date) {
                    /*
                     * Returns previous day.
                     *
                     * @param date (Date): current date (YYYY-MM-DD)
                     *
                     * @return previous day (YYYY-MM-DD)
                     */
                    var previousDay = new Date(),
                        parts = date.split('-');

                    previousDay.setFullYear(parts[0],
                        parseInt(parts[1]) - 1,
                        parts[2]);
                    previousDay.setTime(previousDay.getTime() - 86400000);

                    return previousDay.getFullYear() + '-' + prototypes.getLeadingZero(previousDay.getMonth() + 1) + '-' + prototypes.getLeadingZero(previousDay.getDate());
                },

                getWeekDay: function (date) {
                    /*
                     * Returns week day.
                     *
                     * @param date (String): date for which the function get day of the week (YYYY-MM-DD)
                     *
                     * @return week day index (0 for Sunday)
                     */
                    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        year = date.split('-')[0],
                        month = date.split('-')[1],
                        day = date.split('-')[2],
                        newDate = new Date(eval('"' + day + ' ' + months[parseInt(month, 10) - 1] + ', ' + year + '"'));

                    return newDate.getDay();
                },

                getLeadingZero: function (no) {
                    /*
                     * Adds a leading 0 if number smaller than 10.
                     *
                     * @param no (Number): the number
                     *
                     * @return number with leading 0 if needed
                     */
                    if (no < 10) {
                        return '0' + no;
                    }
                    else {
                        return no;
                    }
                }

            };

        return methods.init.apply(this);
    };
})(jQuery);

function parseAvailability(array){

    var days = [];

    if(array.indexOf('Mon') != -1){
        days.push(1);
    }

    if(array.indexOf('Tue') != -1){
        days.push(2);
    }

    if(array.indexOf('Wed') != -1){
        days.push(3);
    }

    if(array.indexOf('Thur') != -1){
        days.push(4);
    }

    if(array.indexOf('Fri') != -1){
        days.push(5);
    }

    if(array.indexOf('Sat') != -1){
        days.push(6);
    }

    if(array.indexOf('Sun') != -1){
        days.push(7);
    }

    return days;

}

(function ($) {
    $.fn.DOPSelect = function (options) {
        /*
         * Private variables.
         */
        var Data = {},
            Container = this,

            ID = '',
            id = '',
            name = '',
            classes = '',
            onChange = '',
            isDisabled = false,
            isMultiple = false,
            thisItem = '',
            values = new Array(),
            labels = new Array(),
            selectedOption = 0,

            firstClick = false,
            wasChanged = false,

            methods = {
                init: function () {
                    /*
                     * Initialize jQuery plugin.
                     */
                    return this.each(function () {
                        if (options) {
                            $.extend(Data, options);
                        }
                        methods.parse();
                    });
                },
                parse: function () {
                    /*
                     * Parse select options.
                     */
                    id = $(Container).attr('id') !== undefined ? $(Container).attr('id') : '';
                    name = $(Container).attr('name') !== undefined ? $(Container).attr('name') : '';
                    classes = $(Container).attr('class') !== undefined ? $(Container).attr('class') : '';
                    onChange = $(Container).attr('onchange') !== undefined ? $(Container).attr('onchange') : '';
                    isDisabled = $(Container).attr('disabled') !== undefined ? true : false;
                    thisItem = id !== '' ? '#' + id : 'select[name*="' + name + '"]';
                    isMultiple = $(thisItem + '[multiple]').length ? true : false;
                    ID = id !== '' ? id : name;

                    $(thisItem + ' option').each(function () {
                        values.push($(this).attr('value'));
                        labels.push($(this).html());

                        if ($(this).is(':selected')) {
                            selectedOption = values.length - 1;
                        }
                    });
                    methods.display();
                },
                display: function () {
                    /*
                     * Display select component.
                     */
                    var HTML = new Array(),
                        i;

                    HTML.push('<div id="DOPSelect-' + ID + '" class="DOPSelect ' + (isMultiple ? 'dopselect-multiple' : 'dopselect-single') + ' ' + (isDisabled ? 'dopselect-disabled' : '') + ' ' + classes + '">');
                    HTML.push(' <input type="hidden" id="' + ID + '" name="' + name + '" value="' + (isMultiple ? '' : values[selectedOption]) + '">');

                    /*
                     * Display "selected" component only on single select.
                     */
                    if (!isMultiple) {
                        HTML.push(' <div class="dopselect-select">');
                        HTML.push('     <div class="dopselect-selection">' + (values.length !== 0 ? labels[selectedOption] : '') + '</div>');
                        HTML.push('     <div class="dopselect-icon">&#x25BE;</div>');
                        HTML.push(' </div>');
                    }
                    HTML.push(' <ul>');

                    for (i = 0; i < values.length; i++) {
                        if (!isMultiple) {
                            /*
                             * Single select options.
                             */
                            HTML.push('     <li id="DOPSelect-' + ID + '-' + values[i] + '" title="' + labels[i] + '"' + (selectedOption === i ? ' class="dopselect-selected"' : '') + '>' + labels[i] + '</li>');
                        }
                        else {
                            /*
                             * Multiple select options.
                             */
                            HTML.push('     <li title="' + labels[i] + '">');
                            HTML.push('         <input type="checkbox" name="DOPSelect-' + ID + '-' + values[i] + '" id="DOPSelect-' + ID + '-' + values[i] + '"' + (isDisabled ? ' disabled="disabled"' : '') + ' />');
                            HTML.push('         <label for="DOPSelect-' + ID + '-' + values[i] + '">' + labels[i] + '</label>');
                            HTML.push('     </li>');
                        }
                    }
                    HTML.push(' </ul>');
                    HTML.push(' <div class="option-extension"></div>');
                    HTML.push('</div>');

                    $(Container).replaceWith(HTML.join(''));

                    if (!isDisabled) {
                        methods.events();
                    }
                },
                events: function () {
                    /*
                     * Initialize select component events.
                     */
                    if (isMultiple) {
                        /*
                         * Multiple select events.
                         */
                        $('#DOPSelect-' + ID + ' ul li').unbind('click');
                        $('#DOPSelect-' + ID + ' ul li').bind('click', function () {
                            var selected = new Array(),
                                id;

                            $('#DOPSelect-' + ID + ' ul li input[type=checkbox]').each(function () {
                                if ($(this).is(':checked')) {
                                    id = $(this).attr('id').split('DOPSelect-' + ID + '-')[1];
                                    selected.push(id);
                                }
                            });

                            $('#' + ID).val(selected)
                                .trigger('change');

                            if (onChange !== '') {
                                eval(onChange.replace(/this.value/g, selected));
                            }
                        });
                    }
                    else {
                        /*
                         * Single select events.
                         */
                        $(document).mousedown(function (event) {
                            if ($(event.target).parents('#DOPSelect-' + ID).length === 0) {
                                $('#DOPSelect-' + ID + ' ul').css('display', 'none')
                                    .scrollTop(0);
                            }
                        });

                        $('#DOPSelect-' + ID + ' .dopselect-select').unbind('click');
                        $('#DOPSelect-' + ID + ' .dopselect-select').bind('click', function () {

                            if ($('#DOPSelect-' + ID + ' ul').css('display') === 'block') {
                                $('#DOPSelect-' + ID + ' ul').css('display', 'none')
                                    .scrollTop(0);
                            }
                            else {
                                var scrollTo;

                                $('.DOPSelect.dopselect-single ul').css('display', 'none');
                                $('#DOPSelect-' + ID + ' ul').css('display', 'block');

                                /*
                                 * Duplicate scrollTo action for the right position.
                                 */
                                scrollTo = $('#DOPSelect-' + ID + ' ul li.dopselect-selected').position().top - $('#DOPSelect-' + ID + ' ul li.dopselect-selected').height();
                                $('#DOPSelect-' + ID + ' ul').scrollTop(scrollTo);

                                if (wasChanged
                                    || firstClick) {
                                    scrollTo = $('#DOPSelect-' + ID + ' ul li.dopselect-selected').position().top - $('#DOPSelect-' + ID + ' ul li.dopselect-selected').height();
                                    $('#DOPSelect-' + ID + ' ul').scrollTop(scrollTo);
                                }

                                if (!firstClick) {
                                    firstClick = true;
                                }
                            }
                        });

                        $('#DOPSelect-' + ID + ' ul li').unbind('click');
                        $('#DOPSelect-' + ID + ' ul li').bind('click', function () {
                            if (!$(this).hasClass('dopselect-selected')) {
                                wasChanged = true;

                                $('#DOPSelect-' + ID + ' ul li').removeClass('dopselect-selected');
                                $(this).addClass('dopselect-selected');
                                $('#DOPSelect-' + ID + ' .dopselect-selection').html($(this).html());
                                $('#' + ID).val($(this).attr('id').split('DOPSelect-' + ID + '-')[1])
                                    .trigger('change');

                                if (onChange !== '') {
                                    eval(onChange.replace(/this.value/g, "'" + $(this).attr('id').split('DOPSelect-' + ID + '-')[1] + "'"));
                                }
                            }
                            $('#DOPSelect-' + ID + ' ul').css('display', 'none')
                                .scrollTop(0);
                        });
                    }
                }
            };

        return methods.init.apply(this);
    };
})(jQuery);


$(document).ready(function () {

    //  Menu
    $('#menu-btn').unbind('click');
    $('#menu-btn').bind('click', function () {

        if ($('#menu').css('display') === 'none') {
            $(this).addClass('selected');
            $('#menu').slideDown(300);
        } else {
            $(this).removeClass('selected');
            $('#menu').slideUp(100);
        }
    });


    // Support Center
    if ($(window).width() < 600) {
        $('#footer .bar-wrapper .support .icon').css('left', parseInt($(window).width() / 2 - 20));

        $(window).resize(function () {
            $('#footer .bar-wrapper .support .icon').css('left', parseInt($(window).width() / 2 - 20));
        });
    } else {
        $('#footer .bar-wrapper .support .icon').css('left', '47.5%');
    }

    $(window).resize(function () {

        if ($(window).width() < 600) {
            $('#footer .bar-wrapper .support .icon').css('left', parseInt($(window).width() / 2 - 20));

            $(window).resize(function () {
                $('#footer .bar-wrapper .support .icon').css('left', parseInt($(window).width() / 2 - 20));
            });
        } else {
            $('#footer .bar-wrapper .support .icon').css('left', '47.5%');
        }
    });
});


$(document).ready(function () {
    $('#frontend').DOPFrontendBookingCalendarPRO();
});
