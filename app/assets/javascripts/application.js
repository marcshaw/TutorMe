//
//= require main

//= require autocomplete-rails
//= require jquery-ui/widgets/autocomplete
//= require autonumeric

//= require autocomplete
//=require jquery-bar-rating
//=require rating
//=require profile
//=require price_range_slider

//= require conversation
//= require jquery.dotdotdot
//= require search
//= require profile
//= require malihu-custom-scrollbar-plugin
//= require bookings
//= require jquery-ui/widgets/datepicker
//= require jquery-timepicker-jt
//= require add_funds
//= require faq

//https://github.com/kossnocorp/jquery.turbolinks
//http://stackoverflow.com/questions/18769109/rails-4-turbo-link-prevents-jquery-scripts-from-working
//moved to head.js
//TODO = require jquery.turbolinks
//TODO Commented out turbo links as interfares with autocomplete
//TODO = require turbolinks

