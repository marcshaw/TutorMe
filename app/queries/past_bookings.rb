class PastBookings
  def initialize(bookings)
    @bookings = bookings
  end

  def call
    @bookings.where("datetime < ?", Time.now)
  end
end
