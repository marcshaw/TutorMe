class ConfirmedBookings
  def initialize(bookings: Booking.all, tutor: nil, student: nil)
    @bookings = bookings
    @tutor = tutor
    @student = student
  end

  def call
    CurrentBookings.new(@bookings).call.where("tutor_confirmed = ? or student_confirmed = ?",
                                             @tutor,
                                             @student)
  end
end
