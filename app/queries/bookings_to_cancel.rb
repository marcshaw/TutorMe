class BookingsToCancel
  DAYS_BEFORE = 1.day

  def initialize(bookings: Booking.uncancelled.uncompleted.payment_not_complete.payment_not_held)
    @bookings = bookings
  end

  def call
    cutoff_date = Time.now + DAYS_BEFORE
    @bookings.where('datetime < ?', cutoff_date)
  end
end
