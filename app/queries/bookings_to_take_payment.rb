class BookingsToTakePayment
  DAYS_BEFORE = 2.days

  def initialize(bookings: Booking.uncancelled.confirmed.payment_not_complete.payment_held)
    @bookings = bookings
  end

  def call
    payment_date = Time.now + DAYS_BEFORE
    @bookings.where('Date(datetime) = ?', payment_date.utc.to_date)
  end
end
