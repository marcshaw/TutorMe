class PaymentsToRelease
  DAYS_AFTER = 3.days

  def initialize(bookings: Booking.payment_not_complete.payment_held.not_disputed.uncancelled.confirmed.uncompleted)
    @bookings = bookings
  end

  def call
    release_date = Time.now - DAYS_AFTER
    @bookings.where('datetime < ?', release_date)
  end
end
