class SearchProfiles
  def initialize(current_profile_id, search, profiles = Profile.all)
    @current_profile_id = current_profile_id
    @profiles = profiles
    @search = search
  end

  def call
    if @search.empty?
      return Profile.none
    end

    profiles = @profiles.where(:active_tutor => true).where('profiles.id != ?', @current_profile_id)

    profiles = search_by_media(profiles)
    profiles = search_by_tutoring_year(profiles)

    if TutoringSubject.find_by(name: @search.tutoring_subject)
      profiles = search_by_tutoring_subject(profiles)
    else
      profiles = search_by_course_code(profiles)
    end

    profiles = search_by_university(profiles)
    profiles = search_by_price_range(profiles)

    return profiles
  end

  protected
  def search_by_price_range(profiles)
    profiles = profiles.where(["rate_cents >= ?", @search.min_rate_cents * 100]) if @search.min_rate_cents != 0

    if @search.max_rate_cents != 0
      profiles.where(["rate_cents <= ?", @search.max_rate_cents * 100])
    else
      profiles
    end
  end

  def search_by_university(profiles)
    university_id = University.find_by(name: @search.university)
    if university_id.present?
      profiles = profiles.where("university_id = ?", university_id)
    else
      profiles
    end
  end

  def search_by_course_code(profiles)
    if @search.course_code.present?
      profile_ids = CourseCode.where('course_code LIKE ?', @search.course_code.downcase).select(:profile_id).group("profile_id").pluck(:profile_id)
      profiles.where('profiles.id IN (?)', profile_ids)
    else
      profiles
    end
  end

  def search_by_tutoring_subject(profiles)
    if(@search.tutoring_subject.present?)
      #Joining the tables, then group the profiles by the id so we don't get duplicates.
      tutoring_subject = TutoringSubject.find_by(name: @search.tutoring_subject)
      return profiles.joins("join profiles_tutoring_subjects on profiles.id = profiles_tutoring_subjects.profile_id").where(["tutoring_subject_id = ?", tutoring_subject.id]).group("profiles.id")
    end
    profiles
  end


  def search_by_media(profiles)
    if(@search.media.present?)
      new_media_ids = @search.media.delete("^0-9,").split(',')

      if !new_media_ids.empty?
        #Joining the tables, then group the profiles by the id so we don't get duplicates.
        return profiles.joins("join media_profiles on profiles.id = media_profiles.profile_id").where(["medium_id IN (?)", new_media_ids.reject(&:blank?)]).group("profiles.id")
      end
    end
    profiles
  end

  def search_by_tutoring_year(profiles)
    if(@search.tutoring_year.present?)
      new_tutoring_years_ids = @search.tutoring_year.delete("^0-9,").split(',')

      if !new_tutoring_years_ids.empty?
        #Joining the tables, then group the profiles by the id so we don't get duplicates.
        return profiles.joins("join profiles_tutoring_years on profiles.id = profiles_tutoring_years.profile_id").where(["tutoring_year_id IN (?)", new_tutoring_years_ids.reject(&:blank?)]).group("profiles.id")
      end
    end
    profiles
  end
end
