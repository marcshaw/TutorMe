class DeviseMailer < Devise::Mailer
  include Devise::Controllers::UrlHelpers

  helper :application

  default template_path: 'devise/mailer'
  default :from => 'no-reply@tutrme.co'
  layout 'base_email'

  def confirmation_instructions(record, token, opts={})
    @resource = record
    @token = token
    mail(to: record.email,
        subject: "Tutrme Confirmation Instructions")
  end

  def reset_password_instructions(record, token, opts={})
    @resource = record
    @token = token
    mail(to: record.email,
        subject: "Password Reset")
  end

  def unlock_instructions(record, token, opts={})
    @resource = record
    @token = token
    mail(to: record.email,
        subject: "Unlock Account")
  end
end
