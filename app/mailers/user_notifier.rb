class UserNotifier < ApplicationMailer
  #TODO
  default :from => 'no-reply@tutrme.co'
  layout 'base_email'

  def welcome_email(profile)
    @profile = profile
    mail( :to => profile.email,
         :subject => 'Welcome to Tutrme!' )
  end

  def new_tutor_booking(tutor, student)
    @tutor = tutor
    @student = student
    mail( :to => tutor.email,
         :subject => 'You have a new booking!' )
  end

  def new_student_booking(tutor, student)
    @tutor = tutor
    @student = student
    mail( :to => student.email,
         :subject => 'Successfully requested a booking' )
  end

  def confirmed_tutor_booking(booking)
    @tutor = booking.tutor_profile
    @student = booking.student_profile
    @booking = booking
    mail( :to => @tutor.email,
         :subject => 'You have a confirmed booking!' )
  end

  def confirmed_student_booking(booking)
    @tutor = booking.tutor_profile
    @student = booking.student_profile
    @booking = booking
    mail( :to => @student.email,
         :subject => 'You have a confirmed booking!' )
  end

  def confirm_booking(profile, booking)
  end

  def reminder_email(profile, booking)
    @booking = booking
    @profile = profile
    mail( :to => profile.email,
         :subject => 'You have a confirmed booking!' )
  end

  def review_tutor(review)
    @review = review
    mail( :to => review.reviewer.email,
         :subject => "Review #{review.profile.first_name}" )
  end

  def new_notifcations(profile)
  end

  def week_prior_add_funds_warning(profile, booking)
    @booking = booking
    mail( :to => profile.email,
         :subject => 'Please add funds to your account soon' )
  end

  def three_days_prior_add_funds_warning(profile, booking)
    @booking = booking
    mail( :to => profile.email,
         :subject => 'Please add funds within three days' )
  end

  def cancelled_student_booking(booking)
    @booking = booking
    mail( :to => booking.student_profile.email,
         :subject => 'We have had to cancel a booking' )
  end

  def cancelled_tutor_booking(booking)
    @booking = booking
    mail( :to => booking.tutor_profile.email,
         :subject => 'We have had to cancel a booking' )
  end
end
