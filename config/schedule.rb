# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end
env 'MAILTO', 'support@tutrme.co'
env :PATH, ENV['PATH']

job_type :runner,  'cd :path && bundle exec rails runner -e :environment ":task" :output'

# Learn more: http://github.com/javan/whenever
every 10.minutes do
  runner "job = ReleaseHeldPayments.new; job.call"
end

every 10.minutes do
  runner "job = CancelBookings.new; job.call"
end
