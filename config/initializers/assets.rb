# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
Rails.application.config.assets.precompile += %w( head.js )
Rails.application.config.assets.precompile += %w( vendor.js )
Rails.application.config.assets.precompile += %w( calendar.js )
Rails.application.config.assets.precompile += %w( vendor.css )
Rails.application.config.assets.precompile += %w( main.css )
Rails.application.config.assets.precompile += %w( emails/base.css )
# fonts
Rails.application.config.assets.paths << "#{Rails.root}/vendor/fonts"
Rails.application.config.assets.paths << "#{Rails.root}/vendor/components/bootstrap-sass/assets/fonts/bootstrap"
Rails.application.config.assets.precompile << /\.(?:svg|eot|woff|ttf|woff2)\z/
