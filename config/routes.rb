Rails.application.routes.draw do


  root 'home#index'

  devise_for :profiles, :controllers => {:registrations => "registrations"}

  devise_scope :profile do
    get 'signed_up', :to => 'registrations#show'
  end

  get '/error', to: 'errors#index'
  get 'privacy_policy', to: 'privacy_policy#show'
  get 'legal', to: 'legal#show'
  get 'faq', to: 'faq#show'

  resources :profiles, only: [:show, :update, :edit]  do
    get :autocomplete_tutoring_subject_name, :on => :collection
    get :autocomplete_university_name, :on => :collection
    resources :reviews, only: [:edit, :update, :index]
    resource :availability, only: [:index]
    get '/history', to: 'booking_history#index'
    resources :bookings do
      get 'confirm'
      get 'cancel'
      get 'pay'
      resources :bookings_messages, only: [:new,:create,:index]
    end
    resources :add_funds, only: [:index, :new, :create]
  end

  resources :conversations ,only: [:create, :index, :show] do
    resources :messages, only: [:create]
  end

  resources :tutoring_subjects, only: [:collection]
  resources :universities, only: [:collection]
  resource :searches, :path => '/search', only: [:show , :new]

  %w( 404 422 500 503 ).each do |code|
    match code, :to => "errors#index", :code => code, :via => :all
  end
end
